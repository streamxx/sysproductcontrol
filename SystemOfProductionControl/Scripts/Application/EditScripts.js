﻿


$(function () { 
    $.getJSON("/JournalOfRepairToComponents/GetComponent", null, SetAutocomplete);
});


var mas;

function SetAutocomplete(flowers) {

    mas = Array(flowers.length);

    $.each(flowers,
        function (i) {
            mas[i] = flowers[i].Text;
        }); 

    $('input[name="acInput"]')
        .autocomplete({
            source: mas,
            minLength: 4
          
        }); 
};



function addComponent(Idjournal, nameId,countId)
{
    var compname = $("#" + nameId).val();
    $("#" + nameId).val('');
    var count = $("#" + countId).val();
    IdjournalRequest = Idjournal;
    AddComponent(Idjournal, compname, count, updateComponentRequest);
}


function addWarning(Idjournal, warningId, nameId) {
     
    var IDwarning = $("#" + warningId).val(); 
    var compname = $("#" + nameId).val();
    $("#" + nameId).val('');
    $("#" + warningId).val(-1);
    IdjournalRequest = Idjournal;
    AddWarning(Idjournal, IDwarning, compname, updateWarningRequest);
}

function addWarningForComponent(Idjournal,compname) {

    var IDwarning = -1;
 
    IdjournalRequest = Idjournal;
    AddWarning(Idjournal, IDwarning, compname, updateWarningRequest);
}

function removeComponent(Idjournal, idComponent, type) {
    IdjournalRequest = Idjournal;
    RemoveComponent(Idjournal, idComponent, type, updateComponentRequest);
}

function removeWarning(Idjournal, idComponent, type) {
    IdjournalRequest = Idjournal;
    RemoveWarning(Idjournal, idComponent, type, updateWarningRequest);
}


var IdjournalRequest;
function updateComponentRequest(ComponentsTableDate)
{

    $("#componentContayner" + IdjournalRequest).empty();
    $("#componentContayner" + IdjournalRequest)
        .append(ComponentsTableDate); 
    // .append(ComponentsTableDate.ViewString); 
}

function updateWarningRequest(ComponentsTableDate) {

    $("#warningContainer_" + IdjournalRequest).empty();
    $("#warningContainer_" + IdjournalRequest)
        .append(ComponentsTableDate);
    // .append(ComponentsTableDate.ViewString); 
}

function CloseRepair(id, button) {
    var Components = {
        Id: id
    };
    $.getJSON("/Acts/CloseRepair", Components, null);

    var n = $(button).parent();
    n.empty();
    n.append('Зaвершено');
};



function CloseRepairAll(applicationID) {
    
    window.location.href = "/Applications/CloseRepairAll?applicationID=" + applicationID;
};


var ContainerId;

function changefilter(object, containerId, productId) {

    var name = $(object).val();
    ContainerId = containerId;

    var data =
    {  Name: name, 
       productId: productId
    };

    $.getJSON("/JournalOfRepairToComponents/GetComponentOFfilter", data, SetFilerData);
}

function SetFilerData(data) {

    var containerId = ContainerId;
    var s = document.getElementById(containerId);

    $("#" + containerId).empty();

    for (var i = 0; i < data.length; i++) {
        var element = "<option value='" + data[i].Text + "' />"; 
        $("#" + containerId).append(element);
    }
    }

   



//function DeleteAllCheckbox() {
//    var checkbox = $(".checkboxSelected");

//    var arr = "";
//    var count = 0;
//    for (var i = 0; i < checkbox.length; i++) {


//        var val = $(checkbox[i]).val();
//        var checked = checkbox[i].checked;

//        if (checked) {
//            if (count != 0) {
//                arr += "&";
//            }
//            arr += "idArray[" + count + "]=" + val; //+ "&";

//            count++;
//        }


//    }

//    if (count == 0) return;
//    window.location.href = "/JournalOfRepairs/DeleteAll?" + arr;
//}


$(function () {

    $('#ButtonGone')
        .click(function () {
            var button = $(this);
            // if (name != "Нет") {
            //if (document.getElementById("Body").style.display == "none") {
            //    document.getElementById("Body").style.display = "block";

            // }
            // else {
            document.getElementById("Body").style.display = "none";
            document.getElementById("ButtonVisible").style.display = "block";
            document.getElementById("ButtonGone").style.display = "none";
            //  }
            // document.getElementById("text1").style.display = "block";
            //}
        });
});

$(function () {

    $('#ButtonVisible')
        .click(function () {
            var button = $(this);
            // if (name != "Нет") {
            document.getElementById("Body").style.display = "block";
            document.getElementById("ButtonGone").style.display = "block";
            document.getElementById("ButtonVisible").style.display = "none";
            // document.getElementById("text1").style.display = "block";
            //}
        });
});
