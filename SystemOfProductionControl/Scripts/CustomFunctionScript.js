﻿function RemoveComponent(Idjournal, idComponent, type, functionRequest) {

    var Components = {
        id: idComponent,
        type: type,
        Idjournal: Idjournal
    };

    $.ajax({
        type: "GET",
        url: "/Applications/RemoveComponent",
        data: Components,
        success: functionRequest,
        error: function (errorData) {

        }
    });
}


function SetReplays(Idjournal, replays) {
    var data = {
        replays: replays,
        Idjournal: Idjournal
    };

    $.getJSON("/JournalOfRepairs/SetReplays", data, null);
}

function AddComponent(Idjournal, compname, count, functionRequest) {

    var Components = {
        name: compname,
        count: count,
        Idjournal: Idjournal
    };

    $.ajax({
        type: "GET",
        url: "/Applications/AddComponent",
        data: Components,
        success: functionRequest,
        error: function (errorData) {

        }
    });
}


function AddWarning(Idjournal, idWarning, compname, functionRequest) {

    var warning =
    {
        idWarning: idWarning,
        compname: compname,
        Idjournal: Idjournal
    };

    $.ajax({
        type: "GET",
        url: "/Applications/AddWarning",
        data: warning,
        success: functionRequest,
        error: function (errorData) {

        }
    });
}


function RemoveWarning(Idjournal, idWarning, type, functionRequest) {

    var warning =
    {
        idWarning: idWarning,
        type: type,
        Idjournal: Idjournal
    };

    $.ajax({
        type: "GET",
        url: "/Applications/RemoveWarning",
        data: warning,
        success: functionRequest,
        error: function (errorData) {

        }
    });
}

function ElementGone(idElement) {

    document.getElementById(idElement).style.display = "none";
}

function ElementVisible(idElement) {

    document.getElementById(idElement).style.display = "block";
}

function ToOtherType(id) {
    var data = {
        Id: id
    };
    $.getJSON("/Acts/ToOtherType", data, null);
};

function SetWaitingComponent(id) {
    var data = {
        Id: id
    };
    $.getJSON("/JournalOfRepairs/SetWaitingComponent", data, null);
};

function SetVisible(idElement, visible) {

    if ((visible === "true") || (visible === true)) {
        ElementVisible(idElement);
    } else {
        ElementGone(idElement);
    }
}
function SetActReady(idAct) {
    document.getElementById("ActReadyButton").style.display = "none";
    var Data = {
        actId: idAct
    };
    $.getJSON("/Acts/SetActReady", Data, null);
};
function SetActReadyStorage(idAct) {
    document.getElementById("ActReadyButton").style.display = "none";
    var Data = {
        actId: idAct
    };
    $.getJSON("/Acts/SetActReadyStorage", Data, null);
};

function SetActReadyStorageCancel(idAct) {
    document.getElementById("ActReadyButton").style.display = "none";
    var Data = {
        actId: idAct
    };
    $.getJSON("/Acts/SetActReadyStorageCancel", Data, null);
};





function GoToLink(link) {

    document.location.href = link;
}


function SetVisibleTable(id, IsVisible) {

    if (IsVisible) {
        id.style.display = "table-row";
    } else {
        id.style.display = "none";
    }

};

function SetVisibleButton(id, IsVisible) {

    if (IsVisible)
        id.style.display = "inline-block";
    else
        id.style.display = "none";
};




