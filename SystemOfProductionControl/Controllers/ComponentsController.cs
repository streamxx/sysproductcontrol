﻿
/*
 *      Компоненты
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using SystemOfProductionControl.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl;
using SystemOfProductionControl.other;

namespace SystemOfProductionControl.Controllers
{
    public class ComponentsController : Controller
    {
        private Entities db = new Entities();

        // GET: Components
        public ActionResult Index()
        {
            var component = db.Component.Include(c => c.Storage1);
            ViewBag.storage = new SelectList(db.Storage, "Id", "Name");
            return View(component.ToList());
        }

        public ActionResult PreIndex()
        {

            ViewBag.storage = new SelectList(db.Storage, "Id", "Name");
            return View();
        }
        // GET: Components/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Component.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            return View(component);
        }

        // GET: Components/Create
        public ActionResult Create()
        {
            ViewBag.storage = new SelectList(db.Storage, "Id", "Name");
            return View();
        }

        // POST: Components/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,code,storage")] Component component)
        {
            if (ModelState.IsValid)
            {
                db.Component.Add(component);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.storage = new SelectList(db.Storage, "Id", "Name", component.storage);
            return View(component);
        }

        // GET: Components/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Component.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            ViewBag.storage = new SelectList(db.Storage, "Id", "Name", component.storage);
            return View(component);
        }


        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload,int storage = 0)
        {
            if (upload != null)
            {
                // получаем имя файла
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                // сохраняем файл в папку Files в проекте

                CSVReader reader = new CSVReader(upload.InputStream, Encoding.Default);
                string[] headers = new []{"",""};
                headers = reader.GetCSVLine();

                while ((headers[0] == "")&&(headers[1] == ""))
                {
                    headers = reader.GetCSVLine();
                }

                string[] data;

                List<Component> components = new List<Component>();
                while ((data = reader.GetCSVLine()) != null)
                {
                    if (data[0] != "" && data[1] != "")
                    {
                        Component component = new Component();
                        component.Name = data[1];
                        component.code = int.Parse(data[0].TrimStart('0'));
                        component.storage = storage;
                        components.Add(component);
                    }
                }

                var dataComponent2 = db.Component.Where(t=>t.storage==storage);
                 
                foreach (Component component in components)
                {
                    var dataComponent = dataComponent2.FirstOrDefault(t => t.code == component.code); 
                    if (dataComponent != null)
                    {   
                        dataComponent.Name = component.Name;
                    }
                    else
                    {
                        db.Component.Add(component);
                    } 
                }
                
                db.SaveChanges();

                //    upload.SaveAs(Server.MapPath("~/Files/" + fileName));
            }
            return RedirectToAction("Index");
        }

        // POST: Components/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,code,storage")] Component component)
        {
            if (ModelState.IsValid)
            {
                db.Entry(component).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.storage = new SelectList(db.Storage, "Id", "Name", component.storage);
            return View(component);
        }

        // GET: Components/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Component.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            return View(component);
        }

        // POST: Components/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Component component = db.Component.Find(id);
            db.Component.Remove(component);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
