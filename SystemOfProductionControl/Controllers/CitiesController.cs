﻿/*
 *  Не используется на данный момент
 *  ("Продажи"?) используется в _Layout (основное окно)
 */

using SystemOfProductionControl.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class CitiesController : Controller
    {
        private Entities db = new Entities();

        // GET: Cities
        public ActionResult Index()
        {
            return View(db.Cities.ToList());
        }

        // GET: Cities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cities Cities = db.Cities.Find(id);
            if (Cities == null)
            {
                return HttpNotFound();
            }
            return View(Cities);
        }

        // GET: Cities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cities/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] Cities Cities)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(Cities);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Cities);
        }

        // GET: Cities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cities Cities = db.Cities.Find(id);
            if (Cities == null)
            {
                return HttpNotFound();
            }
            return View(Cities);
        }

        // POST: Cities/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Cities Cities)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Cities).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Cities);
        }

        // GET: Cities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cities Cities = db.Cities.Find(id);
            if (Cities == null)
            {
                return HttpNotFound();
            }
            return View(Cities);
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cities Cities = db.Cities.Find(id);
            db.Cities.Remove(Cities);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
















        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
