﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SystemOfProductionControl.Models;
using System.Net;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class ProductTypesController : Controller
    {
        private Entities db = new Entities();

        // GET: ProductTypes
        [Authorize]
        [Authorize]
        public ActionResult Index()
        {
            return View(db.ProductType.ToList());
        }

        // GET: ProductTypes/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductType ProductType = db.ProductType.Find(id);
            if (ProductType == null)
            {
                return HttpNotFound();
            }
            return View(ProductType);
        }

        // GET: ProductTypes/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductTypes/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,CoefficientPrice")] ProductType ProductType, String coeff)
        {
            //if (upload != null)
            //{
            //    // получаем имя файла
            //    string fileName = System.IO.Path.GetFileName(upload.FileName);
            //    // сохраняем файл в папку Files в проекте
            //    String path = Server.MapPath("~/Content/ImgUpload/" + fileName);
            //    upload.SaveAs(path);

            //}

            float Data = 0;


            try
            {
                coeff = coeff.Trim(' ').Replace('.', ',');
                Data = float.Parse(coeff);
            }
            catch
            {

            }

            ProductType.CoefficientPrice = Data;

            if (ModelState.IsValid)
            { 
                db.ProductType.Add(ProductType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(ProductType);
        }

        // GET: ProductTypes/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductType ProductType = db.ProductType.Find(id);
            if (ProductType == null)
            {
                return HttpNotFound();
            }
            return View(ProductType);
        }

        // POST: ProductTypes/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] ProductType ProductType, String coeff)
        {
            float Data = 0; 
            try
            {
                coeff = coeff.Trim(' ').Replace('.', ',');
                Data = float.Parse(coeff);
            }
            catch
            {

            }

            ProductType.CoefficientPrice = Data;

            if (ModelState.IsValid)
            {
                db.Entry(ProductType).State = EntityState.Modified;
                db.SaveChanges();
                   return RedirectToAction("Index", "Reference");
            }
            return View(ProductType);
        }

        // GET: ProductTypes/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductType ProductType = db.ProductType.Find(id);
            if (ProductType == null)
            {
                return HttpNotFound();
            }
            return View(ProductType);
        }



        public ActionResult AddPriceOfProductType(String id, String data, String dataId)
        {
            List<string> rez = new List<string>();
            rez.Add(dataId);

            if (id == "") return null;

            int Id = Int32.Parse(id);
            float Data = 0;


            try
            {
                data = data.Trim(' ').Replace('.', ','); 
                Data = float.Parse(data);
            }
            catch
            {
                
            }

            ProductType ProductType = db.ProductType.Find(Id);

            if (ProductType != null)
            {

                ProductType.CoefficientPrice = Data;
                db.SaveChanges();
            }

           String s= Data.ToString("0.00");
            rez.Add(s);

            return Json(rez, JsonRequestBehavior.AllowGet);
        }

        // POST: ProductTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductType ProductType = db.ProductType.Find(id);
            db.ProductType.Remove(ProductType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
