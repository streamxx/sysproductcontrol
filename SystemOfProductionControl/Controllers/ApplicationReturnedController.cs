﻿/* Тест-Драйв */

using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SystemOfProductionControl.Models;
using SystemOfProductionControl.Services;

namespace SystemOfProductionControl.Controllers
{
    public class ApplicationReturnedController : Controller
    {
        private Entities db = new Entities();

        // GET: ApplicationReturned
        public ActionResult Index()
        {
            var applicationReturned = db.ApplicationReturned.Include(a => a.Company1);
            return View(applicationReturned.ToList());
        }

        // GET: ApplicationReturned/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationReturned applicationReturned = db.ApplicationReturned.Find(id);
            if (applicationReturned == null)
            {
                return HttpNotFound();
            }
            return View(applicationReturned);
        }

        // GET: ApplicationReturned/Create
        public ActionResult Create()
        {

            var dbCity = db.Cities.OrderBy(t => t.Name);
            var city = new SelectList(dbCity, "ID", "Name");
            ViewBag.City = city;
            int idCity = dbCity.FirstOrDefault().ID;

            var CompanyQuery = (from d in db.Company
                    where d.City == idCity
                    select new
                    {
                        ID = d.Id,
                        Name = d.Name + " ИНН:" + d.INN
                    }

                ).AsEnumerable();


            var CompanyQueryTrim = (from d in CompanyQuery

                    select new
                    {
                        d.ID,
                        Name = d.Name.TrimStart("ООО".ToCharArray()).TrimStart("НО".ToCharArray()).TrimStart("ИП".ToCharArray()).TrimStart("ОАО".ToCharArray()).TrimStart("ЗАО".ToCharArray()).TrimStart(' ').TrimStart('"')
                    }

                );

            ViewBag.Companies = new SelectList(CompanyQueryTrim.OrderBy(t => t.Name), "ID", "Name");
            return View();
        }

        // POST: ApplicationReturned/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int CityID, String CityNew, int CompanyID, String CompanyName, String CompanyPhone,String DocNumber, DateTime DateCreate, DateTime DateCompletion)
        {


            if (CityNew != "")
            { 
                Cities Cities = db.Cities.FirstOrDefault(t => t.Name == CityNew);


                if (Cities == null)
                {
                    Cities = new Cities();
                    Cities.Name = CityNew;

                    var C = db.Cities.Add(Cities);
                    db.SaveChanges();
                    CityID = C.ID;
                }
                else
                {

                    CityID = Cities.ID;
                }
            }


            if (CompanyName != "")
            {
                Company company = db.Company.FirstOrDefault(t => t.Name == CompanyName && t.City == CityID);

                if (company == null)
                {

                    company = new Company();
                    company.Name = CompanyName;
                    company.Phone = CompanyPhone;
                    company.City = CityID;
                    var C = db.Company.Add(company);
                    db.SaveChanges();
                    CompanyID = C.Id;
                }
                else
                {
                    CompanyID = company.Id;
                }
            }


            if (DocNumber != null)
            {

                if (DocNumber.TrimEnd(' ') != "")
                {

                    int num = 0;

                   
                    try
                    {
                        num = int.Parse(DocNumber);
                    }
                    catch (Exception)
                    {
                        
                    }

                    if (num != 0)
                    {
                        ApplicationReturnedService ApplicationReturnedService = new ApplicationReturnedService(db);
                        ApplicationReturned applicationReturned = ApplicationReturnedService.CreateApplicationReturned(num,CompanyID, DateCreate, DateCompletion);
                         
                        if (applicationReturned != null)
                        {
                            return RedirectToAction("Edit", new { id = applicationReturned.ID });
                        }
                    }
                     
                } 
            }


            var dbCity = db.Cities.OrderBy(t => t.Name);
            var city = new SelectList(dbCity, "ID", "Name");
            ViewBag.City = city;
            int idCity = dbCity.FirstOrDefault().ID;
            var CompanyQuery = (from d in db.Company
                                           where d.City == idCity
                                           select new
                                           {
                                               ID = d.Id,
                                               Name = d.Name + " ИНН:" + d.INN
                                           }
                ).AsEnumerable();
            var CompanyQueryTrim = (from d in CompanyQuery

                                            select new
                                            {
                                                d.ID,
                                                Name = d.Name.TrimStart("ООО".ToCharArray()).TrimStart("НО".ToCharArray()).TrimStart("ИП".ToCharArray()).TrimStart("ОАО".ToCharArray()).TrimStart("ЗАО".ToCharArray()).TrimStart(' ').TrimStart('"')
                                            }

                );

            ViewBag.Companies = new SelectList(CompanyQueryTrim.OrderBy(t => t.Name), "ID", "Name");


            return View();
        }

        // GET: ApplicationReturned/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationReturned applicationReturned = db.ApplicationReturned.Find(id);
            if (applicationReturned == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
        
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", applicationReturned.Company);
            var additional = db.AdditionalAgreement.OrderByDescending(t => t.CompletionDate)
                .FirstOrDefault(t => t.ApplicationReturnedId == applicationReturned.ID);

            if(additional!=null)
            ViewBag.CompletionDate = additional.CompletionDate;


            Models.ApplicationReturned_namespace.ApplicationReturnedEditVmModel applicationReturnedEditVmModel = new Models.ApplicationReturned_namespace.ApplicationReturnedEditVmModel(applicationReturned);

            return View(applicationReturnedEditVmModel);
        }

        // POST: ApplicationReturned/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Company,DateCreate,Status")] ApplicationReturned applicationReturned)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationReturned).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
     
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", applicationReturned.Company);
            Models.ApplicationReturned_namespace.ApplicationReturnedEditVmModel applicationReturnedEditVmModel = new Models.ApplicationReturned_namespace.ApplicationReturnedEditVmModel(applicationReturned);
            return View(applicationReturnedEditVmModel);
        }



        // GET: ApplicationReturned/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationReturned applicationReturned = db.ApplicationReturned.Find(id);
            if (applicationReturned == null)
            {
                return HttpNotFound();
            }
            return View(applicationReturned);
        }

        // POST: ApplicationReturned/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationReturned applicationReturned = db.ApplicationReturned.Find(id);


           ApplicationReturnedService ApplicationReturnedService = new ApplicationReturnedService(db);
           ApplicationReturnedService.Delete(applicationReturned); 
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
