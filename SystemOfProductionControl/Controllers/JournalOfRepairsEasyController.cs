﻿
/*
 *      Не задействован
 * 
 */
using System.Data;
using System.Data.Entity;
using SystemOfProductionControl.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SystemOfProductionControl.other;

namespace SystemOfProductionControl.Controllers
{
    public class JournalOfRepairsEasyController : Controller
    {
        private Entities db = new Entities();

        // GET: JournalOfRepairsEasy
        public ActionResult Index()
        {


                // var warningTable = journalOfRepair.JournalToWarningOrComponent
                //.Where(t => t.Type == (int)BaseEnums.JournalToWarningOrComponentOrProduct.Warning)
                //.Join(db.WarningType, p => p.IDWarningOrComponent, t => t.ID, (p, t) => new { ID = t.ID, Name = t.Name });

            var journalOfRepair = db.JournalOfRepair.Include(j => j.Act1).Include(j => j.AspNetUsers)
                .Include(j => j.ProductType)
                .Include(j => j.RepairComments);
            
            //.Include(j => j.WarningType1);
            return View(journalOfRepair.ToList());
        }

        // GET: JournalOfRepairsEasy/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepair);
        }

        // GET: JournalOfRepairsEasy/Create
        public ActionResult Create()
        {
            ViewBag.Act = new SelectList(db.Act, "ID", "Name");
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Company = new SelectList(db.Company, "Id", "Name");
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name");
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text");
            ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name");
            return View();
        }

        // POST: JournalOfRepairsEasy/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,City,Company,TypeProduct,NumberBlock,WarningType,WarningComments,DateOfDelivery,LoginId,Act,DateOfSale")] JournalOfRepair journalOfRepair)
        {
            if (ModelState.IsValid)
            {
                db.JournalOfRepair.Add(journalOfRepair);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Act = new SelectList(db.Act, "ID", "Name", journalOfRepair.Act);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);
 
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.WarningComments);


          var warningTable =  journalOfRepair.JournalToWarningOrComponent
                .Where(t => t.Type == (int) BaseEnums.JournalToWarningOrComponentOrProduct.Warning)
                .Join(db.WarningType, p => p.IDWarningOrComponent, t => t.ID, (p, t) => new { t.ID, t.Name });

            ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name", warningTable);
            return View(journalOfRepair);
        }

        // GET: JournalOfRepairsEasy/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            ViewBag.Act = new SelectList(db.Act, "ID", "Name", journalOfRepair.Act);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);
            
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.WarningComments);


            var warningTable = journalOfRepair.JournalToWarningOrComponent
           .Where(t => t.Type == (int)BaseEnums.JournalToWarningOrComponentOrProduct.Warning)
           .Join(db.WarningType, p => p.IDWarningOrComponent, t => t.ID, (p, t) => new { t.ID, t.Name });

            ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name", warningTable);
            return View(journalOfRepair);
        }

        // POST: JournalOfRepairsEasy/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,City,Company,TypeProduct,NumberBlock,WarningType,WarningComments,DateOfDelivery,LoginId,Act,DateOfSale")] JournalOfRepair journalOfRepair)
        {
            if (ModelState.IsValid)
            {
                db.Entry(journalOfRepair).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Act = new SelectList(db.Act, "ID", "Name", journalOfRepair.Act);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);
 
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.WarningComments);


            var warningTable = journalOfRepair.JournalToWarningOrComponent
                .Where(t => t.Type == (int)BaseEnums.JournalToWarningOrComponentOrProduct.Warning)
                .Join(db.WarningType, p => p.IDWarningOrComponent, t => t.ID, (p, t) => new { t.ID, t.Name });
            ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name", warningTable);
            return View(journalOfRepair);
        }

        // GET: JournalOfRepairsEasy/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepair);
        }

        // POST: JournalOfRepairsEasy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            db.JournalOfRepair.Remove(journalOfRepair);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
