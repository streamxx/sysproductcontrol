﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class WarningTypeFromUsersController : Controller
    {
        private Entities db = new Entities();

        // GET: WarningTypeFromUsers
        public ActionResult Index()
        {
            return View(db.WarningTypeFromUsers.ToList());
        }

        // GET: WarningTypeFromUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningTypeFromUsers warningTypeFromUsers = db.WarningTypeFromUsers.Find(id);
            if (warningTypeFromUsers == null)
            {
                return HttpNotFound();
            }
            return View(warningTypeFromUsers);
        }

        // GET: WarningTypeFromUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WarningTypeFromUsers/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] WarningTypeFromUsers warningTypeFromUsers)
        {
            if (ModelState.IsValid)
            {
                db.WarningTypeFromUsers.Add(warningTypeFromUsers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(warningTypeFromUsers);
        }

        // GET: WarningTypeFromUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningTypeFromUsers warningTypeFromUsers = db.WarningTypeFromUsers.Find(id);
            if (warningTypeFromUsers == null)
            {
                return HttpNotFound();
            }
            return View(warningTypeFromUsers);
        }

        // POST: WarningTypeFromUsers/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] WarningTypeFromUsers warningTypeFromUsers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(warningTypeFromUsers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(warningTypeFromUsers);
        }

        // GET: WarningTypeFromUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningTypeFromUsers warningTypeFromUsers = db.WarningTypeFromUsers.Find(id);
            if (warningTypeFromUsers == null)
            {
                return HttpNotFound();
            }
            return View(warningTypeFromUsers);
        }

        // POST: WarningTypeFromUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WarningTypeFromUsers warningTypeFromUsers = db.WarningTypeFromUsers.Find(id);
            db.WarningTypeFromUsers.Remove(warningTypeFromUsers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
