﻿/*
 *      Для ТестДрайва
 * 
 */
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class AdditionalAgreementsController : Controller
    {
        private readonly Entities db = new Entities();

        // GET: AdditionalAgreements
        public ActionResult Index(int IDApplication)
        {
            var additionalAgreement = db.AdditionalAgreement.Include(a => a.ApplicationReturned).Where(t => t.ApplicationReturnedId == IDApplication);

            ApplicationReturned applicationReturned = db.ApplicationReturned.Find(IDApplication);

            ViewBag.application = applicationReturned;

            return View(additionalAgreement.ToList());
        }

        // GET: AdditionalAgreements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            AdditionalAgreement additionalAgreement = db.AdditionalAgreement.Find(id);
            return additionalAgreement == null ? HttpNotFound() : (ActionResult)View(additionalAgreement);
        }

        // GET: AdditionalAgreements/Create
        public ActionResult Create()
        {
            ViewBag.ApplicationReturnedId = new SelectList(db.ApplicationReturned, "ID", "ID");
            return View();
        }

        public ActionResult AddAdditional(int IDApplication, DateTime dateComplition, String docName)
        {
            AdditionalAgreement additionalAgreement = new AdditionalAgreement
            {
                ApplicationReturnedId = IDApplication,
                DateCreate = DateTime.Now.AddHours(4),
                CompletionDate = dateComplition,
                DocumentName = docName
            };

            db.AdditionalAgreement.Add(additionalAgreement);
            db.SaveChanges();

            return Redirect("index?IDApplication=" + IDApplication.ToString());
        }

        // POST: AdditionalAgreements/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ApplicationReturnedId,DocumentName,CompletionDate")] AdditionalAgreement additionalAgreement)
        {
            if (ModelState.IsValid)
            {
                db.AdditionalAgreement.Add(additionalAgreement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApplicationReturnedId = new SelectList(db.ApplicationReturned, "ID", "ID", additionalAgreement.ApplicationReturnedId);
            return View(additionalAgreement);
        }

        // GET: AdditionalAgreements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            AdditionalAgreement additionalAgreement = db.AdditionalAgreement.Find(id);
            if (additionalAgreement == null) return HttpNotFound();
            ViewBag.ApplicationReturnedId = new SelectList(db.ApplicationReturned, "ID", "ID", additionalAgreement.ApplicationReturnedId);
            return View(additionalAgreement);
        }

        // POST: AdditionalAgreements/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ApplicationReturnedId,DocumentName,CompletionDate")] AdditionalAgreement additionalAgreement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(additionalAgreement).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect("../Index?IDApplication=" + additionalAgreement.ApplicationReturnedId.ToString());
            }
            ViewBag.ApplicationReturnedId = new SelectList(db.ApplicationReturned, "ID", "ID", additionalAgreement.ApplicationReturnedId);
            return View(additionalAgreement);
        }

        // GET: AdditionalAgreements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return db.AdditionalAgreement.Find(id) != null ? (ActionResult)View(db.AdditionalAgreement.Find(id)) : HttpNotFound();
        }

        // POST: AdditionalAgreements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AdditionalAgreement additionalAgreement = db.AdditionalAgreement.Find(id);
            int IDApplication = additionalAgreement.ApplicationReturnedId;

            db.AdditionalAgreement.Remove(additionalAgreement);
            db.SaveChanges();
            return Redirect("../Index?IDApplication=" + IDApplication.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}
