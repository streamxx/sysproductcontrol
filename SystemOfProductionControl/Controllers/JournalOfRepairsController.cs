﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using SystemOfProductionControl.Models;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class JournalOfRepairsController : Controller
    {
        private readonly Entities db = new Entities();

        // GET: JournalOfRepairs
        public ActionResult Index(int state = 0)
        {
            var departmentsQuery = (from d in db.WarningType
                                    select d).ToList<WarningType>();

            departmentsQuery.Add(new WarningType { ID = -1, Name = "Нет" });


            ViewBag.TypeWarningID = new SelectList(departmentsQuery.OrderBy(t => t.ID), "ID", "Name");

            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers, "ID", "Name");
            var dbCity = db.Cities.OrderBy(t => t.Name);
            var city = new SelectList(dbCity, "ID", "Name");
            ViewBag.City = city;

            int idCity = dbCity.FirstOrDefault().ID;

            var CompanyQuery = (from d in db.Company
                                where d.City == idCity
                                select new
                                {
                                    ID = d.Id,
                                    Name = d.Name + " ИНН:" + d.INN
                                }

            ).AsEnumerable();


            var CompanyQueryTrim = (from d in CompanyQuery

                                    select new
                                    {
                                        d.ID,
                                        Name = d.Name
                                        .TrimStart("ООО".ToCharArray())
                                        .TrimStart("НО".ToCharArray())
                                        .TrimStart("ИП".ToCharArray())
                                        .TrimStart("ОАО".ToCharArray())
                                        .TrimStart("ЗАО".ToCharArray())
                                        .TrimStart(' ').TrimStart('"')
                                    }

      );

            ViewBag.Companies = new SelectList(CompanyQueryTrim.OrderBy(t => t.Name), "ID", "Name");
            //    ViewBag.ActList = new SelectList(db.Act.Where(t => !t.Ready).OrderByDescending(t => t.ID).Take(100).Select((t,s)=>(t.ID)), "ID", "Name"); 
            var act = db.Act.Where(t => !t.Ready).Include(a => a.Cities).Include(a => a.Company1).OrderByDescending(t => t.ID).Take(100);



            //.TrimStart("Новый ".ToCharArray()).TrimStart("ООО".ToCharArray()).TrimStart(' ')


            var date2 = (from d in db.Act.OrderByDescending(t => t.ID).Take(100).AsEnumerable()
                         where !d.Ready
                         select new
                         {
                             d.ID,
                             Name = d.Name
                             .TrimStart("Новый ".ToCharArray())
                             .TrimStart("ООО".ToCharArray())
                             .TrimStart("НО".ToCharArray())
                             .TrimStart("ИП".ToCharArray())
                             .TrimStart("ОАО".ToCharArray())
                             .TrimStart("ЗАО".ToCharArray())
                             .TrimStart(' ').TrimStart('"') + " " + d.DateCreate.ToString("dd-MM:hh:mm")

                         }

            );
            //foreach (var item in date2 )
            //{
            //    item.Name = item.Name.TrimStart("Новый ".ToCharArray()).TrimStart("ООО".ToCharArray()).TrimStart(' ');
            //}


            var data = new SelectList(date2, "ID", "Name");


            //foreach (var item in data)
            //{
            //    item.Value = item.Value.TrimStart("Новый ".ToCharArray()).TrimStart("ООО".ToCharArray()).TrimStart(' ');
            //}

            ViewBag.ActList = data;


            List<journalOfRepairIndexModel> journalOfRepairIndexModel = new List<journalOfRepairIndexModel>();

            foreach (var item in act)
            {

                journalOfRepairIndexModel journalOfRepairIndexModelItem = new journalOfRepairIndexModel(item);

                var journal = db.JournalOfRepair.Where(t => t.Act == item.ID).GroupBy(t => t.TypeProduct).Select(x => new { type = x.Key, count = x.Count() });

                Dictionary<int, int> products = new Dictionary<int, int>();

                int a = db.JournalOfRepair.Count(t => t.Act == item.ID && t.DateOfDelivery == null);
                int b = db.JournalOfRepair.Count(t => t.Act == item.ID && t.DateOfDelivery != null); ;

                foreach (var jitem in journal)
                {
                    products.Add(jitem.type, jitem.count);

                }

                if (a == 0 && b != 0)
                {
                    journalOfRepairIndexModelItem.Shipment = 3;
                }
                else
                    if (a != 0 && b == 0)
                {
                    journalOfRepairIndexModelItem.Shipment = 1;

                }
                else if (a != 0 && b != 0)
                {
                    journalOfRepairIndexModelItem.Shipment = 2;
                }


                foreach (var productsitem in products)
                {
                    journalOfRepairIndexModelItem.Count += productsitem.Value.ToString() + " | ";
                    journalOfRepairIndexModelItem.BlocksType += db.ProductType.First(t => t.ID == productsitem.Key).Name + " | ";
                }

                journalOfRepairIndexModelItem.Count = journalOfRepairIndexModelItem.Count.TrimEnd(' ').TrimEnd('|');
                journalOfRepairIndexModelItem.BlocksType = journalOfRepairIndexModelItem.BlocksType.TrimEnd(' ').TrimEnd('|');

                if (item.Comment != null)
                    journalOfRepairIndexModelItem.TextComments = db.RepairComments.First(t => t.ID == item.Comment).Text;
                journalOfRepairIndexModel.Add(journalOfRepairIndexModelItem);
            }

            if (state != 0)
            {
                journalOfRepairIndexModel[0].state = state;
            }

            return View(journalOfRepairIndexModel);


            //var journalOfRepair = db.JournalOfRepair.Include(j => j.Cities).Include(j => j.Company1).Include(j => j.ProductType).Include(j => j.RepairComments).Include(j => j.WarningType1).Include(j => j.AspNetUsers);
            //return View(journalOfRepair.ToList());
        }


        [HttpGet]
        public ActionResult SetReplays(bool replays, string Idjournal)
        {
            int ID = int.Parse(Idjournal);

            JournalOfRepair journal = db.JournalOfRepair.Find(ID);

            if (journal != null)
            {
                journal.Replace = replays;
                db.SaveChanges();
            }

            return null;
        }


        public ActionResult SetWaitingComponent(string Id)
        {
            int ID = Int32.Parse(Id);
            JournalOfRepair journal = db.JournalOfRepair.Find(ID);
            if (journal != null)
            {
                journal.WaitingComponent = !journal.WaitingComponent;
                db.SaveChanges();
            }

            return null;
        }

        [HttpGet]
        public ActionResult AjaxGetCompany(int ID)
        {
            var CompanyQuery = (from d in db.Company
                                where d.City == ID
                                select new
                                {
                                    ID = d.Id,
                                    Name = d.Name + " ИНН:" + d.INN
                                }
            ).AsEnumerable();
            var CompanyQueryTrim = from d in CompanyQuery
                                   select new
                                   {
                                       d.ID,
                                       Name = d.Name
                                   .TrimStart("ООО".ToCharArray())
                                   .TrimStart("НО".ToCharArray())
                                   .TrimStart("ИП".ToCharArray())
                                   .TrimStart("ОАО".ToCharArray())
                                   .TrimStart("ЗАО".ToCharArray())
                                   .TrimStart(' ')
                                   .TrimStart('"')
                                   };
            SelectList CompaniesSelectList = new SelectList(CompanyQueryTrim.OrderBy(t => t.Name), "ID", "Name");
            return Json(CompaniesSelectList, JsonRequestBehavior.AllowGet);
        }




        [HttpGet]
        public ActionResult AjaxTest(string name)
        {
            int d = int.Parse(name);
            var departmentsQuery = db.WarningType.Where(t => t.ProductType.ID == d || t.ProductType == null).ToList<WarningType>();
            departmentsQuery.Add(new WarningType { ID = -1, Name = "Нет" });
            SelectList TypeWarningID = new SelectList(departmentsQuery.OrderBy(t => t.ID), "ID", "Name");
            return Json(TypeWarningID, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult AjaxGetActInfo(string Id)
        {
            int d = int.Parse(Id);
            Act a = db.Act.FirstOrDefault(t => t.ID == d);



            ActAjaxModel model = new ActAjaxModel(a);

            //if (a.Comment != null)
            //    model.TextComments = db.RepairComments.FirstOrDefault(t => t.ID == a.Comment)?.Text;

            //if (a.UserProblemComments!= null)
            //    model.TextUserProblem = db.RepairComments.FirstOrDefault(t => t.ID == a.UserProblemComments)?.Text;

            return Json(model, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult AjaxGetComment(string Id)
        {
            int d = int.Parse(Id);
            RepairComments a = db.RepairComments.Where(t => t.ID == d).First();

            return Json(a, JsonRequestBehavior.AllowGet);

        }


        public ActionResult IndexAdmin()
        {
            var journalOfRepair = db.JournalOfRepair.Include(j => j.ProductType).Include(j => j.RepairComments).Include(j => j.AspNetUsers);
            return View(journalOfRepair.ToList());
        }


        // GET: JournalOfRepairs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepair);
        }

        // GET: JournalOfRepairs/Create

        public ActionResult Create()
        {
            //ViewBag.LoginId = new SelectList(db.AspNetRoles, "Id", "Name");
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Company = new SelectList(db.Company, "Id", "Name");
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name");
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers, "ID", "Name");
            ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name");
            ViewBag.ActList = new SelectList(db.Act, "ID", "Name");
            return View();
        }

        // POST: JournalOfRepairs/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOfIndex(int CityID, string CityNew, int CompanyID, string CompanyName, string CompanyPhone, int? Act, int TypeProductID, DateTime? DataOfReceipt, int TypeWarningID, int? WarningTypeFromUsers, string[] NumberBlock, int?[] DateOfSaleMM, int?[] DateOfSaleYY, string WarningComments, DateTime? DateOfDelivery, int? Ch0, int? Ch1, int? Ch2, int? Ch3)
        {

            //UserProblemComments ,WarningComments ,LoginId
            //[Bind(Include = "DataOfReceipt,City,Company,TypeProduct,WarningType,DateOfDelivery,DateOfShipment")]


            DateTime?[] DateOfSale = null;


            if (DateOfSaleMM != null && DateOfSaleYY != null)
            {
                DateOfSale = new DateTime?[DateOfSaleMM.Length];

                for (int i = 0; i < DateOfSaleMM.Length; i++)
                {
                    DateTime? dateTime = null;
                    if (DateOfSaleMM[i] != null && DateOfSaleYY[i] != null)
                    {
                        dateTime = new DateTime(DateOfSaleYY[i].Value, DateOfSaleMM[i].Value, 1);

                    }
                    else if (DateOfSaleYY[i] != null)
                    {

                        dateTime = new DateTime(DateOfSaleYY[i].Value, 1, 1);
                    }

                    DateOfSale[i] = dateTime;
                }

            }


            bool boolWarranty = false;

            if (Ch0 != null)
            {
                boolWarranty = true;
            }




            if (CityNew != "")
            {
                Cities Cities;
                Cities = new Cities
                {
                    Name = CityNew
                };

                var C = db.Cities.Add(Cities);
                db.SaveChanges();
                CityID = C.ID;
            }




            if (CompanyName != "")
            {
                Company company = new Company();
                company.Name = CompanyName;
                company.Phone = CompanyPhone;
                company.City = CityID;

                var C = db.Company.Add(company);
                db.SaveChanges();
                CompanyID = C.Id;
            }



            Act act = null;
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                      .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            Application application = null;
            DateTime? dateCopy = null;
            if (Act != null)
            {

                act = db.Act.FirstOrDefault(t => t.ID == Act);
                //application = db.Application.FirstOrDefault(t => t.IdGuaranty == act.ID || t.IdNoGuaranty == act.ID);
                dateCopy = act.DateCreate;


                if (act.Warranty != null)
                {
                    if (act.Warranty.Value != boolWarranty)
                    {


                        Act = null;
                        if (application != null)
                        {

                            if (boolWarranty)
                            {
                                //if (application.IdGuaranty != null)
                                //{
                                //    act = db.Act.FirstOrDefault(t => t.ID == application.IdGuaranty);
                                //    Act = act.ID;
                                //} 


                            }
                            else
                            {
                                //if (application.IdNoGuaranty != null)
                                //{
                                //    act = db.Act.FirstOrDefault(t => t.ID == application.IdNoGuaranty);
                                //    Act = act.ID;
                                //}
                            }
                        }

                    }
                }
                else
                {
                    act.Warranty = boolWarranty;

                    if (application != null)
                    {
                        if (boolWarranty)
                        {
                            // application.IdGuaranty = act.ID;

                        }
                        else
                        {
                            // application.IdNoGuaranty = act.ID;
                        }

                    }
                }



            }

            if (Act == null)
            {
                act = new Act();
                DateTime date = DateTime.Now.AddHours(4); ;
                //date=date.AddHours(4);
                if (dateCopy == null)
                {
                    if (DataOfReceipt != null)
                    {
                        date = (DateTime)DataOfReceipt;
                        var d = DateTime.Now;

                        if (date.Date.Equals(d.Date))
                        {
                            date = date.AddHours(d.Hour + 4);
                            date = date.AddMinutes(d.Minute);
                        }
                    }
                }
                else
                    date = dateCopy.Value;


                Company company = db.Company.First(t => t.Id == CompanyID);

                var name = "Новый " + company.Name.TrimEnd(' ');
                if (boolWarranty)
                    name += " гар.";
                // name +=" "+date.ToString("dd-MM:hh:mm");
                act.Name = name;
                act.DateCreate = date;
                act.City = CityID;
                act.Company = CompanyID;
                act.Warranty = boolWarranty;
                //act.UserId = user.Id;

                db.Act.Add(act);

                if (application == null)
                {
                    application = new Application();
                    db.Application.Add(application);
                }



                db.SaveChanges();
                Act = act.ID;

                if (boolWarranty)
                {
                    // application.IdGuaranty = act.ID;

                }
                else
                {
                    //  application.IdNoGuaranty = act.ID;
                }
                db.SaveChanges();
            }

            //if (Ch1 != null)
            // {
            //        act.replace = true;
            //    } else
            //{
            //    act.replace = false;
            //}

            //if (Ch2 != null)
            //    {
            //        act.repair = true;
            //    }
            //else
            //{
            //    act.repair = false;
            //}

            //if (Ch3 != null)
            //    {
            //        act.diagnostic = true;
            //    } else
            //{
            //    act.diagnostic = false;
            //}



            int Count = 1;
            if (NumberBlock != null)
                Count = NumberBlock.Length;


            for (int i = 0; i < Count; i++)
            {

                //string Act,int TypeProductID, DateTime? DataOfReceipt, int TypeWarningID,  string[] NumberBlock, DateTime?[] DateOfSale,

                JournalOfRepair journalOfRepair = new JournalOfRepair();

                journalOfRepair.LoginId = user.Id;
                journalOfRepair.WarningTypeFromUser = WarningTypeFromUsers;
                if (Act != null)
                    journalOfRepair.Act = Act;


                journalOfRepair.TypeProduct = TypeProductID;

                if (DataOfReceipt == null)
                    DataOfReceipt = (DateTime.Now).AddHours(4);

                if (DateOfDelivery != null)
                    journalOfRepair.DateOfDelivery = DateOfDelivery;

                if (TypeWarningID != -1)
                    // journalOfRepair.WarningType = TypeWarningID;
                    journalOfRepair.NumberBlock = NumberBlock[i];

                if (DateOfSale[i] != null)
                    journalOfRepair.DateOfSale = DateOfSale[i];
                db.JournalOfRepair.Add(journalOfRepair);
                db.SaveChanges();





                int WarningCommentsId = -1;


                if (journalOfRepair.WarningComments != null)
                    WarningCommentsId = (int)journalOfRepair.WarningComments;

                if (WarningComments != null)
                {

                    RepairComments Warning;

                    if (WarningCommentsId == -1)
                    {
                        if (WarningComments != "")
                        {
                            Warning = new RepairComments();
                            Warning.Text = WarningComments;
                            var d = db.RepairComments.Add(Warning);
                            db.SaveChanges();
                            WarningCommentsId = d.ID;
                        }
                    }
                    else
                    {
                        Warning = journalOfRepair.RepairComments;
                        Warning.Text = WarningComments;
                    }


                }


                if (WarningCommentsId != -1)
                {
                    journalOfRepair.WarningComments = WarningCommentsId;
                }

                db.SaveChanges();
            }

            return Redirect("Index?state=1");

            //   return RedirectToAction("Index");

        }






        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DataOfReceipt,City,Company,TypeProduct,WarningType,DateOfDelivery,DateOfShipment")] JournalOfRepair journalOfRepair, string[] NumberBlock, string[] DateOfSale, string UserProblemComments, string WarningComments)
        {

            //UserProblemComments ,WarningComments ,LoginId




            if (ModelState.IsValid)
            {
                if (UserProblemComments != "")
                {

                }

                if (WarningComments != "")
                {


                }

                ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
                ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
                journalOfRepair.LoginId = user.Id;
                db.JournalOfRepair.Add(journalOfRepair);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LoginId = new SelectList(db.AspNetRoles, "Id", "Name", journalOfRepair.LoginId);

            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.WarningComments);
            //ViewBag.UserProblemComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.UserProblemComments);
            ///ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name", journalOfRepair.WarningType);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);
            return View(journalOfRepair);
        }

        // GET: JournalOfRepairsEasy/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            ViewBag.Act = new SelectList(db.Act, "ID", "Name", journalOfRepair.Act);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);

            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);

            ViewBag.WarningComments = null;

            if (journalOfRepair.RepairComments != null)
            {
                ViewBag.WarningComments = journalOfRepair.RepairComments.Text;
            }
            //if(journalOfRepair.WarningType!=null) 
            //   ViewBag.WarningType = new SelectList(db.WarningType.Where(t=>t.ProductTypeID==journalOfRepair.TypeProduct|| t.ProductTypeID==null), "ID", "Name", journalOfRepair.WarningType);
            else
            {
                ViewBag.WarningType = new SelectList(db.WarningType.Where(t => t.ProductTypeID == journalOfRepair.TypeProduct || t.ProductTypeID == null), "ID", "Name");
            }
            return View(journalOfRepair);
        }

        // POST: JournalOfRepairsEasy/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TypeProduct,NumberBlock,DateOfDelivery,WarningComments,LoginId,Act,DateOfSale")] JournalOfRepair journalOfRepair, int id, string WarningCommentsText)
        {
            if (ModelState.IsValid)
            {
                db.Entry(journalOfRepair).State = EntityState.Modified;
                db.SaveChanges();

                if (WarningCommentsText != null)
                    if (WarningCommentsText != "")
                    {
                        if (journalOfRepair.WarningComments != null)
                        {
                            RepairComments repairComments = db.RepairComments.FirstOrDefault(t => t.ID == journalOfRepair.WarningComments);
                            repairComments.Text = WarningCommentsText;


                        }
                        else
                        {
                            RepairComments repairComments = new RepairComments
                            {
                                Text = WarningCommentsText
                            };
                            db.RepairComments.Add(repairComments);
                            db.SaveChanges();
                            journalOfRepair.WarningComments = repairComments.ID;

                        }


                        db.SaveChanges();
                    }

                //   int id = journalOfRepair.Act1.Application.FirstOrDefault().ID; // хуйня, а не код

                var ActID = db.JournalOfRepair.Where(t => t.ID == id).FirstOrDefault().Act;
                int dd = journalOfRepair.Act1.ID;


                // var id = db.Application.Where(t => t.ID == x).Select(t=>t.ID).FirstOrDefault();

                id = dd;
                return RedirectToAction("Edit", "Applications", new { id });

            }
            ViewBag.Act = new SelectList(db.Act, "ID", "Name", journalOfRepair.Act);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journalOfRepair.LoginId);

            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfRepair.TypeProduct);
            ViewBag.WarningComments = new SelectList(db.RepairComments, "ID", "Text", journalOfRepair.WarningComments);
            //ViewBag.WarningType = new SelectList(db.WarningType, "ID", "Name", journalOfRepair.WarningType);
            return View(journalOfRepair);
        }


        [HttpGet]
        public ActionResult DeleteAll(int[] idArray)
        {
            if (idArray == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


            if (idArray.Length == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }




            var s = db.JournalOfRepair.Where(t => idArray.Contains(t.ID));

            return View(s.ToList());
        }



        [HttpPost, ActionName("DeleteAll")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAllConfirmed(int[] idArray)
        {

            var s = db.JournalOfRepair.Where(t => idArray.Contains(t.ID));
            int actId = s.FirstOrDefault().Act.Value;
            Act act = db.Act.Find(actId);
            Application application = act.Application.FirstOrDefault();

            db.JournalOfRepair.RemoveRange(s);
            db.SaveChanges();


            return RedirectToAction("Edit", "Applications", new { id = application.ID });
        }


        // GET: JournalOfRepairs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);
            if (journalOfRepair == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepair);
        }


        // POST: JournalOfRepairs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            int actId = 0;

            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(id);

            if (journalOfRepair != null)
            {
                actId = journalOfRepair.Act.Value;

                var s = journalOfRepair.JournalOfRepairToComponent;
                var f = journalOfRepair.JournalToWarningOrComponent;

                journalOfRepair.JournalOfRepairToComponent.Clear();
                journalOfRepair.JournalToWarningOrComponent.Clear();
                db.SaveChanges();

                db.JournalOfRepairToComponent.RemoveRange(s);
                db.JournalToWarningOrComponent.RemoveRange(f);
                db.JournalOfRepair.Remove(journalOfRepair);
            }
            db.SaveChanges();

            Act act = db.Act.Find(actId);

            Application application = act.Application.FirstOrDefault();


            if (application != null)
            {
                if (application.Type == 0)
                {
                    return RedirectToAction("Edit", "Applications", new { id = application.ID });
                }
                else if (application.Type == 1)
                {
                    return RedirectToAction("EditReturn", "Applications", new { id = application.ID });
                }
                else if (application.Type == 2)
                {
                    return RedirectToAction("EditOFF", "Applications", new { id = application.ID });
                }

            }

            return RedirectToAction("Index");


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
