﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SystemOfProductionControl.Services;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class ActOfTransfersController : Controller
    {
        private Entities db = new Entities();

        // GET: ActOfTransfers
        public ActionResult Index()
        {
            return View(db.ActOfTransfer.ToList());
        }

        // GET: ActOfTransfers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);
            if (actOfTransfer == null)
            {
                return HttpNotFound();
            }
            return View(actOfTransfer);
        }

        // GET: ActOfTransfers/Create
        public ActionResult Create(int? IDAppRet)
        {
            if (IDAppRet is null)
            {
                throw new ArgumentNullException(nameof(IDAppRet));
            }
            else
            {
                ApplicationReturned applicationReturned = db.ApplicationReturned.Find(IDAppRet);
                if (applicationReturned == null) return Redirect("Index");
                int? num = IDAppRet;
                ActOfTransfer actOfTransfer = new ActOfTransferService(db).CreateActOfTransfer(num.Value);
                applicationReturned.ActOfTransfer.Add(actOfTransfer);
                db.SaveChanges();
                return Redirect("Edit?id=" + actOfTransfer.ID.ToString());
            }
        }

        public ActionResult AddProduct(int idAct, int TypeProductID,
            string[] NumberBlock, int?[] DateOfSaleMM, int?[] DateOfSaleYY,
            string WarningComments, string Price)
        {
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(idAct);

            if (actOfTransfer == null) return Redirect("Edit?id=" + actOfTransfer.ID.ToString());
            float price = 0;

            try
            {
                Price = Price.Trim(' ');
                Price = Price.Replace('.', ',');
                price = float.Parse(Price);
            }
            catch (Exception)
            {

            }
            int Count = 1;
            if (NumberBlock != null) Count = NumberBlock.Length;
            DateTime?[] DateOfSale = null;


            if (DateOfSaleMM != null && DateOfSaleYY != null)
            {
                DateOfSale = new DateTime?[DateOfSaleMM.Length];
                for (int i = 0; i < DateOfSaleMM.Length; i++)
                {
                    DateTime? dateTime = null;
                    if (DateOfSaleMM[i] != null && DateOfSaleYY[i] != null)
                    {
                        dateTime = new DateTime(DateOfSaleYY[i].Value, DateOfSaleMM[i].Value, 1);
                    }
                    else if (DateOfSaleYY[i] != null)
                    {
                        dateTime = new DateTime(DateOfSaleYY[i].Value, 1, 1);
                    }

                    DateOfSale[i] = dateTime;
                }

            }

            ActOfTransferService actOfTransferService = new ActOfTransferService(db);
            actOfTransferService.AddProducts(actOfTransfer, Count, TypeProductID, NumberBlock, DateOfSale, price, WarningComments);
            return Redirect("Edit?id=" + actOfTransfer.ID.ToString());
        }


        // GET: ActOfTransfers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);
            if (actOfTransfer == null)
            {
                return HttpNotFound();
            }

            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            ApplicationReturned apl = actOfTransfer.ApplicationReturned.FirstOrDefault();
            ViewBag.Application = apl;
            ViewBag.Company = apl.Company1;
            return View(actOfTransfer);
        }

        // POST: ActOfTransfers/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DateCreate,Administrative,TransferMail")] ActOfTransfer actOfTransfer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actOfTransfer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(actOfTransfer);
        }

        // GET: ActOfTransfers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);
            if (actOfTransfer == null)
            {
                return HttpNotFound();
            }
            return View(actOfTransfer);
        }



        public ActionResult SetTransferMail(String actID, String text)
        {
            int act = int.Parse(actID);
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(act);
            actOfTransfer.TransferMail = text;
            db.SaveChanges();
            return null;
        }

        public ActionResult SetAdministrativ(String actID, String text)
        {
            int act = int.Parse(actID);
            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(act);
            actOfTransfer.Administrative = text;
            db.SaveChanges();
            return null;
        }



        public ActionResult ReportSpecification(int? id)
        {

            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);
            ApplicationReturned applicationReturned = actOfTransfer.ApplicationReturned.FirstOrDefault();

            ViewBag.Dogovor = applicationReturned.ID.ToString();
            ViewBag.DateCreate = applicationReturned.DateCreate;
            ViewBag.Company = applicationReturned.Company1;

            return View(actOfTransfer);

        }

        public ActionResult ReportAct(int? id)
        {

            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);
            ApplicationReturned applicationReturned = actOfTransfer.ApplicationReturned.FirstOrDefault();

            ViewBag.Dogovor = applicationReturned.ID.ToString();
            ViewBag.DateCreate = applicationReturned.DateCreate;
            ViewBag.Company = applicationReturned.Company1;

            return View(actOfTransfer);

        }
        // POST: ActOfTransfers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(id);

            int idApl = actOfTransfer.ApplicationReturned.FirstOrDefault().ID;

            ActOfTransferService actOfTransferService = new ActOfTransferService(db);
            actOfTransferService.Delete(actOfTransfer);


            return Redirect("../ApplicationReturned/Edit?id=" + idApl.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
