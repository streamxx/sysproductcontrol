﻿/*
 Экспорт в CSV , пока не ясно, зачем это
 */


using System;
using System.Collections.Generic;
using System.Data.Entity;
using SystemOfProductionControl.Models;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SystemOfProductionControl.Controllers
{
    public class JournalsController : Controller
    {
        private Entities db = new Entities();

        // GET: Journals
        public ActionResult Index()
        {
            var journal = db.Journal.Include(j => j.Comments).Include(j => j.WarningType).Include(j => j.ProductType);
            return View(journal.ToList());
        }

        // GET: Journals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Journal journal = db.Journal.Find(id);
            if (journal == null)
            {
                return HttpNotFound();
            }
            return View(journal);
        }

        // GET: Journals/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(db.Comments, "ID", "Text");
            ViewBag.TypeWarningID = new SelectList(db.WarningType, "ID", "Name");
            ViewBag.TypeLoginID = new SelectList(db.AspNetRoles, "Id", "Name");
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.TypeProductID = new SelectList(db.ProductType, "ID", "Name");
            
            return View();
        }

        // POST: Journals/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TypeProductID,TypeWarningID")] Journal journal)
        {
            IList<string> roles = new List<string> { "Роль не определена" };
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                             .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            if (user != null)
                userManager.GetRoles(user.Id);

            var n = userManager.GetRoles(user.Id)[0]; ;
                    var firstOrDefault = db.AspNetRoles.FirstOrDefault(t => t.Name == n);
                    if (firstOrDefault != null)
                        journal.TypeLoginID = firstOrDefault.Id;
               

                journal.Data = DateTime.Now.AddHours(4);

            journal.LoginId = user.Id;
           // journal.



            if (ModelState.IsValid)
            {
                db.Journal.Add(journal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
          
            //  ViewBag.ID = new SelectList(db.Comments, "ID", "Text", journal.ID);
            ViewBag.TypeWarningID = new SelectList(db.WarningType, "ID", "Name", journal.TypeWarningID);

           // ViewBag.TypeLoginID = new SelectList(db.AspNetRoles, "Id", "Name", userManager.getRO);
            //ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email",User.Identity.GetUserId());
            ViewBag.TypeProductID = new SelectList(db.ProductType, "ID", "Name", journal.TypeProductID);
            return View(journal);
        }

        // GET: Journals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Journal journal = db.Journal.Find(id);
            if (journal == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(db.Comments, "ID", "Text", journal.ID);
            ViewBag.TypeWarningID = new SelectList(db.WarningType, "ID", "Name", journal.TypeWarningID);
            ViewBag.TypeLoginID = new SelectList(db.AspNetRoles, "Id", "Name", journal.TypeLoginID);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journal.LoginId);
            ViewBag.TypeProductID = new SelectList(db.ProductType, "ID", "Name", journal.TypeProductID);
            return View(journal);
        }

        // POST: Journals/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TypeProductID,Data,LoginId,TypeLoginID,TypeWarningID,IDProduct")] Journal journal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(journal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(db.Comments, "ID", "Text", journal.ID);
            ViewBag.TypeWarningID = new SelectList(db.WarningType, "ID", "Name", journal.TypeWarningID);
            ViewBag.TypeLoginID = new SelectList(db.AspNetRoles, "Id", "Name", journal.TypeLoginID);
            ViewBag.LoginId = new SelectList(db.AspNetUsers, "Id", "Email", journal.LoginId);
            ViewBag.TypeProductID = new SelectList(db.ProductType, "ID", "Name", journal.TypeProductID);
            return View(journal);
        }

        // GET: Journals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Journal journal = db.Journal.Find(id);
            if (journal == null)
            {
                return HttpNotFound();
            }
            return View(journal);
        }

        // POST: Journals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Journal journal = db.Journal.Find(id);
            db.Journal.Remove(journal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
