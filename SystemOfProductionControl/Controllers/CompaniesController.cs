﻿/*
 *  Не используется на данный момент
 *  ("Продажи"?) используется в _Layout (основное окно)
 */



using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl.Models;
using SystemOfProductionControl.other;

namespace SystemOfProductionControl.Controllers
{
    public class CompaniesController : Controller
    {
        private Entities db = new Entities();

        // GET: Companies
        public ActionResult Index()
        {
            var company = db.Company.Include(c => c.Cities);
            return View(company.ToList());
        }

        // GET: Companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Company.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }


        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                // получаем имя файла
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                // сохраняем файл в папку Files в проекте

                CSVReader reader = new CSVReader(upload.InputStream, Encoding.Default);
                string[] headers = reader.GetCSVLine();

                string[] data;

               // List<Component> components = new List<Component>();
                while ((data = reader.GetCSVLine()) != null)
                {
                    if (data[0] != "" && data[1] != "")
                    {
                        String name = data[0];
                        String Cytiname = data[1];
                        Company company =
                            db.Company.FirstOrDefault(t => t.Name.Contains(name));

                        Cities Cities = db.Cities.FirstOrDefault(t => t.Name.Contains(Cytiname));

                        if (Cities==null) continue;

                        if (company == null)
                        {
                            company = new Company();
                            company.Name = data[0];
                            company.City = Cities.ID;
                            db.Company.Add(company);
                        }
                        else
                        { 
                            company.Name = data[0];
                            company.City = Cities.ID;
                        }



                    }
                }

                db.SaveChanges();


                //    upload.SaveAs(Server.MapPath("~/Files/" + fileName));
            }
            return RedirectToAction("Index");
        }


        // GET: Companies/Create
        public ActionResult Create()
        {
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            return View();
        }

        // POST: Companies/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,City,Phone")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Company.Add(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.City = new SelectList(db.Cities, "ID", "Name", company.City);
            return View(company);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Company.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", company.City);
            return View(company);
        }

        // POST: Companies/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,City,Phone,INN,KPP")] Company company)
        {
             

            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", company.City);
            return View(company);
        }

        // GET: Companies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Company.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company = db.Company.Find(id);
            db.Company.Remove(company);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void SetINN(int companyId, String text)
        {
            Company company = db.Company.Find(companyId);
            company.INN = text.TrimEnd(' ');
            db.SaveChanges();

        }

        public void SetKPP(int companyId, String text)
        {

            Company company = db.Company.Find(companyId);
            company.KPP = text.TrimEnd(' ');
            db.SaveChanges();

        }

        public void SetPhone(int companyId, String text)
        {
            Company company = db.Company.Find(companyId);
            company.Phone = text.TrimEnd(' ');
            db.SaveChanges();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
