﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using SystemOfProductionControl.Models;
using System.Web.Mvc;
using SystemOfProductionControl.other;

namespace SystemOfProductionControl.Controllers
{
    public class JournalOfRepairToComponentsController : Controller
    {
        private Entities db = new Entities();

        // GET: JournalOfRepairToComponents
        public ActionResult Index()
        {
            var journalOfRepairToComponent = db.JournalOfRepairToComponent.Include(j => j.JournalOfRepair);
            return View(journalOfRepairToComponent.ToList());
        }

        // GET: JournalOfRepairToComponents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepairToComponent journalOfRepairToComponent = db.JournalOfRepairToComponent.Find(id);
            if (journalOfRepairToComponent == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepairToComponent);
        }

        // GET: JournalOfRepairToComponents/Create
        public ActionResult Create()
        {
            ViewBag.JournalOfRepairID = new SelectList(db.JournalOfRepair, "ID", "NumberBlock");
            return View();
        }

        // POST: JournalOfRepairToComponents/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ComponentOrProductID,JournalOfRepairID,Price,Size,Type")] JournalOfRepairToComponent journalOfRepairToComponent)
        {
            if (ModelState.IsValid)
            {
                db.JournalOfRepairToComponent.Add(journalOfRepairToComponent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.JournalOfRepairID = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfRepairToComponent.JournalOfRepairID);
            return View(journalOfRepairToComponent);
        }

        // GET: JournalOfRepairToComponents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepairToComponent journalOfRepairToComponent = db.JournalOfRepairToComponent.Find(id);
            if (journalOfRepairToComponent == null)
            {
                return HttpNotFound();
            }
            ViewBag.JournalOfRepairID = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfRepairToComponent.JournalOfRepairID);
            return View(journalOfRepairToComponent);
        }

        // POST: JournalOfRepairToComponents/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ComponentOrProductID,JournalOfRepairID,Price,Size,Type")] JournalOfRepairToComponent journalOfRepairToComponent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(journalOfRepairToComponent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.JournalOfRepairID = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfRepairToComponent.JournalOfRepairID);
            return View(journalOfRepairToComponent);
        }

        // GET: JournalOfRepairToComponents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfRepairToComponent journalOfRepairToComponent = db.JournalOfRepairToComponent.Find(id);
            if (journalOfRepairToComponent == null)
            {
                return HttpNotFound();
            }
            return View(journalOfRepairToComponent);
        }

        // POST: JournalOfRepairToComponents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JournalOfRepairToComponent journalOfRepairToComponent = db.JournalOfRepairToComponent.Find(id);
            db.JournalOfRepairToComponent.Remove(journalOfRepairToComponent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult GetComponent()
        {
            var Comp = db.Component.ToList().Select(t=>new { t.ID,Name = "Код:" + t.code + " " + t.Name.TrimStart(' ').TrimEnd(' ') }).ToList();
            
        
            var Products = db.Products.ToList().Select(t => new { ID = t.Id, Name = "АБРМ:" + t?.ABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();

            Comp.AddRange(Products);
           

            SelectList d = new SelectList(Comp, "ID", "Name");

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetComponentOFfilter(String Name,  String productId)
        {


            SelectList d = null;
            int proId = int.Parse(productId);

            if (Name == "частое")
            {

                ProductType Product = db.ProductType.Include(t=>t.ProductTypeToComponentHistory).FirstOrDefault(t => t.ID == proId);

                if (Product != null)
                {
                    var jcomphistprodtype =
                        Product.ProductTypeToComponentHistory.ToList();


                  var CompProductModels =   jcomphistprodtype.Select(t => new CompProductModel(t)).ToList();


                    var ProductHistory = CompProductModels
                        .Where(t => t.Type == 1).Select(t =>
                            new
                            {
                                t.ID,
                                Name = "АБРМ:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ')
                            }).ToList();
                    var ProductBuf = CompProductModels
                        .Where(t => t.Type == 0).Select(t =>
                            new
                            {
                                t.ID,
                                Name = "Код:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ')
                            }).ToList();
                    ProductHistory.AddRange(ProductBuf);
                    d = new SelectList(ProductHistory, "ID", "Name");
                }
            }
            else if(Name == "продукция")
            {
                var product =   db.Products.ToList().Select(t => new { ID = t.Id, Name = "АБРМ:" + t?.ABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();
                d = new SelectList(product, "ID", "Name");
            }
            else
            { 
                if (BaseEnums.ComponentsGroupfilter.ContainsKey(Name))
                {
                  List<string> list =  BaseEnums.ComponentsGroupfilter[Name];

                    List<Component> components = new List<Component>();

                    //foreach (var itemfilter in list)
                    //{ 
                    //  var s =  db.Component.Where(t => t.Name.ToLower().Contains(itemfilter)).ToList();
                    //    components.AddRange(s); 
                    //}


                    components = db.Component.Where(t => list.Any(x=>t.Name.ToLower().Contains(x))).ToList();

                    var Comp = components.GroupBy(t=>t).Select(t => new { t.Key.ID, Name = "Код:" + t.Key.code + " " + t.Key.Name.TrimStart(' ').TrimEnd(' ') }).ToList();
                    d = new SelectList(Comp, "ID", "Name"); 
                }

            }
             

           // var Comp = db.Component.ToList().Select(t => new { ID = t.ID, Name = "Код:" + t.code + " " + t.Name.TrimStart(' ').TrimEnd(' ') }).ToList();
         
            return Json(d, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
