﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using SystemOfProductionControl.Models;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl.Models.App_namespace;
using SystemOfProductionControl.Models.JournalOfrepair;
using SystemOfProductionControl.other;
using SystemOfProductionControl.Services;
namespace SystemOfProductionControl.Controllers
{
    public class ApplicationsController : Controller
    {
        private readonly Entities db = new Entities();
        // GET: Applications
        public ActionResult Index(string sortOrder,
        string currentFilter,
        string SearchType,
        string searchString,
        int? page,
        int? type,
        int? pageSize)
        {
            List<SelectListItem> DDown_Search = new List<SelectListItem>
                {
                    new SelectListItem{ Text="Города", Value = "Cities" },
                    new SelectListItem{ Text="Организации", Value = "Firms" },
                    new SelectListItem{ Text="ID", Value = "ID" }
                };
            SelectList selectdropdown = new SelectList(DDown_Search, "Value", "Text");

            List<SelectListItem> DDown_Type = new List<SelectListItem>
                {
                    new SelectListItem{ Text="Ремонт", Value = "0" },
                    new SelectListItem{ Text="Возврат", Value = "1" },
                    new SelectListItem{ Text="Списание", Value = "2" },
                    new SelectListItem{ Text="Всё", Value = "" }
                };

            SelectList selecttypedropdown = new SelectList(DDown_Type, "Value", "Text");
            ViewBag.selectdropdown = selectdropdown;
            ViewBag.selecttypedropdown = selecttypedropdown;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParm = string.IsNullOrEmpty(sortOrder) ? "ID_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.CompaniesSortParm = sortOrder == "Company" ? "Company_desc" : "Company";
            ViewBag.CitiesSortParm = sortOrder == "City" ? "City_desc" : "City";
            ViewBag.Type = type;
            ViewBag.pageSize = pageSize;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            IQueryable<Application> application = from s in db.Application select s;
            int searchint = 0;
            switch (SearchType)
            {
                case "ID":

                    if (!string.IsNullOrEmpty(searchString))
                    {
                        try
                        {
                            searchint = int.Parse(searchString);
                        }
                        catch (FormatException)
                        {
                            searchint = 0;
                        }
                        application = application.Where(s => s.ID == searchint);
                    }
                    break;
                case "Cities":

                    if (!string.IsNullOrEmpty(searchString))
                    {
                        application = application.Where(s => s.Act.FirstOrDefault().Company1.Cities.Name.Contains(searchString));
                    }
                    break;

                case "Firms":
                    if (!string.IsNullOrEmpty(searchString))
                    {
                        application = application.Where(s => s.Act.FirstOrDefault().Company1.Name.Contains(searchString));
                    }
                    break;
                default:
                    break;
            }
            ViewBag.SearchType = SearchType;
            switch (sortOrder)
            {
                case "ID":
                    application = application.OrderBy(s => s.ID);
                    break;
                case "ID_desc":
                    application = application.OrderByDescending(s => s.ID);
                    break;
                case "Date":
                    application = application.OrderBy(s => s.Data);
                    break;
                case "date_desc":
                    application = application.OrderByDescending(s => s.Data);
                    break;
                case "Company_desc":
                    application = application.OrderByDescending(s => s.Act.FirstOrDefault().Company1.Name);
                    break;
                case "Company":
                    application = application.OrderBy(s => s.Act.FirstOrDefault().Company1.Name);
                    break;
                case "City_desc":
                    application = application.OrderByDescending(s => s.Act.FirstOrDefault().Cities.Name);
                    break;
                case "City":
                    application = application.OrderBy(s => s.Act.FirstOrDefault().Cities.Name);
                    break;
                default:
                    application = application.OrderBy(s => s.ID);
                    break;
            }
            switch (type)
            {
                case 0:
                    application = application.Where(t => t.Type == 0);
                    break;
                case 1:
                    application = application.Where(t => t.Type == 1);
                    break;
                case 2:
                    application = application.Where(t => t.Type == 2);
                    break;
            }

            Dictionary<int, MailOfRepair> AppMailOfRepairs = new Dictionary<int, MailOfRepair>();
            Dictionary<int, Consignment> AppConsignments = new Dictionary<int, Consignment>();
            List<int> appID = application.Select(t => t.ID).ToList();
            List<ApplicationToLinkedDocument> appdocLst = db.ApplicationToLinkedDocument.ToList();

            foreach (var appdoc in appdocLst)
            {
                if (appdoc.Type == 0)
                {
                    MailOfRepair mailOfRepair = db.MailOfRepair.FirstOrDefault(t => t.ID == appdoc.IDDoc); // тоже под вопросом, но удалять не буду
                    if (mailOfRepair != null)
                    {
                        AppMailOfRepairs.Add(appdoc.IDApplication, mailOfRepair);
                    }
                }

                else if (appdoc.Type == 1)
                {
                    Consignment consignment = db.Consignment.FirstOrDefault(t => t.ID == appdoc.IDDoc);
                    if (consignment != null)
                    {
                        AppConsignments.Add(appdoc.IDApplication, consignment);
                    }
                }
            }
            ViewBag.AppMailOfRepairs = AppMailOfRepairs;
            ViewBag.AppConsignments = AppConsignments;
            List<Application> applications = application.ToList();
            if (pageSize == null || pageSize == 0) pageSize = 6;
            int pageNumber = (page ?? 1);
            return View(application.ToPagedList(pageNumber, pageSize.Value));
            // return View(await PaginatedList<Application>.CreateAsync(application.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public ActionResult IndexManufacture()
        {
            var application = db.Application
             .Include(a => a.Act)
            .OrderBy(s => s.ID)
            .Where(t => t.Act.FirstOrDefault().ActCondition.OnRepair == true);


            Dictionary<int, MailOfRepair> AppMailOfRepairs = new Dictionary<int, MailOfRepair>();
            Dictionary<int, Consignment> AppConsignments = new Dictionary<int, Consignment>();
            //var applicationId = application.Select(t => t.ID).ToList();
            var LinkedDocs = db.ApplicationToLinkedDocument.ToList();
            foreach (ApplicationToLinkedDocument ApplicationToLinkedDocument in LinkedDocs)
            {
                if (ApplicationToLinkedDocument.Type == 0)
                {
                    MailOfRepair mailOfRepair = db.MailOfRepair.FirstOrDefault(t => t.ID == ApplicationToLinkedDocument.IDDoc);
                    if (mailOfRepair != null) AppMailOfRepairs.Add(ApplicationToLinkedDocument.IDApplication, mailOfRepair);
                }
                else if (ApplicationToLinkedDocument.Type == 1)
                {
                    Consignment consignment = db.Consignment.FirstOrDefault(t => t.ID == ApplicationToLinkedDocument.IDDoc);
                    if (consignment != null) AppConsignments.Add(ApplicationToLinkedDocument.IDApplication, consignment);
                }
            }
            ViewBag.AppMailOfRepairs = AppMailOfRepairs;
            ViewBag.AppConsignments = AppConsignments;
            return View(application.ToList());
        }

        public ActionResult IndexOfRepair()
        {
            var application = db.Application
           .Where(t => t.Act.FirstOrDefault().ActCondition.OnRepair == true && t.Act.FirstOrDefault().ActCondition.ProductSent == false)
           .OrderByDescending(s => s.ID);
            return View(application.ToList());
        }

        public ActionResult IndexOfStorage()
        {

            var act = db.Act.OrderByDescending(a => a.ID);
            List<Act> acts = new List<Act>();
            List<Application> applications = new List<Application>();

            foreach (Act item in act)
            {
                JournalOfRepair jrep = item.JournalOfRepair.FirstOrDefault(t => t.Act == item.ID);
                if (jrep != null)
                {
                    var jrpcomp = jrep.JournalOfRepairToComponent.FirstOrDefault(t => t.JournalOfRepairID == jrep.ID);

                    if (
                        jrpcomp != null
                        && item.ActCondition.OnRepair == true
                        && item.ActCondition.IsRepairCompleted == false
                        && item.ActCondition.ProductSent == false

                       )

                    {
                        acts.Add(item);
                        applications.AddRange(item.Application.Where(t => t.Act.FirstOrDefault().ID == item.ID));
                    }
                }

            }

            return View(applications.ToList());
        }

        public ActionResult IndexOfBookkeeping()
        {
            return View(
             db.Application
            .Where(t => t.Act
           .FirstOrDefault().ActCondition.OnRepair == true &&
            t.Act
           .FirstOrDefault().ActCondition.ProductSent == false)
            .OrderByDescending(s => s.ID).ToList());
        }

        public ActionResult SetRepairMail(string applicationId, string text)
        {
            if (applicationId == "") return null;
            int Type = 0;
            int AppID = int.Parse(applicationId);
            text = text.Trim(' ');
            if (text == "") return null;
            ApplicationToLinkedDocument linked = db.ApplicationToLinkedDocument.FirstOrDefault(t => t.IDApplication == AppID && t.Type == Type);
            if (linked == null)
            {
                MailOfRepair mailOfRepair = new MailOfRepair
                {
                    Text = text.Trim(' ')
                };
                db.MailOfRepair.Add(mailOfRepair);
                db.SaveChanges();
                linked = new ApplicationToLinkedDocument
                {
                    Type = Type,
                    IDApplication = AppID,
                    IDDoc = mailOfRepair.ID
                };
                db.ApplicationToLinkedDocument.Add(linked);
                db.SaveChanges();
            }
            else
            {
                MailOfRepair mailOfRepair = db.MailOfRepair.FirstOrDefault(t => t.ID == linked.IDDoc);
                if (mailOfRepair == null)
                {
                    mailOfRepair = new MailOfRepair
                    {
                        Text = text.Trim(' ')
                    };
                    db.MailOfRepair.Add(mailOfRepair);
                    db.SaveChanges();
                    linked.IDDoc = mailOfRepair.ID;
                    db.ApplicationToLinkedDocument.Add(linked);
                    db.SaveChanges();
                }
                else
                {
                    mailOfRepair.Text = text.Trim(' ');
                    db.SaveChanges();
                }
            }
            return null;
        }
        public ActionResult SetConsignment(string applicationId, string text)
        {
            if (applicationId == "") return null;
            int Type = 1;
            int AppID = int.Parse(applicationId);
            text = text.Trim(' ');
            if (text == "") return null;
            ApplicationToLinkedDocument linked = db.ApplicationToLinkedDocument.FirstOrDefault(t => t.IDApplication == AppID && t.Type == Type);
            if (linked == null)
            {
                Consignment consignment = new Consignment
                {
                    Text = text.Trim(' ')
                };
                db.Consignment.Add(consignment);
                linked = new ApplicationToLinkedDocument
                {
                    Type = Type,
                    IDApplication = AppID,
                    IDDoc = consignment.ID
                };
                db.ApplicationToLinkedDocument.Add(linked);
                db.SaveChanges();
            }
            else
            {
                Consignment consignment = db.Consignment.FirstOrDefault(t => t.ID == linked.IDDoc);
                if (consignment == null)
                {
                    consignment = new Consignment
                    {
                        Text = text.Trim(' ')
                    };
                    db.Consignment.Add(consignment);
                    linked.IDDoc = consignment.ID;
                    db.ApplicationToLinkedDocument.Add(linked);
                    db.SaveChanges();
                }
                else
                {
                    consignment.Text = text.Trim(' ');
                    db.SaveChanges();
                }
            }
            return null;
        }
        public ActionResult DetailsOfStorage(int? id)
        {
            var application = db.Application.Find(id);
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var acts = application.Act.Select(t => (int?)t.ID);
            var journal1 = db.JournalOfRepair
            .Include(j => j.Act1)
            .Where(t => acts.Contains(t.Act));

            List<JournalOfRepairModel> jrep = journal1.ToList().Select(t => new JournalOfRepairModel(t))
            .ToList();
            ViewBag.journalOfRepair = jrep;
            var jrepcomp = db.JournalOfRepairToComponent
                .Where(t => journal1.Contains(t.JournalOfRepair))
                .ToList();

            IEnumerable<CompProductModel> compprod = jrepcomp.Select(t => new CompProductModel(t));

            var compprodgrp = compprod
                .GroupBy(x => x.Name)
                .Select(x => new { Name = x.Key, Sum = x.Sum(t => t.Size) })
                .ToList();

            var Components = new List<CompProductModel>();

            foreach (var item in compprodgrp)
            {
                CompProductModel jmodelname = compprod.FirstOrDefault(t => t.Name == item.Name);
                if (jmodelname != null)
                {
                    jmodelname.Size = item.Sum;
                    Components.Add(jmodelname);
                }
            }
            ViewBag.Component = Components;
            return View(application);
        }
        // GET: Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Application application = db.Application.Find(id);
            if (application == null) return HttpNotFound();
            return View(application);
        }
        // GET: Applications/Create
        public ActionResult Create()
        {
            var dbCity = db.Cities.OrderBy(t => t.Name);
            var city = new SelectList(dbCity, "ID", "Name");
            ViewBag.City = city;
            int idCity = dbCity.FirstOrDefault().ID;
            var CompanyQuery = (from d in db.Company
                                where d.City == idCity
                                select new
                                {
                                    ID = d.Id,
                                    Name = d.Name + " ИНН:" + d.INN
                                }
            ).AsEnumerable();
            var CompanyQueryTrim = from d in CompanyQuery
                                   select new
                                   {
                                       d.ID,
                                       Name = d.Name.TrimStart("ООО"
                                   .ToCharArray())
                                   .TrimStart("НО".ToCharArray())
                                   .TrimStart("ИП".ToCharArray())
                                   .TrimStart("ОАО".ToCharArray())
                                   .TrimStart("ЗАО".ToCharArray())
                                   .TrimStart(' ').TrimStart('"')
                                   };
            ViewBag.Companies = new SelectList(CompanyQueryTrim.OrderBy(t => t.Name), "ID", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int CityID, string CityNew, int CompanyID, string CompanyName, string CompanyPhone, int apptype)
        {

            if (CityNew != "")
            {
                Cities Cities = db.Cities.FirstOrDefault(t => t.Name == CityNew);
                if (Cities == null)
                {
                    Cities = new Cities
                    {
                        Name = CityNew
                    };
                    var C = db.Cities.Add(Cities); // what?????!!!!
                    db.SaveChanges();
                    CityID = C.ID;
                }
                else CityID = Cities.ID;

            }
            if (CompanyName != "")
            {
                Company company = db.Company.FirstOrDefault(t => t.Name == CompanyName);
                if (company == null)
                {
                    company = new Company
                    {
                        Name = CompanyName,
                        Phone = CompanyPhone,
                        City = CityID
                    };
                    Company Compa = db.Company.Add(company); //what?!
                    db.SaveChanges();
                    CompanyID = Compa.Id;
                }
                else CompanyID = company.Id;
            }
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            Application application = CreateApplication(apptype, user.Id);
            TempData["CityID"] = CityID;
            TempData["CompanyID"] = CompanyID;
            //  application.Act.Add(new ActServices(db).CreateAct(CityID, CompanyID)); // I want to move from this

            if (ModelState.IsValid)
            {
                db.Application.Add(application);
                db.SaveChanges();

                RedirectToRouteResult result = null;

                switch (apptype)
                {
                    case 0:
                        result = RedirectToAction("Edit", new { id = application.ID });
                        break;
                    case 1:
                        result = RedirectToAction("EditReturn", new { id = application.ID });
                        break;
                    case 2:
                        result = RedirectToAction("EditOff", new { id = application.ID });
                        break;
                    default:
                        break;
                }


                return result;
            }


            var dbCity = db.Cities.OrderBy(t => t.Name);
            ViewBag.City = new SelectList(dbCity, "ID", "Name");
            int idCity = dbCity.FirstOrDefault().ID;
            var CompanyQuery = (from d in db.Company
                                where d.City == idCity
                                select new
                                {
                                    ID = d.Id,
                                    Name = d.Name + " ИНН:" + d.INN
                                }
            )
            .AsEnumerable();
            var TrimQuery = (from d in CompanyQuery
                             select new
                             {
                                 d.ID,
                                 Name = d.Name
                             .TrimStart("ООО".ToCharArray())
                             .TrimStart("НО".ToCharArray())
                             .TrimStart("ИП".ToCharArray())
                             .TrimStart("ОАО".ToCharArray())
                             .TrimStart("ЗАО".ToCharArray())
                             .TrimStart(' ')
                             .TrimStart('"')
                             }
            );
            ViewBag.Companies = new SelectList(TrimQuery.OrderBy(t => t.Name), "ID", "Name");
            return View();
        }
        private Application CreateApplication(int type, string userId, DateTime? date = null)
        {
            DateTime d = DateTime.Now;
            d = d.AddHours(4);
            if (date != null) d = date.Value;
            Application application = new Application
            {
                Data = d,
                Type = type,
                LoginId = userId
            };
            return application;
        }


        [HttpPost]
        public ActionResult
        AddProductOff(int? IDApplication, int? TypeProductID, int? WarningTypeFromUsers, string[] NumberBlock,
                      int?[] DateOfSaleMM, int?[] DateOfSaleYY, string WarningComments) =>
            (IDApplication == null) || (TypeProductID == null) || !AddProduct(IDApplication.Value, true, TypeProductID.Value, WarningTypeFromUsers, NumberBlock, DateOfSaleMM, DateOfSaleYY, WarningComments, DateTime.Now.AddHours(4))
                ? Redirect("/Applications/Index")
                : Redirect("/Applications/EditOff?id=" + IDApplication.ToString());


        [HttpPost]
        public ActionResult AddProduct(int? IDApplication, bool? Warranty, int? TypeProductID,
    int? WarningTypeFromUsers, string[] NumberBlock, int?[] DateOfSaleMM, int?[] DateOfSaleYY,
    string WarningComments)
        {

            if (Warranty == null) Warranty = false;
            return IDApplication == null || TypeProductID == null || !AddProduct(IDApplication.Value, Warranty.Value, TypeProductID.Value, WarningTypeFromUsers, NumberBlock, DateOfSaleMM, DateOfSaleYY, WarningComments)
                ? Redirect("/Applications/Index")
                : Redirect("/Applications/Edit?id=" + IDApplication.ToString());
        }


        public bool AddProduct(int IDApplication, bool Warranty, int TypeProductID,
      int? WarningTypeFromUsers, string[] NumberBlock, int?[] DateOfSaleMM, int?[] DateOfSaleYY,

      string WarningComments, DateTime? DateOfDelivery = null, int? productReverse = null)
        {
            Application application = db.Application.FirstOrDefault(t => t.ID == IDApplication);
            if (application == null) return false;
            Act act = application.Act.FirstOrDefault(t => t.Warranty == Warranty);
            if (act == null)
            {
                application.Act.Add(new ActServices(db).CreateAct((int)TempData["CityID"], (int)TempData["CompanyID"], Warranty));
                act = application.Act.FirstOrDefault(t => t.Warranty == Warranty);
                db.SaveChanges();
            }

            int Count = 1;
            if (NumberBlock != null) Count = NumberBlock.Length;

            DateTime?[] DateOfSale = null;
            if (DateOfSaleMM != null && DateOfSaleYY != null)
            {
                DateOfSale = new DateTime?[DateOfSaleMM.Length];
                for (int i = 0; i < DateOfSaleMM.Length; i++)
                {
                    DateTime? dateTime = null;
                    if (DateOfSaleMM[i] != null && DateOfSaleYY[i] != null)
                        dateTime = new DateTime(DateOfSaleYY[i].Value, DateOfSaleMM[i].Value, 1);
                    else if (DateOfSaleYY[i] != null)
                        dateTime = new DateTime(DateOfSaleYY[i].Value, 1, 1);
                    DateOfSale[i] = dateTime;
                }

            }
            new JournalOfRepairServices(db).AddJournals(TypeProductID, act, Count, WarningTypeFromUsers, NumberBlock, DateOfSale, WarningComments, DateOfDelivery, productReverse);
            db.SaveChanges();
            return true;
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Application application = db.Application.Find(id);
            if (application == null) return HttpNotFound();

            var jrepactdict = new List<JournalOfRepairModel>();

            foreach (Act act in application.Act)
            {
                // тут был инклуд с комментами
                if (db.JournalOfRepair.Where(t => t.Act == act.ID).Any())

                    jrepactdict
                        .AddRange(
                        db.JournalOfRepair
                        .Where(t => t.Act == act.ID)
                        .ToList()
                        .Select(s => new JournalOfRepairModel(s))
                        .ToList()) ;
            }

      
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers, "ID", "Name");
            ViewBag.Status = new SelectList(db.ApplicationStatus, "ID", "Name", application.Status);
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name"); // viewbag взятые прямиком из DB, а не классов. Зачем? what
            if (db.UserInfo.Find(application.LoginId) != null)
                ViewBag.User = db.UserInfo.Find(application.LoginId)?.LastName + " " + db.UserInfo.Find(application.LoginId)?.FirstName + " " + db.UserInfo.Find(application.LoginId)?.MiddleName;

            return View(new ApplicationEditVmModel(application, jrepactdict));
        }


        public ActionResult EditOfRepair(int? id)
        {
            Application application = db.Application.Find(id);
            if (id == null || application == null) return HttpNotFound();
            var acts = application.Act.Select(t => t.ID).ToList();
            var jrepair = db.JournalOfRepair.Where(t => acts.Contains(t.Act.Value)).ToList();
            //  ViewBag.jrepairOfRepair = jrepair;

            var j_grp_byProdType = jrepair.GroupBy(t => t.ProductType).ToList();
            Dictionary<int, SelectList> selectListWarning = new Dictionary<int, SelectList>();
            Dictionary<int, SelectList> HistoryListRepairComponent = new Dictionary<int, SelectList>();

            foreach (var item in j_grp_byProdType)
            {
                List<WarningType> WarningTypeList = (from d in db.WarningType select d).ToList(); // тут было непонятное условие
                List<CompProductModel> jcomphistprodtype = item.Key.ProductTypeToComponentHistory
                    .Select(t => new CompProductModel(t)).ToList();
                var ProductHistory = jcomphistprodtype.Where(t => t.Type == 1)
                    .Select(t => new { t.ID, Name = "АБРМ:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();
                var ProductBuf = jcomphistprodtype.Where(t => t.Type == 0)
                    .Select(t => new { t.ID, Name = "Код:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();
                ProductHistory.AddRange(ProductBuf);
                HistoryListRepairComponent.Add(item.Key.ID, new SelectList(ProductHistory.OrderBy(t => t.Name), "Name", "Name"));
                WarningTypeList.Add(new WarningType { ID = -1, Name = "Нет" });
                selectListWarning.Add(item.Key.ID, new SelectList(WarningTypeList.OrderBy(t => t.ID), "ID", "Name"));
            }
            List<string> Filter = new List<string> { "частое", "продукция" };
            Filter.AddRange(from text in BaseEnums.ComponentsGroupfilter select text.Key);
            ViewBag.Status = new SelectList(db.ApplicationStatus, "ID", "Name", application.Status);
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            ViewBag.TypeWarningID = selectListWarning;
            ViewBag.HistoryListRepairComponent = HistoryListRepairComponent;
            ViewBag.Filter = new SelectList(Filter);
            ViewBag.Journalofrepcomp = jrepair.Select(s => new JournalOfRepairAndComponentModel(s)).ToList();
            return View(application);
        }


        public ActionResult EditOFF(int? id)
        {
            Application application = db.Application.Find(id);
            if (id == null || application == null) return HttpNotFound();
            if (application == null) return HttpNotFound();
            List<JournalOfRepairAndComponentModel> JournalOfRepairs = new List<JournalOfRepairAndComponentModel>();
            var acts = application.Act.Select(t => t.ID).ToList();
            var journal = db.JournalOfRepair
            .Where(t => acts.Contains(t.Act.Value))
            .ToList();
            var groupOfProduct = journal.GroupBy(t => t.ProductType).ToList();
            Dictionary<int, SelectList> selectListWarning = new Dictionary<int, SelectList>();
            Dictionary<int, SelectList> HistoryListRepairComponent = new Dictionary<int, SelectList>();
            foreach (var item in groupOfProduct)
            {
                var WarningTypeList = (from d in db.WarningType
                                       where d.Name.Contains("Списание") || d.Name.Contains("Недокомплект")
                                       select d).ToList();
                var jcomphistprodtype = item.Key.ProductTypeToComponentHistory.Select(t => new CompProductModel(t)).ToList();
                var ProductHistory = jcomphistprodtype.Where(t => t.Type == 1).Select(t => new { t.ID, Name = "АБРМ:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();
                var ProductBuf = jcomphistprodtype.Where(t => t.Type == 0).Select(t => new { t.ID, Name = "Код:" + t?.CodeOrABRM?.TrimEnd(' ') + " " + t?.Name?.TrimStart(' ').TrimEnd(' ') }).ToList();
                ProductHistory.AddRange(ProductBuf);
                HistoryListRepairComponent.Add(item.Key.ID, new SelectList(ProductHistory.OrderBy(t => t.Name), "Name", "Name"));
                //WarningTypeList.Add(new WarningType { ID = -1, Name = "Нет" });
                selectListWarning.Add(item.Key.ID, new SelectList(WarningTypeList.OrderBy(t => t.ID), "ID", "Name"));
            }

            List<string> filter = new List<string> { "частое", "продукция" };
            foreach (var text in BaseEnums.ComponentsGroupfilter) filter.Add(text.Key);
            ViewBag.filter = new SelectList(filter);
            ViewBag.Status = new SelectList(db.ApplicationStatus, "ID", "Name", application.Status);
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            ViewBag.TypeWarningID = selectListWarning;
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers.Where(t => t.Name.Contains("Списание") || t.Name.Contains("Недокомплект")).OrderByDescending(t => t.Name), "ID", "Name");
            ViewBag.HistoryListRepairComponent = HistoryListRepairComponent;
            string user = "";
            UserInfo info = db.UserInfo.Find(application.LoginId);
            if (info != null) user = info?.LastName + " " + info?.FirstName + " " + info?.MiddleName;
            List<JournalOfRepairAndComponentModel> journalofrepcomp = journal.Select(s => new JournalOfRepairAndComponentModel(s)).ToList();
            ViewBag.User = user;
            ViewBag.Journalofrepcomp = journalofrepcomp;
            return View(application);
        }
        // GET: Applications/Edit/5
        public ActionResult EditReturn(int? id)
        {

            Application application = db.Application.Find(id);
            if (id == null || application == null) return HttpNotFound();
            List<JournalOfRepairModel> JournalOfRepairs = new  List<JournalOfRepairModel>();
            foreach (var act in application.Act)
            {
                var g = db.JournalOfRepair.Include(t => t.RepairComments).Where(t => t.Act == act.ID);
                List<JournalOfRepair> JournalOfRepairsjmodelname = new List<JournalOfRepair>();
                if (g.Any())
                    JournalOfRepairsjmodelname = g.ToList();
                List<JournalOfRepairModel> d = JournalOfRepairsjmodelname.Select(s => new JournalOfRepairModel(s)).ToList();
            //    JournalOfRepairs.Add( d);
            }
            ApplicationEditVmModel applicationEditVmModel = new ApplicationEditVmModel(application, JournalOfRepairs);
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers, "ID", "Name");
            ViewBag.Status = new SelectList(db.ApplicationStatus, "ID", "Name", application.Status);
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            ViewBag.Product = new SelectList(db.Products.OrderBy(t => t.Name), "ID", "Name");
            string user = "";
            UserInfo info = db.UserInfo.Find(application.LoginId);
            if (info != null) user = info?.LastName + " " + info?.FirstName + " " + info?.MiddleName;
            ViewBag.User = user;
            return View(applicationEditVmModel);
        }

        public ActionResult AddComponent(string name, string count, string Idjournal)
        {
            int IDjournal = int.Parse(Idjournal);
            int Count = int.Parse(count);
            if (Count == 0) Count = 1;
            int code = -1;
            if (name.Contains("Код:"))
            {
                name = name.TrimStart("Код:".ToCharArray());
                int countSpace = name.IndexOf(" ", StringComparison.Ordinal);
                if (countSpace != 0)
                {
                    code = int.Parse(name.Substring(0, countSpace));
                    name = name.Substring(countSpace + 1);
                }
            }
            string ABRM = "";
            if (name.Contains("АБРМ") || name.Contains("ABPM") || name.Contains("ABRM"))
            {
                name = name
               .TrimStart("АБРМ".ToCharArray())
               .TrimStart("ABPM".ToCharArray())
               .TrimStart("ABRM".ToCharArray())
               .TrimStart(":".ToCharArray())
               .TrimStart(".".ToCharArray());
                int countSpace = name.IndexOf(" ", StringComparison.Ordinal);
                if (countSpace != 0)
                {
                    ABRM = name.Substring(0, countSpace);
                    name = name.Substring(countSpace + 1);
                }
            }
            if (name != "")
            {
                Component component = null;
                Products products = null;

                products = db.Products.FirstOrDefault(t => t.Name.Contains(name) && t.ABRM.Contains(ABRM)); // тут лучше селектор, который активируется по Viewbag сделать, 

                // НЕОДНОЗНАЧНОСТЬ!

                if (code != -1)
                {
                    component = db.Component.FirstOrDefault(t => t.Name.Contains(name) && t.code == code);
                }
                int ID = -1;
                int Type = -1;
                if (component != null)
                {
                    ID = component.ID;
                    Type = 0;
                }
                else if (products != null) // уловка и не более, хотя логике не противоречит и действует
                {
                    ID = products.Id;
                    Type = 1;
                }
                else
                {
                    return Componentpartial(IDjournal);
                }

                JournalOfRepair journalOfRepair = db.JournalOfRepair.FirstOrDefault(t => t.ID == IDjournal);

                if (journalOfRepair != null)
                {
                    JournalOfRepairToComponent jrcomp = db.JournalOfRepairToComponent
                        .FirstOrDefault(t => t.ComponentOrProductID == ID && t.Type == Type && t.JournalOfRepairID == IDjournal);
                    if (jrcomp != null)
                    {
                        jrcomp.Size += Count;
                    }
                    else
                    {
                        jrcomp = new JournalOfRepairToComponent
                        {
                            ComponentOrProductID = ID,
                            Size = Count,
                            Type = Type,
                            JournalOfRepairID = journalOfRepair.ID
                        };
                        db.JournalOfRepairToComponent.Add(jrcomp);
                    }

                    db.SaveChanges();
                    ActServices ActServices = new ActServices(db);
                    Act act = db.Act.Find(journalOfRepair.Act);
                    act.ActCondition.Completed = false;
                    act.ActCondition.ProcessedStorage = false;
                    ActServices.SetActReadyCancel(act);  //
                    ActServices.SetActReadyStorageCancel(act);  // от этого надо избавляться what
                }

            }
            return Componentpartial(IDjournal);
        }


        public ActionResult AddWarning(string idWarning, string compname, string Idjournal)
        {
            int idW = -1;
            int IDjournal = int.Parse(Idjournal);
            if (idWarning != null && idWarning != "") idW = int.Parse(idWarning);
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(IDjournal);
            if (journalOfRepair != null)
            {
                if (compname != "")
                {
                    int code = -1;
                    if (compname.Contains("Код:"))
                    {
                        compname = compname.TrimStart("Код:".ToCharArray());
                        int countSpace = compname.IndexOf(" ", StringComparison.Ordinal);
                        if (countSpace != 0)
                        {
                            code = int.Parse(compname.Substring(0, countSpace));
                            compname = compname.Substring(countSpace + 1);
                        }
                    }
                    string АБРМ = "";
                    if (compname.Contains("АБРМ") || compname.Contains("ABPM") || compname.Contains("ABRM"))
                    {
                        compname = compname
                        .TrimStart("АБРМ".ToCharArray())
                        .TrimStart("ABPM".ToCharArray())
                        .TrimStart("ABPM".ToCharArray())
                        .TrimStart("ABRM".ToCharArray())
                        .TrimStart(":".ToCharArray())
                        .TrimStart(".".ToCharArray());
                        int countSpace = compname.IndexOf(" ", StringComparison.Ordinal);
                        if (countSpace != 0)
                        {
                            АБРМ = compname.Substring(0, countSpace);
                            compname = compname.Substring(countSpace + 1);
                        }
                    }
                    Component component = null;
                    Products products = null;
                    if (АБРМ != "") products = db.Products.FirstOrDefault(t => t.Name.Contains(compname) && t.ABRM.Contains(АБРМ));
                    if (code != -1) component = db.Component.FirstOrDefault(t => t.Name.Contains(compname) && t.code == code);
                    int ID = -1;
                    int Type = -1;
                    if (component != null)
                    {
                        ID = component.ID;
                        Type = 1;
                    }
                    else if (products != null)
                    {
                        ID = products.Id;
                        Type = 2;
                    }
                    if (component != null || products != null)
                    {
                        JournalToWarningOrComponent journalToWarningOrComponent = journalOfRepair.JournalToWarningOrComponent.FirstOrDefault(t => t.IDWarningOrComponent == ID && t.Type == Type);
                        if (journalToWarningOrComponent == null) AddJournalToWarningOrComponent(ID, journalOfRepair.ID, Type);
                    }
                }
                if (idW != -1)
                {
                    int ID = idW;
                    int Type = 0;
                    JournalToWarningOrComponent journalToWarningOrComponent =
                    journalOfRepair.JournalToWarningOrComponent.FirstOrDefault(t =>
                    t.IDWarningOrComponent == ID && t.Type == Type);
                    if (journalToWarningOrComponent == null)
                    {
                        AddJournalToWarningOrComponent(ID, journalOfRepair.ID, Type);
                    }
                }
            }
            return CreateTableWarnings(IDjournal);
        }
        public ActionResult RemoveComponent(string id, string type, string Idjournal)
        {
            int IDjournal = int.Parse(Idjournal);
            int idComp = int.Parse(id);
            int Type = int.Parse(type);
            var jrcomp =
            db.JournalOfRepairToComponent.FirstOrDefault(t => t.JournalOfRepairID == IDjournal && t.ComponentOrProductID == idComp && t.Type == Type);
            if (jrcomp != null)
            {
                db.JournalOfRepairToComponent.Remove(jrcomp);
                db.SaveChanges();
            }

            return PartialView("ComponentsTablePartialView", new JournalOfRepairAndComponentModel(db.JournalOfRepair.Find(IDjournal)).Components);
            // return Componentpartial(IDjournal);
        }
        public ActionResult Componentpartial(int Idjournal)
        {
            //  JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(Idjournal);
            // var model = new JournalOfRepairAndComponentModel(db.JournalOfRepair.Find(Idjournal));
            return PartialView("ComponentsTablePartialView", new JournalOfRepairAndComponentModel(db.JournalOfRepair.Find(Idjournal)).Components);
            //  return Json(PartialView("ComponentsTablePartialView", model.Components), JsonRequestBehavior.AllowGet); 
        }
        public ActionResult RemoveWarning(string idWarning, string type, string Idjournal)
        {
            int idW = int.Parse(idWarning);
            int IDjournal = int.Parse(Idjournal);
            int Type = int.Parse(type);
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(IDjournal);
            if (journalOfRepair != null)
            {
                JournalToWarningOrComponent journalToWarningOrComponent =
                journalOfRepair.JournalToWarningOrComponent.FirstOrDefault(t => t.IDWarningOrComponent == idW && t.Type == Type);
                if (journalToWarningOrComponent != null)
                {
                    db.JournalToWarningOrComponent.Remove(journalToWarningOrComponent);
                    db.SaveChanges();
                }
            }
            return CreateTableWarnings(IDjournal);
        }
        public void AddJournalToWarningOrComponent(int ID, int Idjournal, int Type)
        {
            JournalToWarningOrComponent journalToWarningOrComponent = new JournalToWarningOrComponent
            {
                IDJournal = Idjournal,
                IDWarningOrComponent = ID,
                Type = Type
            };
            db.JournalToWarningOrComponent.Add(journalToWarningOrComponent);
            db.SaveChanges();
        }
        public ActionResult CloseRepairAll(int applicationId)
        {
            Application application = db.Application.Find(applicationId);
            List<int?> actsID = application.Act.Select(t => (int?)t.ID).ToList();
            application.Act.ToList().ForEach(t => t.ActCondition.IsRepairCompleted = true);

            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            var jrep = db.JournalOfRepair.Where(t => actsID.Contains(t.Act)).ToList();
            foreach (var item in jrep) new JournalOfRepairServices(db).CloseRepair(item, user);
            db.SaveChanges();
            return RedirectToAction("EditOfRepair", "Applications", new { id = applicationId });
        }

        public ActionResult UnCloseRepairAll(int applicationId)
        {
            Application application = db.Application.Find(applicationId);
            List<int?> actsID = application.Act.Select(t => (int?)t.ID).ToList();
            application.Act.ToList().ForEach(t => t.ActCondition.IsRepairCompleted = false);

            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            var jrep = db.JournalOfRepair.Where(t => actsID.Contains(t.Act) && (t.DateOfDelivery == null)).ToList();
            foreach (var journalOfRepair in jrep)
                new JournalOfRepairServices(db).CloseRepair(journalOfRepair, user);
            db.SaveChanges();
            return RedirectToAction("EditOfRepair", "Applications", new { id = applicationId });
        }





        public ActionResult CreateTableWarnings(int Idjournal)
        {
            // var journalOfRepair = db.JournalOfRepair.Find(Idjournal);
            //JournalOfRepairAndComponentModel model = new JournalOfRepairAndComponentModel(db.JournalOfRepair.Find(Idjournal));
            return PartialView("WarningsTablePartialView", new JournalOfRepairAndComponentModel(db.JournalOfRepair.Find(Idjournal)).WarningList);
        }

        public ActionResult SetStatusOfRepair(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Application application = db.Application.Find(id);
            if (application == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var act = application.Act.FirstOrDefault(t => t.JournalOfRepair.Count == 0); // ???? what
            if (act != null)
            {
                application.Act.Remove(act);
                db.Act.Remove(act);
                db.SaveChanges();
            }
            var acts = application.Act
                .Select(t => (int?)t.ID)
                .ToList();
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            var journals = db.JournalOfRepair
                .Where(t => acts.Contains(t.Act) && (t.DateOfDelivery == null))
                .ToList();
            JournalOfRepairServices JournalOfRepairServices = new JournalOfRepairServices(db);
            foreach (var journalOfRepair in journals)
                JournalOfRepairServices.CloseRepair(journalOfRepair, user);
            application.Act.FirstOrDefault().ActCondition.OnRepair = true;
            application.Act.FirstOrDefault().ActCondition.IsRepairCompleted = false;
            application.Act.FirstOrDefault().ActCondition.ProcessedStorage = false;
            db.SaveChanges();
            return Redirect("Index");
        }

        public ActionResult SetStatusOfPendingSubmission(int? id)
        {
            Application application = db.Application.Find(id);
            if (application == null || id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            application.Act.FirstOrDefault().ActCondition.PendingSubmission = true;
            application.Act.FirstOrDefault().ActCondition.ProductSent = false;

            foreach (var act in application.Act)
                act.DateOfShipment = null;
            db.SaveChanges();
            return Redirect("Index");
        }
        public ActionResult SetStatusOfProductSent(int? id)
        {

            Application application = db.Application.Find(id);
            if (application == null || id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var s = application.ApplicationToLinkedDocument
                .FirstOrDefault(t => t.Type == 1);

            if (application.Type == 0 && s == null)
                return Redirect("Edit?id=" + id.Value.ToString());

            application.Act.FirstOrDefault().ActCondition.ProductSent = true;
            application.Act.FirstOrDefault().ActCondition.PendingSubmission = false;
            foreach (var act in application.Act)
            {
                act.DateOfShipment = DateTime.Now.AddHours(4);
            }
            db.SaveChanges();
            return Redirect("Index?type=" + application.Type.ToString());
        }

        public ActionResult Report4(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Act act = db.Act.Find(id);
            //var journal =
            //    db.JournalOfRepair.Where(t => t.Act == act.ID)
            //        .GroupBy(t => t.TypeProduct)
            //        .Select(x => new { type = x.Key, count = x.Count() });
            //Dictionary<int, int> products = new Dictionary<int, int>();
            //foreach (var jjmodelname in journal)
            //{
            //    products.Add(jjmodelname.type, jjmodelname.count);
            //}
            Application application = db.Application.Find(id);
            string user = "___________________";
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
            .GetUserManager<ApplicationUserManager>();
            ApplicationUser userwe = userManager.FindByEmail(User.Identity.Name);
            UserInfo info = db.UserInfo.Find(userwe.Id);
            UsersOptions options = db.UsersOptions.Find(userwe.Id);
            bool printName = false;
            if (options != null)
            {
                printName = options.IsPrintNameIAct.GetValueOrDefault(true);
            }
            if (info != null && printName)
                user = info?.LastName + info?.FirstName.Substring(0, 1) + ". " + info?.MiddleName.Substring(0, 1) + ".";
            ViewBag.User = user;
            List<JournalOfRepair> journalNumberAndDate;
            var ActsID = application.Act.Select(t => (int?)t.ID).ToList();
            ViewBag.application = application;
            journalNumberAndDate =
            db.JournalOfRepair.Where(t => ActsID.Contains(t.Act)).ToList();
            List<string[]> TypeAndNumber = new List<string[]>();
            foreach (var jmodelname in journalNumberAndDate)
            {
                string a = "";
                string b = "";
                string m = "";
                if (jmodelname.ProductType != null)
                    a = jmodelname.ProductType.Name.TrimEnd(' ');
                if (jmodelname.NumberBlock != null)
                    b = jmodelname.NumberBlock.TrimEnd(' ');
                m = "Не работает";
                if (jmodelname.WarningTypeFromUser != null)
                    m = jmodelname.WarningTypeFromUsers.Name;
                if (jmodelname.DateOfSale != null)
                    b += " - " + jmodelname.DateOfSale.Value.ToString("dd.MM.yyг.").TrimEnd(' ');
                TypeAndNumber.Add(new string[] { a, b, m });
            }
            ViewBag.TypeAndNumber = TypeAndNumber;
            ViewBag.Comments = "";
            foreach (var act in application.Act)
            {
                if (act.UserProblemComments != null)
                {
                    if (db.RepairComments.FirstOrDefault(t => t.ID == act.UserProblemComments) != null)
                        ViewBag.Comments += db.RepairComments.FirstOrDefault(t => t.ID == act.UserProblemComments).Text;
                }
            }
            return View(application.Act.FirstOrDefault());
        }
        public ActionResult Report5(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Application application = db.Application.Find(id);

            IEnumerable<int?> ActsID = application.Act.Select(t => (int?)t.ID);

            var journal1 = db.JournalOfRepair
                .Where(t => ActsID.Contains(t.Act))
                .Select(t => t.ID);

            List<JournalOfRepairToComponent> jcomp = db.JournalOfRepairToComponent
                .Where(t => journal1.Contains(t.JournalOfRepair.ID))
                .ToList();

            IEnumerable<CompProductModel> jcomptomodel = jcomp.Select(t => new CompProductModel(t));

            var jcomptomodelgr = jcomptomodel
                .GroupBy(x => x.Name)
                .Select(x => new
                {
                    Name = x.Key,
                    Sum = x.Sum(t => t.Size)
                })
                .ToList();
            List<CompProductModel> Components = new List<CompProductModel>();
            foreach (var item in jcomptomodelgr)
            {
                CompProductModel jmodelname = jcomptomodel.FirstOrDefault(t => t.Name == item.Name);
                if (jmodelname != null)
                {
                    jmodelname.Size = item.Sum;
                    Components.Add(jmodelname);
                }
            }
            ViewBag.name = "Заявка №" + application.ID;
            return View("../Acts/Report5", Components);
            // return View("Acts", "Report5", Components);
        }
        public ActionResult SetIssued(string id, string type, string applicationId, string data, string idPar)
        {
            if (id == "" || type == "" || applicationId == "" || data == "" || idPar == "") return null;
            int ID = int.Parse(id);
            int Type = int.Parse(type);
            int ApplicationId = int.Parse(applicationId);
            int issueCount = int.Parse(data);
            //Act act = db.Act.Find(ActID);
            //if (act == null)
            //    return null;
            var application = db.Application.Find(ApplicationId);
            if (application == null) return null;
            var ActsID = application.Act.Select(t => (int?)t.ID).ToList();
            var journalsId = db.JournalOfRepair.Where(t => ActsID.Contains(t.Act.Value)).Select(t => t.ID).ToList();
            var components = db.JournalOfRepairToComponent
                .Where(t =>
            journalsId.Contains(t.JournalOfRepairID) && t.ComponentOrProductID == ID && t.Type == Type)
                .OrderByDescending(t => t.Issued).ToList();
            foreach (var journalOfRepairToComponent in components)
            {
                int sizeOnIsued = journalOfRepairToComponent.Size <= issueCount ? journalOfRepairToComponent.Size : issueCount;
                journalOfRepairToComponent.Issued = sizeOnIsued;
                issueCount -= sizeOnIsued;
                if (issueCount == 0) break;
            }
            db.SaveChanges();
            var count = components.Sum(t => t.Issued).ToString();
            List<string> rez = new List<string>();
            rez.Insert(0, idPar);
            rez.Insert(1, count);
            return Json(rez, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Application application = db.Application.Find(id);
            if (application == null)
                return HttpNotFound();
            return View(application);
        }
        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Application.Find(id);
            ApplicationServices ApplicationServices = new ApplicationServices(db);
            ApplicationServices.Delete(application);
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}
