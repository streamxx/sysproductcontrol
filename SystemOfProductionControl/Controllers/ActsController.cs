﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl.Models;
using SystemOfProductionControl.other;
using SystemOfProductionControl.Services;
namespace SystemOfProductionControl.Controllers
{
    public class ActsController : Controller
    {
        private readonly Entities db = new Entities();
        // GET: Acts
        public ActionResult Index()
        {
            var act = db.Act
            //.Include(a => a.AspNetUsers)
            .Where(t => t.DateOfShipment == null)
            .OrderByDescending(a => a.DateCreate);
            return View(act.ToList());
        }
        [HttpPost]
        public ActionResult AddProduct(int? idAct, int? TypeProductID, DateTime? DataOfReceipt, int? TypeWarningID1,
            int? WarningTypeFromUsers, string[] NumberBlock, int?[] DateOfSaleMM, int?[] DateOfSaleYY,
            string WarningComments)

        {
            int id = 0;
            try
            {
                id = idAct.Value;
            }
            catch (Exception)
            {
            }
            if (id == 0 || TypeProductID == 0) return Redirect("/Acts/Index");
            Act act = db.Act.Find(id);
            if (act == null) return Redirect("/Acts/Index");
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            int Count = 1;
            if (NumberBlock != null)
                Count = NumberBlock.Length;
            DateTime?[] DateOfSale = null;
            if (DateOfSaleMM != null && DateOfSaleYY != null)
            {
                DateOfSale = new DateTime?[DateOfSaleMM.Length];
                for (int i = 0; i < DateOfSaleMM.Length; i++)
                {
                    DateTime? dateTime = null;
                    if (DateOfSaleMM[i] != null && DateOfSaleYY[i] != null)
                    {
                        dateTime = new DateTime(DateOfSaleYY[i].Value, DateOfSaleMM[i].Value, 1);
                    }
                    else if (DateOfSaleYY[i] != null)
                    {
                        dateTime = new DateTime(DateOfSaleYY[i].Value, 1, 1);
                    }
                    DateOfSale[i] = dateTime;
                }
            }
            for (int i = 0; i < Count; i++)
            {
                JournalOfRepair journalOfRepair = new JournalOfRepair
                {
                    LoginId = user.Id,
                    WarningTypeFromUser = WarningTypeFromUsers,
                    Act = act.ID,
                    TypeProduct = TypeProductID.Value
                };
                if (DataOfReceipt == null)
                    DataOfReceipt = (DateTime.Now).AddHours(4);
                if (TypeWarningID1 != -1)
                {
                    JournalToWarningOrComponent warningOrComponent = new JournalToWarningOrComponent
                    {
                        Type = 0,
                        IDWarningOrComponent = (int)TypeWarningID1
                    };
                    journalOfRepair.JournalToWarningOrComponent.Add(warningOrComponent);
                }
                journalOfRepair.NumberBlock = NumberBlock[i];
                if (DateOfSale[i] != null)
                    journalOfRepair.DateOfSale = DateOfSale[i];
                db.JournalOfRepair.Add(journalOfRepair);
                int WarningCommentsId = -1;
                if (WarningComments != null)
                {
                    RepairComments Warning;
                    if (WarningComments != "")
                    {
                        Warning = new RepairComments
                        {
                            Text = WarningComments
                        };
                        var d = db.RepairComments.Add(Warning);
                        db.SaveChanges();
                        WarningCommentsId = d.ID;
                    }
                }
                if (WarningCommentsId != -1)
                    journalOfRepair.WarningComments = WarningCommentsId;
                db.SaveChanges();
            }
            return Redirect("/Acts/Details?id=" + idAct.ToString());
        }
        public ActionResult IndexOfRole()
        {
            var act =
            db.Act //.Include(a => a.AspNetUsers)
            .Include(a => a.Cities)
            .Include(a => a.Company1)
            .Include(a => a.RepairComments).OrderByDescending(a => a.DateCreate).Take(200).ToList();
            List<Act> acts = new List<Act>();
            if (User.IsInRole("Склад"))
            {
                foreach (var item in act)
                {
                    var journal = db.JournalOfRepair.FirstOrDefault(t => t.Act == item.ID && t.DateOfDelivery == null);
                    if (journal == null && !item.ReadyStorage)
                    {
                        acts.Add(item);
                    }
                }
            }
            if (User.IsInRole("Бухгалтерия"))
            {
                foreach (var item in act)
                {
                    if (item.ReadyStorage && !item.Ready)
                    {
                        acts.Add(item);
                    }
                }
            }
            if (User.IsInRole("Admin"))
            {
                foreach (var item in act)
                {
                    var journal = db.JournalOfRepair.FirstOrDefault(t => t.Act == item.ID && t.DateOfDelivery == null);
                    if (journal == null && !item.ReadyStorage)
                    {
                        acts.Add(item);
                    }
                }
                foreach (var item in act)
                {
                    if (item.ReadyStorage && !item.Ready)
                    {
                        acts.Add(item);
                    }
                }
            }
            return View(acts);
        }
        public ActionResult IndexOfBookKeeper()
        {
            var act = db.Act //.Include(a => a.AspNetUsers)
            .OrderByDescending(a => a.DateCreate).ToList();
            List<Act> acts = new List<Act>();
            Dictionary<int, List<string>> dictionaryPrice = new Dictionary<int, List<string>>();
            foreach (var item in act)
            {
                if (item.ReadyStorage && !item.Ready)
                {
                    acts.Add(item);
                    var s = GetRecalculationPrice(item);
                    dictionaryPrice.Add(item.ID, s);
                }
            }
            ViewBag.DictionaryPrice = dictionaryPrice;
            return View(acts);
        }
        public ActionResult IndexOfBookKeeperNofilter()
        {
            var act = db.Act //.Include(a => a.AspNetUsers)
            .Include(a => a.Cities)
            .Include(a => a.Company1)
            .Include(a => a.RepairComments).OrderByDescending(a => a.DateCreate).ToList();
            List<Act> acts = new List<Act>();
            Dictionary<int, List<string>> dictionaryPrice = new Dictionary<int, List<string>>();
            foreach (var item in act)
            {
                if (item.ReadyStorage)
                {
                    acts.Add(item);
                    var s = GetRecalculationPrice(item);
                    dictionaryPrice.Add(item.ID, s);
                }
            }
            ViewBag.DictionaryPrice = dictionaryPrice;
            return View("IndexOfBookKeeper", acts);
        }
        public ActionResult IndexOfStorage()
        {
            var act = db.Act.OrderByDescending(a => a.DateCreate);

            List<Act> acts =
                (
                from Act item in act
                let jrep = item.JournalOfRepair.FirstOrDefault(t => t.Act == item.ID)
                where jrep != null
                let jrpcomp = jrep.JournalOfRepairToComponent.FirstOrDefault(t => t.JournalOfRepairID == jrep.ID)
                where jrpcomp != null
                && item.ActCondition.OnRepair == true && item.ActCondition.ProcessedStorage == false && item.ActCondition.IsRepairCompleted == false
                select item
                )
                .ToList();
            return View(acts);
        }

        // GET: Acts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var dbCity = db.Cities.OrderBy(t => t.Name);
            SelectList city = new SelectList(dbCity, "ID", "Name");
            ViewBag.City = city;
            int idCity = dbCity.FirstOrDefault().ID;
            var warntypequery = (from d in db.WarningType select d).ToList();
            warntypequery.Add(new WarningType { ID = -1, Name = "Нет" });
            ViewBag.TypeWarningID1 = new SelectList(warntypequery.OrderBy(t => t.ID), "ID", "Name");
            ViewBag.TypeProductID = new SelectList(db.ProductType.OrderBy(t => t.Name), "ID", "Name");
            var CompanyQuery = from d in db.Company
                               where d.City == idCity
                               select new
                               {
                                   ID = d.Id,
                                   Name = d.Name + " ИНН:" + d.INN
                               }
            ;
            ViewBag.Companies = new SelectList(CompanyQuery.OrderBy(t => t.Name), "ID", "Name");
            Act act = db.Act.Find(id);
            ViewBag.WarningTypeFromUsers = new SelectList(db.WarningTypeFromUsers, "ID", "Name");
            var journal =
            db.JournalOfRepair
            .Include(j => j.Act1)
            .Include(j => j.AspNetUsers)
            .Include(j => j.ProductType)
            .Include(j => j.RepairComments)
            .Where(t => t.Act == act.ID)
            .ToList();
            ViewBag.journalOfRepair = journal;
            var journal1 = journal.Select(t => (int?)t.ID);
            var s = db.JournalOfRepairToComponent
                .Where(t => journal1.Contains(t.JournalOfRepairID))
                .ToList(); 
            var compprodmod = s.Select(t => new CompProductModel(t));
            var compprodmodgrp = compprodmod.GroupBy((x) => x.Name).Select(x => new { Name = x.Key, Sum = x.Sum(t => t.Size), IssuedSum = x.Sum(t => t.Issued) }).ToList();
            List<CompProductModel> Components = new List<CompProductModel>();
            foreach (var item in compprodmodgrp)
            {
                var compprodmodgrpname = compprodmod.FirstOrDefault(t => t.Name == item.Name);
                if (compprodmodgrpname != null)
                {
                    compprodmodgrpname.Size = item.Sum;
                    compprodmodgrpname.Issued = item.IssuedSum;
                    Components.Add(compprodmodgrpname);
                }
            }
            ViewBag.Component = Components;
            var groupOfProduct = journal.GroupBy(t => t.ProductType).ToList();
            Dictionary<int, SelectList> selectListWarning = new Dictionary<int, SelectList>();
            foreach (var compprodmodgrpname in groupOfProduct)
            {
                var departmentsQuery = (from d in db.WarningType
                                        where d.ProductTypeID == compprodmodgrpname.Key.ID || d.ProductTypeID == null
                                        select d).ToList();
                departmentsQuery.Add(new WarningType { ID = -1, Name = "Нет" });
                selectListWarning.Add(compprodmodgrpname.Key.ID, new SelectList(departmentsQuery.OrderBy(t => t.ID), "ID", "Name"));
            }
            ViewBag.Warranty = null;
            ViewBag.IdOther = null;
            ViewBag.TypeWarningID = selectListWarning;
            ViewBag.Comments = db.RepairComments.Find(act.Comment);
            ViewBag.UserProblemComments = db.RepairComments.Find(act.UserProblemComments);
            if (act == null)
            {
                return HttpNotFound();
            }
            return View(act);
        }

        [HttpGet]
        public ActionResult AjaxTest(String name)
        {
            int d = int.Parse(name);
            var departmentsQuery = db.WarningType.Where(t => t.ProductType.ID == d || t.ProductType == null).ToList<WarningType>();
            departmentsQuery.Add(new WarningType { ID = -1, Name = "Нет" });
            SelectList TypeWarningID = new SelectList(departmentsQuery.OrderBy(t => t.ID), "ID", "Name");
            return Json(TypeWarningID, JsonRequestBehavior.AllowGet);
        }
        private List<string> GetRecalculationPrice(Act act, bool priceIsAct = true)
        {
            float overall = 0;
            float? makeup = null;
            float Sumary = 0;
            float workPrice = 0;
            float all = 0;
            float nds = 0;
            List<string> data = new List<string>();
            if (act.Markup != null)
            {
                if (act.Markup != "")
                {
                    try
                    {
                        makeup = float.Parse(act.Markup);
                    }
                    catch
                    {
                    }
                }
            }
            if (priceIsAct)
            {
                if (act.Price != null)
                {
                    try
                    {
                        workPrice = float.Parse(act.Price);
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                foreach (var journalOfRepair in act.JournalOfRepair)
                {
                    if (journalOfRepair?.Price != "")
                    {
                        try
                        {
                            float? wPrice = 0;
                            wPrice = float.Parse(journalOfRepair.Price);
                            if (wPrice != null)
                            {
                                workPrice += wPrice.Value;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            var comp = db.JournalOfRepairToComponent.Include(t => t.JournalOfRepair);
            //TODO: сделать пересчет стоимости, надо брать данные из 2х таблиц компонентов и  оборудования 
            var rez = comp.Where(t => t.JournalOfRepair.Act == act.ID);
            foreach (var item in rez)
            {
                if (item.Price != null && item.Price != "")
                {
                    try
                    {
                        overall += float.Parse(item.Price) * item.Size;
                    }
                    catch
                    {
                    }
                }
            }
            data.Add(overall.ToString("0.00"));
            if (makeup != null)
                data.Add(makeup.ToString());
            else data.Add("");
            Sumary = overall;
            if (makeup != null)
                Sumary += makeup.Value;
            data.Add(Sumary.ToString("0.00"));
            data.Add(workPrice.ToString("0.00"));
            all = Sumary;
            if (workPrice != 0)
            {
                all += workPrice;
            }
            //nds = all*0.18f;
            nds = all * (CustomConstants.GetNDS() / 100);
            data.Add(nds.ToString("0.00"));
            all = all + nds;
            data.Add(all.ToString("0.00"));
            return data;
        }
        public void SetINN(int actId, String text)
        {
            Act act = db.Act.Find(actId);
            Company company = act.Company1;
            company.INN = text.TrimEnd(' ');
            db.SaveChanges();
        }
        public void SetKPP(int actId, String text)
        {
            Act act = db.Act.Find(actId);
            Company company = act.Company1;
            company.KPP = text.TrimEnd(' ');
            db.SaveChanges();
        }
        public void SetPhone(int actId, String text)
        {
            Act act = db.Act.Find(actId);
            Company company = act.Company1;
            company.Phone = text.TrimEnd(' ');
            db.SaveChanges();
        }
        public void SetDateOfShipment(int actId, DateTime date)
        {
            Act act = db.Act.Find(actId);
            DateTime dateTimeNew = DateTime.Now;
            date = date.AddHours(dateTimeNew.Hour + 4);
            date = date.AddMinutes(dateTimeNew.Minute);
            act.DateOfShipment = date;
            db.SaveChanges();
        }
        public void SetChecked(int actId, int number, bool value)
        {
            Act act = db.Act.Find(actId);
            //if (act != null)
            //{
            //    switch (number)
            //    {
            //        case 1:
            //        {
            //            act.replace = value;
            //            break;
            //        }
            //        case 2:
            //        {
            //            act.diagnostic = value;
            //            break;
            //        }
            //        case 3:
            //        {
            //            act.repair = value;
            //            break;
            //        }
            //    }
            //    db.SaveChanges();
            //}
        }
        public void ToOtherType(int Id)
        {
            Act act = null;
            var journal = db.JournalOfRepair.Find(Id);
            if (journal == null)
                return;
            Act parent = db.Act.Find(journal.Act);
            if (parent == null)
                return;
            Application application = parent.Application.FirstOrDefault();

            if (application == null) return;

            act = application.Act.FirstOrDefault(t => t.ID != parent.ID);

            if (act == null)
            {
                act = new Act();
                var name = "";
                act.Name = name;
                act.DateCreate = parent.DateCreate;
                act.Company = parent.Company;
                act.City = parent.City;
                act.Comment = parent.Comment;
                act.UserProblemComments = parent.UserProblemComments;
                act.Warranty = !parent.Warranty.Value;
                db.Act.Add(act);
                db.SaveChanges();
                application.Act.Add(act);

                db.SaveChanges();

                ActCondition actCondition = new ActCondition
                {
                    Completed = false,
                    Paid = false,
                    CreateApplication = true, // Создана заявка - акт приема оборудования
                    OnRepair = false, // В ремонте, создана заявка ремонта, точнее, был ли хоть раз в ремонте. Действует бессрочно
                    IsRepairCompleted = false, // Завершен ремонт
                    ProcessedStorage = false, // Акт ремонта обработан складом
                    ProcessedBookkeeping = false, // Акт Обработан бухгалтерий 
                    ProductSent = false, // Продукт отправлен
                    PendingSubmission = false, // Ожидает отправки
                    WriteOff = false // Списание
                };

                act.ActCondition = actCondition;
            }



            if (act != null && journal != null)
            {
                journal.Act = act.ID;
                db.SaveChanges();
            }
            if (parent.JournalOfRepair.Count == 0)
            {
                ActCondition parentx = db.ActCondition.Where(t => t.ActID == parent.ID).FirstOrDefault();
                db.Act.Remove(parent);
                db.ActCondition.Remove(parentx);

                db.SaveChanges();
            }
        }
        public void SetComment(int IdAct, int type, string text)
        {
            Act act = db.Act.Find(IdAct);
            if (act != null)
            {
                RepairComments comments;
                switch (type)
                {
                    case 0:
                        {
                            if (act.UserProblemComments != null)
                            {
                                comments = db.RepairComments.Find(act.UserProblemComments);
                                comments.Text = text;
                                db.SaveChanges();
                            }
                            else
                            {
                                comments = new RepairComments
                                {
                                    Text = text
                                };
                                db.RepairComments.Add(comments);
                                db.SaveChanges();
                                act.UserProblemComments = comments.ID;
                                db.SaveChanges();
                            }
                            break;
                        }
                    case 1:
                        {
                            if (act.Comment != null)
                            {
                                comments = db.RepairComments.Find(act.Comment);
                                comments.Text = text;
                                db.SaveChanges();
                            }
                            else
                            {
                                comments = new RepairComments
                                {
                                    Text = text
                                };
                                db.RepairComments.Add(comments);
                                db.SaveChanges();
                                act.Comment = comments.ID;
                                db.SaveChanges();
                            }
                            break;
                        }
                }
            }
        }
        public ActionResult CalculateWork(int id, String cena, String mnog)
        {
            Act act = db.Act.Find(id);
            float Cena = 0;
            float m = 1;
            try
            {
                cena = cena.Trim(' ').Replace('.', ',');
                Cena = float.Parse(cena);
            }
            catch
            {
                return Redirect("/Acts/DetailsBookkeeping?id=" + id.ToString());
            }
            try
            {
                mnog = mnog.Trim(' ').Replace('.', ',');
                m = float.Parse(mnog);
            }
            catch
            {
            }
            var journals = db.JournalOfRepair.Where(t => t.Act == id).ToList();
            foreach (var journalOfRepair in journals)
            {
                if (journalOfRepair.ProductType.CoefficientPrice != null && journalOfRepair.ProductType.CoefficientPrice > 0)
                    journalOfRepair.Price = (m * Cena * journalOfRepair.ProductType.CoefficientPrice.Value).ToString("0.00");
            }
            List<string> f = GetRecalculationPrice(act, false);
            db.SaveChanges();
            act.Price = f[3];
            db.SaveChanges();
            return Redirect("/Acts/DetailsBookkeeping?id=" + id.ToString());
        }
        public ActionResult SetPriceWork(String actId, String data)
        {
            int c = int.Parse(actId);
            Act act = db.Act.FirstOrDefault(t => t.ID == c);
            float price = 0;
            try
            {
                data = data.Trim(' ').Replace('.', ',');
                price = float.Parse(data);
            }
            catch
            {
                if (data != "")
                    return null;
            }
            if (act != null)
            {
                act.Price = data != "" ? price.ToString("0.00") : "";
                db.SaveChanges();
                List<string> rez = GetRecalculationPrice(act, true);
                rez.Insert(0, "");
                rez.Insert(0, "");
                return Json(rez, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        public ActionResult SetMarkup(string actId, String data)
        {
            int c = int.Parse(actId);
            Act act = db.Act.FirstOrDefault(t => t.ID == c);
            float Markup = 0;
            try
            {
                data = data.Trim(' ').Replace('.', ',');
                Markup = float.Parse(data);
            }
            catch
            {
                if (data != "") return null;
            }
            if (act != null)
            {
                act.Markup = data != "" ? Markup.ToString("0.00") : "";
                db.SaveChanges();
                List<string> rez = GetRecalculationPrice(act);
                rez.Insert(0, "");
                rez.Insert(0, "");
                return Json(rez, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        public ActionResult SetActReady(int actId)
        {
            Act act = db.Act.Find(actId);
            ActServices ActServices = new ActServices(db);
            ActServices.SetActReady(act);
            return Redirect("/Acts/DetailsBookkeeping?id=" + actId.ToString());
        }
        public ActionResult SetActReadyCancel(int actId)
        {
            Act act = db.Act.Find(actId);
            ActServices ActServices = new ActServices(db);
            ActServices.SetActReadyCancel(act);
            return Redirect("/Acts/DetailsBookkeeping?id=" + actId.ToString());
        }
        public ActionResult SetActPaidandPend(int actId)
        {
            ActServices ActServices = new ActServices(db);
            ActServices.SetActPaidandPend(db.Act.Find(actId));
            return Redirect("/Acts/DetailsBookkeeping?id=" + actId.ToString());
        }
        public ActionResult PaidCancel(int actId)
        {
            ActServices actServices = new ActServices(db);
            actServices.PaidCancel(db.Act.Find(actId));
            return Redirect("/Acts/DetailsBookkeeping?id=" + actId.ToString());
        }
        public ActionResult SetActReadyStorage(int actId)
        {
            int ID = actId;
            Act act = db.Act.Find(ID);

            if (act != null)
            {
                new ActServices(db).SetActReadyStorage(act);
            }
            return Redirect("/Acts/DetailsStorage?id=" + actId.ToString());


        }

        public ActionResult SetActReadyStorageCancel(int actId)
        {
            Act act = db.Act.Find(actId);
            if (act != null)
            {
                ActServices ActServices = new ActServices(db);
                ActServices.SetActReadyStorageCancel(act);
            }
            return Redirect("/Acts/DetailsStorage?id=" + actId.ToString());
        }
        public ActionResult Report1(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal =
            db.JournalOfRepair.Where(t => t.Act == act.ID)
            .GroupBy(t => t.TypeProduct)
            .Select(x => new { type = x.Key, count = x.Count() });
            Dictionary<int, int> products = new Dictionary<int, int>();
            foreach (var jitem in journal)
            {
                products.Add(jitem.type, jitem.count);
            }
            string Count = "";
            string Text = "";
            foreach (var productsitem in products)
            {
                //Count += productsitem.Value.ToString().TrimEnd(' ') + ", ";
                //Text += db.ProductType.First(t => t.ID == productsitem.Key).Name.TrimEnd(' ') + ", ";
                Text += db.ProductType.FirstOrDefault(t => t.ID == productsitem.Key).Name.TrimEnd(' ') + " - " + productsitem.Value.ToString().TrimEnd(' ') + "шт., ";
            }
            Count = Count.TrimEnd(' ').TrimEnd(',');
            Text = Text.TrimEnd(' ').TrimEnd(',');
            var journalNumberAndDate = db.JournalOfRepair.Where(t => t.Act == act.ID);
            List<string> StringsNumberAndDate = new List<string>();
            foreach (var item in journalNumberAndDate)
            {
                string a = "";
                if (item.NumberBlock != null)
                    if (item.NumberBlock.TrimEnd(' ') != "")
                    {
                        a = item.NumberBlock.TrimEnd(' ');
                    }
                if (a != "")
                {
                    if (item.DateOfSale != null)
                    {
                        a += " - " + item.DateOfSale.Value.ToString("dd.MM.yy");
                    }
                }
                if (a != "")
                    StringsNumberAndDate.Add(a);
            }
            string textStringsNumberAndDate = "";
            for (int i = 0; i < StringsNumberAndDate.Count; i++)
            {
                if (i != StringsNumberAndDate.Count - 1)
                    textStringsNumberAndDate += StringsNumberAndDate[i] + ", ";
                else
                {
                    textStringsNumberAndDate += StringsNumberAndDate[i];
                }
            }
            ViewBag.Count = Count;
            ViewBag.NumberAndData = textStringsNumberAndDate;
            ViewBag.TypeName = Text;
            ViewBag.Comments = "";
            List<string> rez = GetRecalculationPrice(act);
            ViewBag.Prices = rez;
            if (act.Comment != null)
                ViewBag.Comments = db.RepairComments.Where(t => t.ID == act.Comment).FirstOrDefault().Text;
            var s = act.JournalOfRepair.GroupBy(t => t.LoginId).Select(t => t.Key);
            var f = db.AspNetUsers.Include(t => t.UserInfo).Where(t => s.Contains(t.Id));
            String FIO = "";
            foreach (var item in f)
            {
                if (FIO != "")
                {
                    FIO += ", ";
                }
                FIO += item.UserInfo?.LastName + " " + item.UserInfo?.FirstName + " " + item.UserInfo?.MiddleName;
            }
            ViewBag.FIO = FIO;
            var journal1 = act.JournalOfRepair.Select(t => t.ID);
            var gh = db.JournalOfRepairToComponent.Where(t => journal1.Contains(t.JournalOfRepair.ID)).ToList();
            var d = gh.Select(t => new CompProductModel(t));
            var asasd = d.GroupBy((x) => x.Name).Select(x => new { Name = x.Key, Sum = x.Sum(t => t.Size) }).ToList();
            List<CompProductModel> Components = new List<CompProductModel>();
            foreach (var VARIABLE in asasd)
            {
                var item = d.FirstOrDefault(t => t.Name == VARIABLE.Name);
                if (item != null)
                {
                    item.Size = VARIABLE.Sum;
                    Components.Add(item);
                }
            }
            ViewBag.Component = Components;
            return View(act);
        }
        public ActionResult Report5(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal1 = act.JournalOfRepair.Select(t => t.ID);
            var gh = db.JournalOfRepairToComponent.Where(t => journal1.Contains(t.JournalOfRepair.ID)).ToList();
            var d = gh.Select(t => new CompProductModel(t));
            var asasd = d.GroupBy((x) => x.Name).Select(x => new { Name = x.Key, Sum = x.Sum(t => t.Size) }).ToList();
            List<CompProductModel> Components = new List<CompProductModel>();
            foreach (var VARIABLE in asasd)
            {
                var item = d.FirstOrDefault(t => t.Name == VARIABLE.Name);
                if (item != null)
                {
                    item.Size = VARIABLE.Sum;
                    Components.Add(item);
                }
            }
            ViewBag.name = "Акт №" + act.Name;
            return View(Components);
        }
        public ActionResult Report2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal =
            db.JournalOfRepair.Where(t => t.Act == act.ID)
            .GroupBy(t => t.TypeProduct)
            .Select(x => new { type = x.Key, count = x.Count() });
            Dictionary<int, int> products = new Dictionary<int, int>();
            foreach (var jitem in journal)
            {
                products.Add(jitem.type, jitem.count);
            }
            string Count = "";
            string Text = "";
            foreach (var productsitem in products)
            {
                Count += productsitem.Value.ToString().TrimEnd(' ') + ", ";
                Text += db.ProductType.First(t => t.ID == productsitem.Key).Name.TrimEnd(' ') + ", ";
            }
            Count = Count.TrimEnd(' ').TrimEnd(',');
            Text = Text.TrimEnd(' ').TrimEnd(',');
            var journalNumberAndDate = db.JournalOfRepair.Where(t => t.Act == act.ID).GroupBy(t => t.ProductType);
            List<string> TypeAndNumber = new List<string>();
            foreach (var itemProd in journalNumberAndDate)
            {
                int count = 0;
                string a = "";
                if (itemProd.Key != null)
                    if (itemProd.Key.Name.TrimEnd(' ') != "")
                        a = itemProd.Key.Name.TrimEnd(' ');
                string b = "";
                foreach (var item in itemProd)
                {
                    if (item.NumberBlock != null)
                        if (item.NumberBlock.TrimEnd(' ') != "")
                        {
                            if (b != "")
                            {
                                b += ", ";
                            }
                            b += item.NumberBlock.TrimEnd(' ');
                        }
                    count++;
                }
                if (a != "")
                {
                    if (count == 1)
                    {
                        if (b != "")
                        {
                            a = a + " - " + b;
                        }
                    }
                    else
                    {
                        a = a + "-" + count.ToString() + "шт.";
                        if (b != "")
                        {
                            a += "(" + b + ")";
                        }
                    }
                    TypeAndNumber.Add(a);
                }
            }
            string textStringsNumberAndDate = "";
            for (int i = 0; i < TypeAndNumber.Count; i++)
            {
                if (i != TypeAndNumber.Count - 1)
                    textStringsNumberAndDate += TypeAndNumber[i] + ", ";
                else
                {
                    textStringsNumberAndDate += TypeAndNumber[i];
                }
            }
            var application = act.Application.FirstOrDefault();
            ViewBag.applicationId = application.ID.ToString();
            ViewBag.dateCreate = application.Data.AddHours(4);
            ViewBag.Count = Count;
            ViewBag.TypeAndNumber = textStringsNumberAndDate;
            ViewBag.TypeName = Text;
            ViewBag.Comments = "";
            List<string> rez = GetRecalculationPrice(act);
            ViewBag.Prices = rez;
            double d = double.Parse(rez[rez.Count - 1]);
            ViewBag.Suma = Сумма.Пропись(d, Валюта.Рубли).ToString();
            if (act.Comment != null)
                ViewBag.Comments = db.RepairComments.Where(t => t.ID == act.Comment).FirstOrDefault().Text;
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
            .GetUserManager<ApplicationUserManager>();
            ApplicationUser userwe = userManager.FindByEmail(User.Identity.Name);
            UserInfo info = db.UserInfo.Find(userwe.Id);
            UsersOptions options = db.UsersOptions.Find(userwe.Id);
            bool printName = false;
            if (options != null)
            {
                printName = options.IsPrintNameIAct.GetValueOrDefault(true);
            }
            if (printName)
            {
                ViewBag.userInfo = info;
            }
            return View(act);
        }
        public ActionResult Report3(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal =
            db.JournalOfRepair.Where(t => t.Act == act.ID)
            .GroupBy(t => t.TypeProduct)
            .Select(x => new { type = x.Key, count = x.Count() });
            Dictionary<int, int> products = new Dictionary<int, int>();
            foreach (var jitem in journal)
            {
                products.Add(jitem.type, jitem.count);
            }
            var journalNumberAndDate = db.JournalOfRepair.Where(t => t.Act == act.ID);
            List<string[]> TypeAndNumber = new List<string[]>();
            foreach (var item in journalNumberAndDate)
            {
                string a = "";
                string b = "";
                string m = "";
                if (item.ProductType != null)
                    a = item.ProductType.Name.TrimEnd(' ');
                if (item.NumberBlock != null)
                    b = item.NumberBlock.TrimEnd(' ');
                m = "Не работает";
                if (item.WarningTypeFromUser != null)
                    m = item.WarningTypeFromUsers.Name;
                if (item.DateOfSale != null)
                    b += " - " + item.DateOfSale.Value.ToString("dd.MM.yyг.").TrimEnd(' ');
                TypeAndNumber.Add(new string[] { a, b, m });
            }
            ViewBag.TypeAndNumber = TypeAndNumber;
            return View(act);
        }
        public ActionResult Report4(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal =
            db.JournalOfRepair.Where(t => t.Act == act.ID)
            .GroupBy(t => t.TypeProduct)
            .Select(x => new { type = x.Key, count = x.Count() });
            Dictionary<int, int> products = new Dictionary<int, int>();
            foreach (var jitem in journal)
            {
                products.Add(jitem.type, jitem.count);
            }
            string user = "___________________";
            //if (act?.AspNetUsers?.UserInfo?.FirstName != null)
            //    user = act.AspNetUsers.UserInfo.FirstName + " ";
            //if (act?.AspNetUsers?.UserInfo?.LastName != null)
            //    user += act.AspNetUsers.UserInfo.LastName;
            ViewBag.User = user;
            Application application = act.Application.FirstOrDefault();
            List<JournalOfRepair> journalNumberAndDate;
            var actsId = application.Act.Select(t => (int?)t.ID).ToList();
            if (application != null)
            {
                ViewBag.application = application;
                journalNumberAndDate =
                db.JournalOfRepair.Where(t => actsId.Contains(t.Act))
                .ToList();
            }
            else
            {
                journalNumberAndDate = db.JournalOfRepair.Where(t => t.Act == act.ID).ToList();
            }
            List<string[]> TypeAndNumber = new List<string[]>();
            foreach (var item in journalNumberAndDate)
            {
                string a = "";
                string b = "";
                string m = "";
                if (item.ProductType != null)
                    a = item.ProductType.Name.TrimEnd(' ');
                if (item.NumberBlock != null)
                    b = item.NumberBlock.TrimEnd(' ');
                m = "Не работает";
                if (item.WarningTypeFromUser != null)
                    m = item.WarningTypeFromUsers.Name;
                if (item.DateOfSale != null)
                    b += " - " + item.DateOfSale.Value.ToString("dd.MM.yyг.").TrimEnd(' ');
                TypeAndNumber.Add(new String[] { a, b, m });
            }
            ViewBag.TypeAndNumber = TypeAndNumber;
            ViewBag.Comments = "";
            if (act.UserProblemComments != null)
            {
                var firstOrDefault = db.RepairComments.FirstOrDefault(t => t.ID == act.UserProblemComments);
                if (firstOrDefault != null)
                    ViewBag.Comments = firstOrDefault.Text;
            }
            return View(act);
        }
        public ActionResult Report6(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            var journal =
            db.JournalOfRepair.Where(t => t.Act == act.ID)
            .GroupBy(t => t.TypeProduct)
            .Select(x => new { type = x.Key, count = x.Count() });
            Dictionary<int, int> products = new Dictionary<int, int>();
            foreach (var jitem in journal)
            {
                products.Add(jitem.type, jitem.count);
            }
            var journalNumberAndDate = db.JournalOfRepair.Where(t => t.Act == act.ID);
            List<string[]> TypeAndNumber = new List<string[]>();
            foreach (var item in journalNumberAndDate)
            {
                string a = "";
                string b = "";
                string m = "";
                string warning = "";
                if (item.ProductType != null)
                    a = item.ProductType.Name.TrimEnd(' ');
                if (item.NumberBlock != null)
                    b = item.NumberBlock.TrimEnd(' ');
                m = "Исправно";
                JournalOfRepairModel journalOfRepairModels = new JournalOfRepairModel(item);
                foreach (var warningVmModel in journalOfRepairModels.WarningList)
                {
                    if (warningVmModel.Type == (int)BaseEnums.JournalToWarningOrComponentOrProduct.Warning)
                    {
                        if (warning != "")
                        {
                            warning += ", ";
                        }
                        warning += warningVmModel.Name;
                    }
                }
                if (journalOfRepairModels.WarningList.Count != 0 || (item.Replace))
                {
                    m = "";
                    if (item.Replace)
                    {
                        m += "Замена оборудования";
                    }
                    if (warning != "")
                    {
                        if (m != "")
                            m += ", ";
                        m += warning;
                    }
                    else if (journalOfRepairModels.WarningList.Count != 0)
                    {
                        if (m == "")
                        {
                            m += "Неисправно";
                        }
                    }
                }
                if (item.DateOfSale != null)
                    b += " - " + item.DateOfSale.Value.ToString("dd.MM.yyг.").TrimEnd(' ');
                TypeAndNumber.Add(new String[] { a, b, m });
            }
            ViewBag.TypeAndNumber = TypeAndNumber;
            return View(act);
        }


        public ActionResult DetailsStorage(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Act act = db.Act.Find(id);
            var journal_act = db.JournalOfRepair.Where(t => t.Act == act.ID);
            var jr_comp = db.JournalOfRepairToComponent
             .Where(t => t.Type == 0)
             .Where(t => journal_act.Contains(t.JournalOfRepair))
             .ToList();

            /* var jr_comp = db.JournalOfRepairToComponent
                // .Where(t => t.Type == 0)  // Для склада показывает только КИ, но по факту на до делать нормальный класс
                 .Where(t => t.JournalOfRepair.ID == journal_act
                 .FirstOrDefault().ID).ToList();*/ //ошибка
            var jr_comp_m_grouped = jr_comp.Select(t => new CompProductModel(t))
                .GroupBy(x => x.Name)
                .Select(x => new { Name = x.Key, Sum = x.Sum(t => t.Size), IssuedSum = x.Sum(t => t.Issued) })
                .ToList();

            List<CompProductModel> Components = new List<CompProductModel>();

            foreach (var item in jr_comp_m_grouped)
            {
                CompProductModel compmodel = jr_comp
                    .Select(t => new CompProductModel(t))
                    .FirstOrDefault(t => t.Name == item.Name);
                if (compmodel != null)
                {
                    compmodel.Size = item.Sum;
                    compmodel.Issued = item.IssuedSum;
                    Components.Add(compmodel);
                }
            }

            ViewBag.journalOfRepair = journal_act
               .ToList()
               .Select(t => new JournalOfRepairModel(t))
               .ToList();
            ViewBag.Component = Components;
            ViewBag.Prices = GetRecalculationPrice(act);
            ViewBag.Status = act.Application.FirstOrDefault().Status;
            return View(act);
        }


        public ActionResult SetIssued(string id, string type, string actId, string Issued, string idPar)
        {
            if (id == "" || type == "" || actId == "" || Issued == "" || idPar == "") return null;
            int ID = int.Parse(id);
            int Type = int.Parse(type);
            int ActID = int.Parse(actId);
            int issueCount = int.Parse(Issued);
            var journalsId = db.JournalOfRepair
                .Where(t => t.Act.Value == ActID)
                .Select(t => t.ID).ToList();

            var components = db.JournalOfRepairToComponent
                .Where(t => journalsId.Contains(t.JournalOfRepairID) && t.ComponentOrProductID == ID && t.Type == Type)
                .OrderByDescending(t => t.Issued);

            foreach (JournalOfRepairToComponent jrepcomp in components.ToList())
            {
                int sizeOnIsued = jrepcomp.Size <= issueCount ? jrepcomp.Size : issueCount;
                jrepcomp.Issued = sizeOnIsued;
                issueCount -= sizeOnIsued;
                if (issueCount == 0) break;
            }
            db.SaveChanges();
            var count = components.Sum(t => t.Issued).ToString();
            var rez = new List<string>();
            rez.Insert(0, idPar);
            rez.Insert(1, count);
            return Json(rez, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DetailsBookkeeping(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Act act = db.Act.Find(id);
            var journal1 = db.JournalOfRepair.Include(j => j.Act1)
            .Include(j => j.AspNetUsers)
            .Include(j => j.ProductType)
            .Include(j => j.RepairComments)
            //.Include(j => j.WarningType1)
            .Where(t => t.Act == act.ID).OrderByDescending(t => t.Replace);
            var journal = journal1.ToList().Select(t => new JournalOfRepairModel(t))
            .ToList();
            ViewBag.journalOfRepair = journal;
            var usersId = journal.GroupBy(t => t.LoginId).Select(t => t.Key).ToList();
            var usersInfo = db.AspNetUsers.Include(t => t.UserInfo).Where(t => usersId.Contains(t.Id));
            string FIO = "";

            foreach (var item in usersInfo)
            {
                if (FIO != "") FIO += ", ";
                FIO += item.UserInfo?.LastName + " " + item.UserInfo?.FirstName + " " + item.UserInfo?.MiddleName;
            }
            ViewBag.FIO = FIO;
            var jrcomp = db.JournalOfRepairToComponent.Where(t => journal1.Contains(t.JournalOfRepair)).ToList(); ;
            var jrcompmodel = jrcomp.Select(t => new CompProductModel(t));

            var jrcompdelgrp = jrcompmodel.GroupBy((x) => x.CodeOrABRM)
                .Select(x => new { code = x.Key, Sum = x.Sum(t => t.Size) })
                .ToList();

            List<CompProductModel> Components = new List<CompProductModel>();
            foreach (var item in jrcompdelgrp)
            {
                var jrcompcode = jrcompmodel.FirstOrDefault(t => t.CodeOrABRM == item.code);
                if (jrcompcode != null)
                {
                    jrcompcode.Size = item.Sum;
                    Components.Add(jrcompcode);
                }
            }
            ViewBag.Component = Components.OrderBy(t => t.Storage).ToList();
            //TODO: загружать только компоненты 
            if (act == null)
            {
                return HttpNotFound();
            }
            List<string> rez = GetRecalculationPrice(act);
            ViewBag.Prices = rez;
            ViewBag.WorkPrice = CustomConstants.GetWorkPrice().ToString("0.00");
            ViewBag.WorkPriceMnog = CustomConstants.GetWorkPriceMnog().ToString("0.00");
            Application application = act.Application.FirstOrDefault();
            if (application != null)
                ViewBag.Status = application.Status;
            return View(act);
        }
        private IEnumerable<string> Split(TextReader sr, int size, bool fixedSize = true)
        {
            while (sr.Peek() >= 0)
            {
                var buffer = new char[size];
                var c = sr.ReadBlock(buffer, 0, size);
                yield return fixedSize ? new string(buffer) : new string(buffer, 0, c);
            }
        }
        private IEnumerable<string> Split(string s, int size, bool fixedSize = true)
        {
            var sr = new StringReader(s);
            return Split(sr, size, fixedSize);
        }
        // GET: Acts/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Company = new SelectList(db.Company, "Id", "Name");
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text");
            return View();
        }
        // POST: Acts/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
        [Bind(
Include = "ID,Name,City,Company,Warranty,replace,diagnostic,repair,UserId,DateCreate,UserProblemComments,DateOfShipment,CommentText"
)] Act act)
        {
            if (ModelState.IsValid)
            {
                db.Act.Add(act);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", act.UserId);
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", act.City);
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", act.Company);
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text", act.Comment);
            return View(act);
        }
        public void AddWarning(string name, string Id)
        {
            //todo: переделать процедуру добавления неисправности
            int Idjournal = int.Parse(Id);
            int idWarning = int.Parse(name);
            WarningType warningType = db.WarningType.FirstOrDefault(t => t.ID == idWarning);
            JournalOfRepair journalOfRepair = db.JournalOfRepair.FirstOrDefault(t => t.ID == Idjournal);
            if (warningType != null && journalOfRepair != null)
            {
                JournalToWarningOrComponent warningOrComponent = new JournalToWarningOrComponent();
                warningOrComponent.Type = (int)BaseEnums.JournalToWarningOrComponentOrProduct.Warning;
                warningOrComponent.IDWarningOrComponent = warningType.ID;
                journalOfRepair.JournalToWarningOrComponent.Add(warningOrComponent);
                db.SaveChanges();
            }
        }
        public void AddWarningFromUsers(String name, String Id)
        {
            int Idjournal = int.Parse(Id);
            int? idWarning = null;
            if (!string.IsNullOrEmpty(name))
            {
                idWarning = int.Parse(name);
            }
            JournalOfRepair journalOfRepair = db.JournalOfRepair.FirstOrDefault(t => t.ID == Idjournal);
            if (journalOfRepair != null)
            {
                journalOfRepair.WarningTypeFromUser = idWarning;
                db.SaveChanges();
            }
        }
        public void CloseRepair(string Id)
        {
            int Idjournal = int.Parse(Id);
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Where(t => t.ID == Idjournal).FirstOrDefault();
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            JournalOfRepairServices JournalOfRepairServices = new JournalOfRepairServices(db);
            JournalOfRepairServices.CloseRepair(journalOfRepair, user);
        }

        public ActionResult GetComponent()
        {
            var Comp = db.Component.ToList();
            _ = new List<string>();
            foreach (var item in Comp)
                item.Name = "Код:" + item.code + " " + item.Name.TrimStart(' ').TrimEnd(' ');
            SelectList d = new SelectList(Comp, "ID", "Name");
            return Json(d, JsonRequestBehavior.AllowGet);
        }
        public ActionResult component(string name, string count, string JournalOfRepairID)
        {
            int d = int.Parse(count);
            int c = int.Parse(JournalOfRepairID);
            if (name.Contains("Код:"))
            {
                name = name.TrimStart("Код:".ToCharArray());
                int countSpace = name.IndexOf(" ", StringComparison.Ordinal);
                if (countSpace != 0) name = name.Substring(countSpace + 1);
            }

            Component comp = db.Component.FirstOrDefault(t => t.Name.Contains(name));
            if (comp != null)
            {
                JournalOfRepairToComponent currentjrpcomp = db.JournalOfRepairToComponent
                .FirstOrDefault(t => t.JournalOfRepairID == c && t.ComponentOrProductID == comp.ID);
                if (currentjrpcomp != null) currentjrpcomp.Size += d;
                else
                {
                    JournalOfRepairToComponent jrpcomp = new JournalOfRepairToComponent
                    {
                        JournalOfRepairID = c,
                        ComponentOrProductID = comp.ID,
                        Type = 0,
                        Size = d
                    };
                    db.JournalOfRepairToComponent.Add(jrpcomp);
                }
                db.SaveChanges();
            }
            return Json(Componentpartial(c), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddComponentStorage(string name, string count, string JournalOfRepairIDID)
        {
            int d = int.Parse(count);
            int c = int.Parse(JournalOfRepairIDID);
            Component addComp = db.Component.Where(t => t.Name.Contains(name)).FirstOrDefault();
            if (addComp != null)
            {
                JournalOfRepairToComponent CurentActToComponent =
                db.JournalOfRepairToComponent
                .FirstOrDefault(t => t.JournalOfRepairID == c && t.ComponentOrProductID == addComp.ID && t.Type == 0);
                if (CurentActToComponent != null)
                {
                    CurentActToComponent.Size += d;
                }
                else
                {
                    JournalOfRepairToComponent actToComponent = new JournalOfRepairToComponent();
                    actToComponent.JournalOfRepairID = c;
                    actToComponent.ComponentOrProductID = addComp.ID;
                    actToComponent.Size = d;
                    db.JournalOfRepairToComponent.Add(actToComponent);
                }
                db.SaveChanges();
            }
            return Json(ComponentpartialStorage(c), JsonRequestBehavior.AllowGet);
        }
        private List<string> Componentpartial(int JournalOfRepairID)
        {
            var comp = db.JournalOfRepairToComponent
            .Where(s => s.Type == 0 && s.JournalOfRepairID == JournalOfRepairID)
            .Join(db.Component, p => p.ComponentOrProductID, t => t.ID, (p, t) => new { p.JournalOfRepairID, t.ID, t.Name, Storage = t.Storage1, t.code });
            var data = new List<string>();
            foreach (var item in comp)
            {
                int count = 0;
                string str = "";
                str += "<tr>"; // ублюдок так только делает - смешивает C#  и HTML; 
                str += "<td>";
                str += item.code.ToString();
                str += "</td>";
                str += "<td>";
                str += item.Name;
                str += "</td>";
                str += "<td>";
                str += count.ToString();
                str += "</td>";
                str += "<td>";
                str += "<button type = \"button\" id = \"CityIdButton\" onclick=\"RemoveComponent(" +
                item.ID +
                ")\" class=\"btn btn-danger\" style=\"margin - left:2px; \"> <span class=\"fa fa-remove\"></span></button>";
                str += "</td>";
                str += "</tr>";
                data.Add(str);
            }
            return data;
        }
        private List<string> ComponentpartialStorage(int JournalOfRepairID)
        {
            var comp = db.JournalOfRepairToComponent.Join(db.Component, p => p.ComponentOrProductID, t => t.ID, (p, t) => new { p.JournalOfRepairID, t.ID, t.Name, Storage = t.Storage1, t.code, p.Price });
            var sdf = db.JournalOfRepair.Find(JournalOfRepairID);
            Act act = db.Act.FirstOrDefault(t => t.ID == sdf.Act);
            var rez = comp.Where(t => t.JournalOfRepairID == JournalOfRepairID);
            List<string> data = new List<string>();
            foreach (var item in rez)
            {
                int count = 0;
                string length = "";
                length += "<tr>";
                length += "<td>";
                length += item.code.ToString();
                length += "</td>";
                length += "<td>";
                length += item.Name;
                length += "</td>";
                length += "<td>";
                length += count.ToString();
                length += "</td>";
                length += "<td>";
                // length += "<input type=\"text\" name=\"Cena\"";
                length += "<input type=\"text\" name=\"Cena\"";
                length += "onblur = 'AddPrice(this," + item.ID.ToString() + ")'";
                if (item.Price != null)
                {
                    if (item.Price.Trim(' ') != "")
                        length += "value =" + item.Price;
                }
                length += " style=\"width: 60px;\">";
                length += " Всего ";
                if (item.Price != null)
                {
                    if (item.Price.Trim(' ') != "")
                    {
                        try
                        {
                            float c = float.Parse(item.Price.Trim(' ').Replace(".", ","));
                            length += (c * count).ToString("0.00");
                        }
                        catch
                        {
                        }
                    }
                }
                length += "</td>";
                length += "<td>";
                length += "</td>";
                length += "<td>";
                length += "<button type = \"button\" tabindex=\"-1\" id = \"CityIdButton\" onclick=\"RemoveComponent(" +
                item.ID +
                ")\" class=\"btn btn-danger btn - sm\" style=\"margin - left:2px; \"> <span class=\"fa fa-remove\"></span></button>";
                length += "</td>";
                length += "</tr>";
                data.Add(length);
            }
            String htmldata = "";
            List<string> Prices = GetRecalculationPrice(act);
            htmldata = "<tr> <td></td> <td></td><td>Итого</td><td id = \"overall\">";
            htmldata += Prices[0];
            htmldata += "</td ><td></td><td></td> </tr>";
            data.Add(htmldata);
            htmldata = "";
            htmldata = "<tr> <td></td> <td></td><td>Наценка</td><td>";
            htmldata += "<input type = \"text\" name = \"Markup\" id = \"Markup\"";
            if (act.Markup != null)
                if (act.Markup != "")
                    htmldata += "value = \"" + act.Markup + "\"";
            htmldata += " style = \"width: 60px; \" >";
            htmldata += "</td ><td></td><td></td> </tr>";
            data.Add(htmldata);
            htmldata = "";
            htmldata = "<tr> <td></td> <td></td><td>Всего</td> <td id = \"Sumary\">";
            htmldata += Prices[2];
            htmldata += "</td> <td> Стоимость работ: <input type = \"text\" name = \"PriceWork\" id = \"PriceWork\" ";
            if (act.Price != null)
                if (act.Price != "")
                    htmldata += "value = \"" + act.Price + "\"";
            htmldata += "style = \"width: 60px;\"> Общее с НДС: <n id = \"AllPrice\">";
            htmldata += Prices[5];
            htmldata += "</n></td><td></td></tr >";
            data.Add(htmldata);
            htmldata = "";
            return data;
        }
        public ActionResult RemoveComponent(String id, String JournalOfRepairID)
        {
            int c = int.Parse(JournalOfRepairID);
            int idComp = int.Parse(id);
            Component RemovComp = db.Component.FirstOrDefault(t => t.ID == idComp);
            if (RemovComp != null)
            {
                JournalOfRepairToComponent CurentActToComponent =
                db.JournalOfRepairToComponent.FirstOrDefault(t => t.JournalOfRepairID == c && t.ComponentOrProductID == RemovComp.ID && t.Type == 0);
                if (CurentActToComponent != null)
                {
                    db.JournalOfRepairToComponent.Remove(CurentActToComponent);
                    db.SaveChanges();
                }
            }
            return Json(Componentpartial(c), JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemoveComponentStorage(string id, string actID)
        {
            int c = int.Parse(actID);
            int idComp = int.Parse(id);
            Component RemovComp = db.Component.FirstOrDefault(t => t.ID == idComp);
            if (RemovComp != null)
            {
                JournalOfRepairToComponent CurentActToComponent =
                db.JournalOfRepairToComponent.FirstOrDefault(t => t.JournalOfRepairID == c && t.ComponentOrProductID == RemovComp.ID && t.Type == 0);
                if (CurentActToComponent != null)
                {
                    db.JournalOfRepairToComponent.Remove(CurentActToComponent);
                    db.SaveChanges();
                }
            }
            return Json(ComponentpartialStorage(c), JsonRequestBehavior.AllowGet);
        }

        public void SetActName(string actID, string data)
        {
            if (actID == "") return;
            int c = int.Parse(actID);
            if (data == "") return;
            data = data.Trim(' ');
            Act act = db.Act.FirstOrDefault(t => t.ID == c);
            if (act != null)
            {
                act.Name = data;
                db.SaveChanges();
            }
        }
        public ActionResult SetPriceOfJournalOfRepair(String id, String data)
        {
            int Id = int.Parse(id);
            float? price = null;
            JournalOfRepair journalOfRepair = db.JournalOfRepair.Find(Id);
            if (journalOfRepair == null) return null;
            Act act = db.Act.Find(journalOfRepair.Act);
            data = data.Trim(' ');
            data = data.Replace(".", ",");
            try
            {
                if (data != "")
                    price = float.Parse(data);
            }
            catch
            {
                return null;
            }
            if (data != "")
            {
                if (price != null)
                {
                    journalOfRepair.Price = price.Value.ToString("0.00");
                    db.SaveChanges();
                }
            }
            else
            {
                journalOfRepair.Price = "";
                db.SaveChanges();
            }
            List<string> rez = GetRecalculationPrice(act, false);
            act.Price = rez[3];
            db.SaveChanges();
            rez.Insert(0, null);
            rez.Insert(1, null);
            return Json(rez, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetPrice(string id, string type, string actId, string data, string idParent)
        {
            if (id == "") return null;
            if (actId == "") return null;
            if (idParent is null)
            {
                throw new ArgumentNullException(nameof(idParent));
            }

            if (type == "") return null;
            // if (data == "") return null;
            data = data.Trim(' ');
            data = data.Replace(".", ",");
            int c = int.Parse(actId);
            int idComp = int.Parse(id);
            float? price = null;
            int Type = int.Parse(type);
            try
            {
                if (data != "")
                    price = float.Parse(data);
            }
            catch
            {
                return null;
            }
            var Request = new List<string>
            {
                idParent
            };
            var jrcompact =
            db.JournalOfRepairToComponent.Where(t => t.JournalOfRepair.Act == c && t.ComponentOrProductID == idComp && t.Type == Type).ToList();
            if (jrcompact != null)
            {
                int size = 0;
                foreach (var curentActToComponent in jrcompact)
                {
                    if (curentActToComponent != null)
                    {
                        if (data != "")
                        {
                            size += curentActToComponent.Size;
                            curentActToComponent.Price = price.Value.ToString("0.00");
                        }
                        else
                        {
                            curentActToComponent.Price = "";
                        }
                        db.SaveChanges();
                    }
                }
                var CurentActToComponent = jrcompact.FirstOrDefault();
                string length = "";
                length += "<input type=\"text\" name=\"Cena\"";
                length += "<input type=\"text\" name=\"Cena\"";
                length += "onblur = 'AddPrice(this," + CurentActToComponent.ComponentOrProductID.ToString() + "," + CurentActToComponent.Type.ToString() + ")'";
                if (CurentActToComponent.Price != null)
                    if (CurentActToComponent.Price != "")
                        length += "value =" + CurentActToComponent.Price;
                length += " style=\"width: 60px; \">";
                length += " Всего ";
                if (CurentActToComponent.Price != null)
                {
                    if (CurentActToComponent.Price != "")
                    {
                        float cd = float.Parse(CurentActToComponent.Price.Trim(' '));
                        length += ((float)cd * size).ToString("0.00");
                    }
                }
                Request.Add(length);
                List<string> rez = GetRecalculationPrice(CurentActToComponent.JournalOfRepair.Act1);
                rez.Insert(0, Request[0]);
                rez.Insert(1, Request[1]);
                return Json(rez, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        // GET: Acts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            if (act == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", act.UserId);
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", act.City);
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", act.Company);
            ViewBag.Comment = "";
            ViewBag.UserProblem = "";
            var ucom = db.RepairComments.FirstOrDefault(t => t.ID == act.UserProblemComments);
            var com = db.RepairComments.FirstOrDefault(t => t.ID == act.Comment);
            if (ucom != null)
                ViewBag.UserProblem = ucom.Text;
            if (com != null)
                ViewBag.Comment = com.Text;
            return View(act);
        }
        // POST: Acts/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,City,Company,DateCreate,DateOfShipment")] Act act,
        string selectListType, string CommentText, string UserProblemText)
        {
            Act actNew = db.Act.FirstOrDefault(t => t.ID == act.ID);
            actNew.Name = act.Name;
            actNew.City = act.City;
            actNew.Company = act.Company;
            //  actNew.UserId = act.UserId;
            if (!actNew.DateCreate.Date.Equals(act.DateCreate.Date))
                actNew.DateCreate = act.DateCreate;
            actNew.DateOfShipment = act.DateOfShipment;
            if (ModelState.IsValid)
            {
                actNew.Warranty = selectListType == "Гарантийный" ? true : false;
                if (actNew.UserProblemComments != null)
                {
                    RepairComments d = db.RepairComments.FirstOrDefault(t => t.ID == actNew.UserProblemComments);
                    d.Text = UserProblemText;
                }
                else
                {
                    RepairComments repairComments = new RepairComments();
                    repairComments.Text = UserProblemText;
                    db.RepairComments.Add(repairComments);
                    db.SaveChanges();
                    actNew.UserProblemComments = repairComments.ID;
                }
                if (actNew.Comment != null)
                {
                    var d = db.RepairComments.FirstOrDefault(t => t.ID == actNew.Comment);
                    d.Text = CommentText;
                }
                else
                {
                    RepairComments repairComments = new RepairComments();
                    repairComments.Text = CommentText;
                    db.RepairComments.Add(repairComments);
                    db.SaveChanges();
                    actNew.Comment = repairComments.ID;
                }
                db.SaveChanges();
                return RedirectToAction("IndexOfBookKeeper");
            }
            // ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", act.UserId);
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", act.City);
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", act.Company);
            return View(act);
        }
        // GET: Acts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Act act = db.Act.Find(id);
            if (act == null)
            {
                return HttpNotFound();
            }
            return View(act);
        }
        // POST: Acts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Act act = db.Act.Find(id);
            Application application = act.Application.FirstOrDefault();
            if (application != null)
            {
                application.Act.Remove(act);
                db.SaveChanges();
            }
            var component = db.JournalOfRepairToComponent.Where(t => t.ComponentOrProductID == act.ID);
            db.JournalOfRepairToComponent.RemoveRange(component);
            db.SaveChanges();
            var journalOfRepair = db.JournalOfRepair.Where(t => t.Act == act.ID);
            db.JournalOfRepair.RemoveRange(journalOfRepair);
            db.SaveChanges();
            db.Act.Remove(act);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}