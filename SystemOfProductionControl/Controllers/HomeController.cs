﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SystemOfProductionControl.Controllers
{
    public class HomeController : Controller
    {

        Entities dbContext = new Entities();
        public List<Journal> journal;

        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return Redirect("/Journals/Index");
            }
            else
            if (User.IsInRole("Склад"))
            {
                return Redirect("/Acts/IndexOfStorage");
            }
            else if (User.IsInRole("Наладчик") || (User.IsInRole("Стенд")))
            {
                return Redirect("/Applications/IndexOfRepair");
            }
            else if (User.IsInRole("Бухгалтерия"))
            {
                return Redirect("/Acts/IndexOfBookKeeper");
            }
            else if (User.IsInRole("Менеджер"))
            {
                return Redirect("/Applications/Index");
            }
            else if (User.IsInRole("Производство"))
            {
                return Redirect("/Applications/IndexManufacture");
            }


            DateTime dateTime = DateTime.Now.AddHours(4);
            dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            journal = dbContext.Journal.Where(t => t.Data >= dateTime).Where(f => f.LoginId == user.Id).OrderByDescending(t => t.Data).ToList();
            HomeViewModel model = new HomeViewModel();
            var warningQuery = (from d in dbContext.WarningType select d).ToList();
            warningQuery.Add(new WarningType { ID = -1, Name = "Нет" });
            SelectList selectList = new SelectList(dbContext.WarningType, "ID", "Name");
            ViewBag.TypeWarningID = new SelectList(warningQuery.OrderBy(t => t.ID), "ID", "Name");
            ViewBag.TypeProductID = new SelectList(dbContext.ProductType, "ID", "Name");
            return View(journal);
        }

        public ActionResult Pages()
        {


            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Страница контактов";

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(String TypeProductID, int Count, String TypeWarningID, String textArea)
        {
            int TP = Convert.ToInt32(TypeProductID);
            int TW = Convert.ToInt32(TypeWarningID);
            if (Count <= 0) Count = 1;
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                              .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            if (user != null)
                userManager.GetRoles(user.Id);

            var n = userManager.GetRoles(user.Id)[0]; ;
            var firstOrDefault = dbContext.AspNetRoles.FirstOrDefault(t => t.Name == n);

            //  if (firstOrDefault != null)
            //     journal.TypeLoginID = firstOrDefault.Id;


            //  journal.Data = DateTime.Now;

            //  journal.LoginId = user.Id;
            // journal.

            for (int i = 0; i < Count; i++)
            {

                Journal journal = new Journal()
                {
                    TypeProductID = TP,
                    LoginId = user.Id,
                    Data = DateTime.Now,
                    TypeLoginID = firstOrDefault.Id
                };

                if (TW != -1)
                {
                    journal.TypeWarningID = TW;
                }


                if (ModelState.IsValid)
                {




                    dbContext.Journal.Add(journal);
                    dbContext.SaveChanges();


                    if (textArea != null)
                        if (textArea != "")
                        {
                            var id = dbContext.Journal.OrderByDescending(t => t.Data).Select(t => t.ID).FirstOrDefault();

                            Comments comments = new Comments()
                            {
                                ID = id,
                                Text = textArea
                            };

                            dbContext.Comments.Add(comments);

                            dbContext.SaveChanges();
                        }
                }

            }
            //dbContext.Journal.
            //dbContext.Entry(Journal).State = EntityState.Modified;
            //dbContext.SaveChanges();


            return Redirect("/Home/Index");
        }
    }
}