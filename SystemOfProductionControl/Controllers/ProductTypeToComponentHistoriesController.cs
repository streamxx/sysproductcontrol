﻿using System.Data;
using System.Data.Entity;
using SystemOfProductionControl.Models;
using System.Net;
using System.Web.Mvc;
using System.Linq;

namespace SystemOfProductionControl.Controllers
{
    public class jcomphistprodtypeController : Controller
    {
        private Entities db = new Entities();

        // GET: jcomphistprodtype
        public ActionResult Index()
        {
            var ProductTypeToComponentHistory = db.ProductTypeToComponentHistory.Include(p => p.ProductType).GroupBy(t=>t.ProductType).Select(t=>t.Key);
            return View(ProductTypeToComponentHistory.ToList());
        }

        // GET: jcomphistprodtype/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductTypeToComponentHistory ProductTypeToComponentHistory = db.ProductTypeToComponentHistory.Find(id);
            if (ProductTypeToComponentHistory == null)
            {
                return HttpNotFound();
            }
            return View(ProductTypeToComponentHistory);
        }

        // GET: jcomphistprodtype/Create
        public ActionResult Create()
        {
            ViewBag.ProductTypeID = new SelectList(db.ProductType, "ID", "Name");
            return View();
        }

        // POST: jcomphistprodtype/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductTypeID,ComponentOrProductID,Type")] ProductTypeToComponentHistory ProductTypeToComponentHistory)
        {
            if (ModelState.IsValid)
            {
                db.ProductTypeToComponentHistory.Add(ProductTypeToComponentHistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductTypeID = new SelectList(db.ProductType, "ID", "Name", ProductTypeToComponentHistory.ProductTypeID);
            return View(ProductTypeToComponentHistory);
        }

        // GET: jcomphistprodtype/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var product = db.ProductType.Find(id);
            var jcomphistprodtype = product.ProductTypeToComponentHistory;

            if (jcomphistprodtype == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProductType = product;
             

            return View(jcomphistprodtype.Select(t=>new CompProductModel(t)).ToList());
        }

        // POST: jcomphistprodtype/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ProductTypeID,ComponentOrProductID,Type")] ProductTypeToComponentHistory ProductTypeToComponentHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(ProductTypeToComponentHistory).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ProductTypeID = new SelectList(db.ProductType, "ID", "Name", ProductTypeToComponentHistory.ProductTypeID);
        //    return View(ProductTypeToComponentHistory);
        //}

        // GET: jcomphistprodtype/Delete/5
        public ActionResult Delete(int? id,int? type,int? prodid)
        {
            if (id == null||type==null|| prodid==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ProductTypeToComponentHistory ProductTypeToComponentHistory =
                db.ProductTypeToComponentHistory.FirstOrDefault(t => t.ComponentOrProductID == id && t.Type == type&&t.ProductTypeID== prodid);
            if (ProductTypeToComponentHistory == null)
            {
                return HttpNotFound();
            }
            return View(ProductTypeToComponentHistory);
        }

        // POST: jcomphistprodtype/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id,int type,int prodid)
        {
            ProductTypeToComponentHistory ProductTypeToComponentHistory = db.ProductTypeToComponentHistory.FirstOrDefault(t => t.ComponentOrProductID == id && t.Type == type && t.ProductTypeID == prodid);
            db.ProductTypeToComponentHistory.Remove(ProductTypeToComponentHistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
