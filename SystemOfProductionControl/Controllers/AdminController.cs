﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private readonly Entities db = new Entities();
        public ActionResult Index() => View();
        public ActionResult IndexReport(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) date1 = DateTime.Now.AddMonths(-1);
            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;
            return View();
        }

        // [HttpPost]
        public ActionResult ComponentReport(DateTime? date1, DateTime? date2)
        {
            var models = new List<Comp_Report_Model>();
            //  ViewData["NameSortParm"] = sortOrder == "name" ? "name_desc" : "name";
            // ViewData["CountSortParm"] = sortOrder == "count" ? "count_desc" : "count";
            if (date1 == null) date1 = DateTime.Now.AddMonths(-3);
            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;
            var jrpdate = db.JournalOfRepair
            .Where(p => (p.Act1.DateCreate >= date1) & (p.Act1.DateCreate <= date2));

            var jrpcomp = new List<JournalOfRepairToComponent>();
            foreach (var item in jrpdate)
            {
                jrpcomp.AddRange(db.JournalOfRepairToComponent
                    .Where(t => (item.ID == t.JournalOfRepairID) && (t.Type == 0)));
            }

            var jrpcompgrp = jrpcomp.GroupBy(p => p.ComponentOrProductID);
            foreach (var item in jrpcompgrp)
            {
                Comp_Report_Model model = new Comp_Report_Model(item.Key)
                {
                    // Compname = item.ComponentOrProductID.ToString(),
                    CompID = item.Key,
                    //  var nx = db.Component.Where(t => t.ID == CompID);
                    //  CompJournal = item.JournalOfRepairID.ToString(),
                    //CompCount = item.Count()
                    CompCount = item.Sum(t => t.Size)
                };
                models.Add(model);
            }
            return View(models);
        }


        //    [HttpPost]
        public ActionResult Repair_Report(string sortOrder, DateTime? date1, DateTime? date2)

        {
            ViewData["NameSortParm"] = sortOrder == "name" ? "name_desc" : "name";
            ViewData["CountSortParm"] = sortOrder == "count" ? "count_desc" : "count";
            if (date1 == null) date1 = DateTime.Now.AddMonths(-3);
            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;

            string name = "";
            var jl_grp_prod = db.JournalOfRepair
                     .Where(p => (p.Act1.DateCreate >= date1)
                      & (p.Act1.DateCreate <= date2))
                     .GroupBy(p => p.ProductType);

            var models = new List<Repair_Report_Model>();

            foreach (var item in jl_grp_prod)
            {
                List<JournalOfRepair> Jl_ID_prod = db.JournalOfRepair
                .Where(t => (t.TypeProduct == item.Key.ID) & (t.Act1.DateCreate >= date1) & (t.Act1.DateCreate <= date2))
                .ToList();
                string Jl_ID_prodText = "";
                List<string> name_col = new List<string>();

                foreach (JournalOfRepair jl_row in Jl_ID_prod)
                {
                    string s = "Нет";
                    foreach (var reject in jl_row.JournalToWarningOrComponent)
                    {
                        switch (reject.Type)
                        {
                            case 1:
                                {
                                    Component component = db.Component.Find(reject.IDWarningOrComponent);
                                    if (component != null) name = component.Name;
                                    break;
                                }

                            case 0:
                                {
                                    WarningType warningType = db.WarningType.Find(reject.IDWarningOrComponent);
                                    if (warningType != null) name = warningType.Name;
                                    break;
                                }
                        }

                        if (name != "")
                        {
                            s = name;
                            if (!name_col.Contains(s))
                            {
                                name_col.Add(s);
                                Jl_ID_prodText += s + " | ";
                            }
                        }
                    }
                }

                var model = new Repair_Report_Model
                {
                    Name = item.Key.Name,
                    MalfunctionCount = item.Count(),
                    MalfunctionTypes = Jl_ID_prodText.TrimEnd('|', ' ')
                };
                models.Add(model);
            }

            switch (sortOrder)
            {
                case "name":
                    models = models.OrderBy(t => t.Name).ToList();
                    break;
                case "name_desc":
                    models = models.OrderByDescending(t => t.Name).ToList();
                    break;
                case "count":
                    models = models.OrderBy(t => t.MalfunctionCount).ToList();
                    break;
                case "count_desc":
                    models = models.OrderByDescending(t => t.MalfunctionCount).ToList();
                    break;
                default:
                    models = models.OrderBy(t => t.Name).ToList();
                    break;
            }
            return View(models);
        }

        [HttpPost]
        public ActionResult MalfunctionQuantity(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return View();
            if (date2 == null) date2 = DateTime.Now;

            ViewData["date1"] = date1;
            ViewData["date2"] = date2;
            List<NameAndCount> jrwarn = (from d in db.JournalOfRepair
                                         where (d.Act1.DateCreate >= date1) && (d.Act1.DateCreate <= date2)
                                         group d by d.WarningTypeFromUsers into g
                                         select new
                                         {
                                             Count = g.Count(),
                                             g.Key.Name

                                         })
                                                   .Select(t => new NameAndCount()
                                                   {
                                                       Count = t.Count,
                                                       Name = t.Name
                                                   }).ToList();


            return View(jrwarn);
        }

        [HttpPost]
        public ActionResult RepairQuantity(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return View();

            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;
            List<NameAndCount> jrpgrpwarning = (from d in db.JournalOfRepair
                                                where (d.Act1.DateCreate >= date1) && (d.Act1.DateCreate <= date2)
                                                group d by d.WarningTypeFromUsers into g
                                                select new
                                                {
                                                    Count = g.Count(),
                                                    g.Key.Name

                                                })
                                                   .Select(t => new NameAndCount()
                                                   {
                                                       Count = t.Count,
                                                       Name = t.Name
                                                   }).ToList();
            return View(jrpgrpwarning);
        }

        [HttpPost]
        public ActionResult MalfunctionCause(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return View();
            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;

            List<string> Heads = db.WarningTypeFromUsers.OrderBy(t => t.Name).Select(t => t.Name).ToList();
            Heads.Insert(0, "Нет");

            var typeProd = db.ProductType;

            List<NameToCountsModel> modelItems = new List<NameToCountsModel>();

            foreach (var ProductType in typeProd)
            {
                string name = ProductType.Name;
                List<int> Counts = new List<int>();

                var rez =
                    db.JournalOfRepair.Where(t => t.TypeProduct == ProductType.ID & t.Act1.DateCreate >= date1.Value & t.Act1.DateCreate <= date2.Value)
                        .GroupBy(t => t.WarningTypeFromUsers.Name)
                        .OrderBy(t => t.Key)
                        .Select(t => new { name = t.Key, Count = t.Count() })
                        .ToList();
                int count = 0;

                foreach (string h in Heads)
                {
                    if (rez.Count > count && (rez[count]?.name?.TrimEnd(' ') == h.TrimEnd(' ') || rez[count]?.name == null))
                    {
                        Counts.Add(rez[count].Count);
                        count++;
                        continue;
                    }
                    Counts.Add(0);

                }

                int AllCount = 0;
                foreach (var i in Counts)
                {
                    AllCount += i;
                }
                Counts.Add(AllCount);

                NameToCountsModel modelItem = new NameToCountsModel
                {
                    Name = name,
                    Counts = Counts
                };
                modelItems.Add(modelItem);
            }

            RepairCauseModel model = new RepairCauseModel
            {
                Heads = Heads,
                NameToCounts = modelItems
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult RepairCause(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return View();
            if (date2 == null) date2 = DateTime.Now;
            ViewData["date1"] = date1;
            ViewData["date2"] = date2;

            List<string> Heads = db.WarningTypeFromUsers
                .OrderBy(t => t.Name)
                .Select(t => t.Name)
                .ToList();

            Heads.Insert(0, "Нет");

            var typeProd = db.ProductType;

            List<NameToCountsModel> modelItems = new List<NameToCountsModel>();

            foreach (var ProductType in typeProd)
            {
                string name = ProductType.Name;
                List<int> Counts = new List<int>();

                var rez =
                    db.JournalOfRepair.Where(t => t.TypeProduct == ProductType.ID & t.Act1.DateCreate >= date1.Value & t.Act1.DateCreate <= date2.Value)
                        .GroupBy(t => t.WarningTypeFromUsers.Name)
                        .OrderBy(t => t.Key)
                        .Select(t => new { name = t.Key, Count = t.Count() })
                        .ToList();
                int count = 0;

                foreach (String h in Heads)
                {
                    if (rez.Count > count && (rez[count]?.name?.TrimEnd(' ') == h.TrimEnd(' ') || rez[count]?.name == null))
                    {
                        Counts.Add(rez[count].Count);
                        count++;
                        continue;
                    }

                    Counts.Add(0);

                }


                int AllCount = 0;
                foreach (var i in Counts)
                {
                    AllCount += i;
                }
                Counts.Add(AllCount);
                NameToCountsModel modelItem = new NameToCountsModel
                {
                    Name = name,
                    Counts = Counts
                };
                modelItems.Add(modelItem);
            }
            RepairCauseModel model = new RepairCauseModel();
            model.Heads = Heads;
            model.NameToCounts = modelItems;
            return View(model);
        }

        public ActionResult CompanyNoDataFromActs()
        {
            Entities db = new Entities();
            //var acts2 = db.Act.Where(t => t.Company1.INN == "" || t.Company1.KPP == "" || t.Company1.Phone == "").ToList();
            //var acts3 = db.Act.Where(t => t.Company1.INN == null || t.Company1.KPP == null || t.Company1.Phone == null).ToList();

            var acts = db.Act
                .Where(t =>
               t.Company1.INN == ""
            || t.Company1.KPP == ""
            || t.Company1.Phone == ""
            || t.Company1.INN == null
            || t.Company1.KPP == null
            || t.Company1.Phone == null)
            .GroupBy(t => t.Company1).ToList();
            List<Company> companies = acts.Select(act => act.Key).ToList();
            return View(companies);
        }

    }
}
