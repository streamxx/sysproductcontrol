﻿// Для ТестДрайва

using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using SystemOfProductionControl.Models;
using System.Net;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class JournalOfReturnedsController : Controller
    {
        private Entities db = new Entities();

        // GET: JournalOfReturneds
        public ActionResult Index()
        {
            var journalOfReturned = db.JournalOfReturned.Include(j => j.JournalOfRepair).Include(j => j.ProductType).Include(j => j.RepairComments);
            return View(journalOfReturned.ToList());
        }

        // GET: JournalOfReturneds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);
            if (journalOfReturned == null)
            {
                return HttpNotFound();
            }
            return View(journalOfReturned);
        }

        // GET: JournalOfReturneds/Create
        public ActionResult Create()
        {
            ViewBag.IDOfRepair = new SelectList(db.JournalOfRepair, "ID", "NumberBlock");
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name");
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text");
            return View();
        }

        // POST: JournalOfReturneds/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,TypeProduct,IDOfRepair,Number,DateOfSale,DateCreate,DateWriteOff,Comment")] JournalOfReturned journalOfReturned, String Price)
        {


            float price = 0;


            try
            {
                Price = Price.Trim(' ');
                Price = Price.Replace('.', ',');
                price = float.Parse(Price);
            }
            catch (Exception)
            {

            }

            journalOfReturned.Price = price;
            if (ModelState.IsValid)
            {

                db.JournalOfReturned.Add(journalOfReturned);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDOfRepair = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfReturned.IDOfRepair);
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfReturned.TypeProduct);
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text", journalOfReturned.Comment);
            return View(journalOfReturned);
        }

        // GET: JournalOfReturneds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }



            JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);
            if (journalOfReturned == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDOfRepair = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfReturned.IDOfRepair);
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfReturned.TypeProduct);
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text", journalOfReturned.Comment);
            return View(journalOfReturned);
        }

        // POST: JournalOfReturneds/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TypeProduct,IDOfRepair,Number,DateOfSale,DateCreate,DateWriteOff,Comment")] JournalOfReturned journalOfReturned, String Price)
        {

            float price = 0;


            try
            {
                Price = Price.Trim(' ');
                Price = Price.Replace('.', ',');
                price = float.Parse(Price);
            }
            catch (Exception)
            {

            }
            if (ModelState.IsValid)
            {
                journalOfReturned.Price = price;
                db.Entry(journalOfReturned).State = EntityState.Modified;
                db.SaveChanges();


                var s = db.ActOfTransfer.FirstOrDefault(
                    t => t.JournalOfReturned.Select(d => d.ID).Contains(journalOfReturned.ID));

                int idAct = s.ID;
                return Redirect("../ActOfTransfers/Edit?id=" + idAct.ToString());
            }
            ViewBag.IDOfRepair = new SelectList(db.JournalOfRepair, "ID", "NumberBlock", journalOfReturned.IDOfRepair);
            ViewBag.TypeProduct = new SelectList(db.ProductType, "ID", "Name", journalOfReturned.TypeProduct);
            ViewBag.Comment = new SelectList(db.RepairComments, "ID", "Text", journalOfReturned.Comment);
            return View(journalOfReturned);
        }


        //public void Returned(int id,int JournalOfRepairID)
        //{
        //    JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);

        //    journalOfReturned.DateWriteOff = DateTime.Now.AddHours(4);
        //    journalOfReturned.IDOfRepair = JournalOfRepairID;
        //    db.SaveChanges();
        //    //  int idAct = journalOfReturned.ActOfTransfer.Select(t => t.ID).FirstOrDefault();
        //    //return Redirect("../ActOfTransfers/Edit?id=" + idAct.ToString());
        //}


        public ActionResult Returned(int id)
        {
            JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);

            journalOfReturned.DateWriteOff = DateTime.Now.AddHours(4);
            db.SaveChanges();
            int idAct = journalOfReturned.ActOfTransfer.Select(t => t.ID).FirstOrDefault();
            return Redirect("../ActOfTransfers/Edit?id=" + idAct.ToString());
        }
        // GET: JournalOfReturneds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


          

            JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);
            if (journalOfReturned == null)
            {
                return HttpNotFound();
            }
            return View(journalOfReturned);
        }

        // POST: JournalOfReturneds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            JournalOfReturned journalOfReturned = db.JournalOfReturned.Find(id);


            int idAct = journalOfReturned.ActOfTransfer.Select(t => t.ID).FirstOrDefault();

            ActOfTransfer actOfTransfer = db.ActOfTransfer.Find(idAct);

            actOfTransfer.JournalOfReturned.Remove(journalOfReturned);
            db.SaveChanges();

            db.JournalOfReturned.Remove(journalOfReturned);
            db.SaveChanges();
            return Redirect("../ActOfTransfers/Edit?id="+idAct.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
