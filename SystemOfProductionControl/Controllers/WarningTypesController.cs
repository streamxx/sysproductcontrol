﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using SystemOfProductionControl.Models;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl;

namespace SystemOfProductionControl.Controllers
{
    public class WarningTypesController : Controller
    {
        private Entities db = new Entities();

        // GET: WarningTypes
        [Authorize]
        public ActionResult Index()
        {
            return View(db.WarningType.ToList());
        }

        // GET: WarningTypes/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningType warningType = db.WarningType.Find(id);
            if (warningType == null)
            {
                return HttpNotFound();
            }
            return View(warningType);
        }

        // GET: WarningTypes/Create
        [Authorize]
        public ActionResult Create()
        {

           ViewBag.ProductTypeID = new SelectList(db.ProductType, "ID", "Name");

            //var departmentsQuery = (from d in db.ProductType
            //                        select d).ToList<ProductType>();

            //departmentsQuery.Add(new ProductType { ID = -1, Name = "Общее" });  
            //ViewBag.ProductTypeID = new SelectList(departmentsQuery.OrderBy(t => t.ID), "ID", "Name");

            return View();
        }

        // POST: WarningTypes/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create( WarningType warningType)
        {
            
            //[Bind(Include = "ID,Name")]
            if (ModelState.IsValid)
            {
                db.WarningType.Add(warningType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(warningType);
        }

        // GET: WarningTypes/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningType warningType = db.WarningType.Find(id);
            if (warningType == null)
            {
                return HttpNotFound();
            }
            return View(warningType);
        }

        // POST: WarningTypes/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] WarningType warningType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(warningType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Reference");
            }
            return View(warningType);
        }

        // GET: WarningTypes/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WarningType warningType = db.WarningType.Find(id);
            if (warningType == null)
            {
                return HttpNotFound();
            }
            return View(warningType);
        }

        // POST: WarningTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            WarningType warningType = db.WarningType.Find(id);
            db.WarningType.Remove(warningType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
