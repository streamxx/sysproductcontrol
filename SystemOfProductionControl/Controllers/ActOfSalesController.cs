﻿/*
 *  Не используется на данный момент
 *  ("Продажи"?) используется в _Layout (основное окно)
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using SystemOfProductionControl.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class ActOfSalesController : Controller
    {
        private Entities db = new Entities();

        // GET: ActOfSales
        public ActionResult Index()
        {
            var actOfSale = db.ActOfSale.Include(a => a.Cities).Include(a => a.Company1);
            return View(actOfSale.ToList());
        }

        // GET: ActOfSales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfSale actOfSale = db.ActOfSale.Find(id);
            if (actOfSale == null)
            {
                return HttpNotFound();
            }
         var products=actOfSale.ActOfSaleToProducts.ToList();
            ViewBag.products = products;
            return View(actOfSale);
        }


        public ActionResult KitTable(int id)
        {
            Kit kit = db.Kit.Find(id);

            var d = PartialView("KitTable", kit);

            return PartialView("KitTable", kit);
            
        }

        // GET: ActOfSales/Create
        public ActionResult Create()
        {
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Company = new SelectList(db.Company, "Id", "Name");
            ViewBag.Kit = new SelectList(db.Kit, "Id", "Name");

            return  View();
        }

        // POST: ActOfSales/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Company,City,Comment,Data")] ActOfSale actOfSale, String[] NameProduct, int?[] Number, int[] CountProduct)
        { 
            if (ModelState.IsValid)
            {
                
                db.ActOfSale.Add(actOfSale);
                db.SaveChanges();


                for (int i = 0; i < NameProduct.Length; i++)
                {

                    String name = NameProduct[i].TrimEnd(' ');
                    Products product = db.Products.FirstOrDefault(t => t.Name == name);
                    if (product != null)
                    {

                        for (int j = 0; j < CountProduct[i]; j++)
                        {  
                        ActOfSaleToProducts ActOfSaleToProducts = new ActOfSaleToProducts();
                        ActOfSaleToProducts.IDAct = actOfSale.ID;
                      
                        ActOfSaleToProducts.IDProduct = product.Id;
                            if (Number[i] != null)
                                ActOfSaleToProducts.Number = Number[i].Value + j;

                            db.ActOfSaleToProducts.Add(ActOfSaleToProducts);
                            db.SaveChanges();
                        }
                     
                    }


                } 
 
              // db.SaveChanges();





                return RedirectToAction("Index");
            }

            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Company = new SelectList(db.Company, "Id", "Name");
            ViewBag.Kit = new SelectList(db.Kit, "Id", "Name");


            return View(actOfSale);
        }

        // GET: ActOfSales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfSale actOfSale = db.ActOfSale.Find(id);
            if (actOfSale == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", actOfSale.City);
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", actOfSale.Company);
            List<ActOfSaleToProducts> products = actOfSale.ActOfSaleToProducts.ToList();
            ViewBag.products = products;
            return View(actOfSale);
        }

        // POST: ActOfSales/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Company,City,Comment,Data")] ActOfSale actOfSale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actOfSale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.City = new SelectList(db.Cities, "ID", "Name", actOfSale.City);
            ViewBag.Company = new SelectList(db.Company, "Id", "Name", actOfSale.Company);
            return View(actOfSale);
        }

        // GET: ActOfSales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActOfSale actOfSale = db.ActOfSale.Find(id);
            if (actOfSale == null)
            {
                return HttpNotFound();
            }
            return View(actOfSale);
        }

        // POST: ActOfSales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActOfSale actOfSale = db.ActOfSale.Find(id);
            var product = db.ActOfSaleToProducts.Where(t => t.IDAct == actOfSale.ID);

            db.ActOfSaleToProducts.RemoveRange(product);
            db.SaveChanges();

            db.ActOfSale.Remove(actOfSale);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
