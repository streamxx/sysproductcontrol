﻿
using System.Linq;
using System.Web.Mvc;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class ReferenceController : Controller
    {
        // GET: Reference
       
        private Entities db = new Entities();


        [Authorize]
        public ActionResult Index()
        {
            ReferenceViewModels models = new ReferenceViewModels();
            models.WarningTypes = db.WarningType.OrderBy(t => t.ProductType.Name).ToList();
            models.ProductTypes = db.ProductType.OrderBy(t => t.Name).ToList();
            models.AspNetUsers = db.AspNetUsers.ToList();
            models.Cities = db.Cities.OrderBy(t=>t.Name).ToList();
            models.Kits = db.Kit.OrderBy(t => t.Name).ToList();
            models.Companies = db.Company.OrderBy(t => t.Name).ToList();

            return View(models);
        }
    }
}