﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Controllers
{
    public class ProductsController : Controller
    {
        private Entities db = new Entities();

        // GET: Products
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.ProductType);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.TypeId = new SelectList(db.ProductType, "ID", "Name");
            return View();
        }

        // POST: Products/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,ABRM,Comments,Link,TypeId")] Products products, HttpPostedFileBase upload)
        {



            if (ModelState.IsValid)
            {
                String fileName = "";

                try
                {
                    if (upload != null)
                    {
                        // получаем имя файла
                        fileName = products.Name.Trim() + "_" + System.IO.Path.GetFileName(upload.FileName);
                        // сохраняем файл в папку Files в проекте
                        String path = Server.MapPath("~/Content/ImgUpload/" + fileName);
                        upload.SaveAs(path);

                    }
                }
                catch (Exception)
                {
                    fileName = "";

                }

                products.Image = fileName;
                db.Products.Add(products);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }


                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {


                foreach (var item in e.EntityValidationErrors)
                {
                    foreach (var item2 in item.ValidationErrors)
                    {
                        String d = item2.PropertyName;

                        if (d == "Name")
                        {
                            ViewBag.nameError = "Укажите имя";
                        }

                    }
                }
                  
                }
                catch ( Exception e)
                {
                    

                    if (e.InnerException.InnerException.Message.Contains("АБРМ"))
                    {
                        ViewBag.АБРМError = "Такой АБРМ уже есть";
                    }


                    if (e.InnerException.InnerException.Message.Contains("Name"))
                    {
                        ViewBag.nameError = "Такое имя уже есть";
                    }


                

                }



            }

            ViewBag.TypeId = new SelectList(db.ProductType, "ID", "Name", products.TypeId);
            return View(products);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeId = new SelectList(db.ProductType, "ID", "Name", products.TypeId);
            return View(products);
        }

        // POST: Products/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,АБРМ,Comments,Image,Link,TypeId")] Products products, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {


                String fileName = "";

                try
                {
                    if (upload != null)
                    {
                        // получаем имя файла
                          fileName = products.Name.Trim()+"_"+System.IO.Path.GetFileName(upload.FileName);
                        // сохраняем файл в папку Files в проекте
                        String path = Server.MapPath("~/Content/ImgUpload/" + fileName);
                        upload.SaveAs(path);
                        
                    }
                }
                catch (Exception)
                {
                      fileName = "";
 
                }

                if (fileName != "")
                {
                    try
                    {   
                        if(products.Image!=null&&products.Image!="")
                        System.IO.File.Delete(Server.MapPath("~/Content/ImgUpload/" + products.Image));
                    }catch (Exception)
                    {

                    }
                    products.Image = fileName;
                }
                db.Entry(products).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TypeId = new SelectList(db.ProductType, "ID", "Name", products.TypeId);
            return View(products);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Products products = db.Products.Find(id);

            if(products.Image!=null)
                try
                {
                    if (products.Image != "")
                        System.IO.File.Delete(Server.MapPath("~/Content/ImgUpload/" + products.Image));
                }
                catch (Exception)
                {

                }
            db.Products.Remove(products);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
