﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using SystemOfProductionControl.Models;
using System.Web.Mvc;

namespace SystemOfProductionControl.Controllers
{
    public class KitsController : Controller
    {
        private Entities db = new Entities();

        // GET: Kits
        public ActionResult Index()
        {
            return View(db.Kit.ToList());
        }

        // GET: Kits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kit kit = db.Kit.Find(id);
            if (kit == null)
            {
                return HttpNotFound();
            }
            return View(kit);
        }

        // GET: Kits/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kits/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] Kit kit,String[] NameProduct, int[] CountProduct)
        { 


            if (ModelState.IsValid)
            {
                
                db.Kit.Add(kit);
                db.SaveChanges();

                for (int i = 0; i < NameProduct.Length; i++)
                {
                 
                    String name = NameProduct[i].TrimEnd(' ');
                    Products product = db.Products.Where(t => t.Name==name).FirstOrDefault();
                    if (product != null)
                    {
                        KitToProduct kitToProduct = new KitToProduct();
                        kitToProduct.IDKit = kit.ID;
                        kitToProduct.IDProduct = product.Id;
                        kitToProduct.Default = CountProduct[i];
                        db.KitToProduct.Add(kitToProduct);
                       
                    }
                  

                }

                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(kit);
        }

        // GET: Kits/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kit kit = db.Kit.Find(id);
            if (kit == null)
            {
                return HttpNotFound();
            }
            return View(kit);
        }


        public ActionResult GetProducts()
        {
            var Comp = db.Products.ToList();
            List<string> fd = new List<string>();
            foreach (var item in Comp)
            {
                item.Name = item.Name.TrimEnd(' ');
              
            }



            SelectList d = new SelectList(Comp, "Id", "Name");

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        // POST: Kits/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Kit kit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kit);
        }

        // GET: Kits/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kit kit = db.Kit.Find(id);
            if (kit == null)
            {
                return HttpNotFound();
            }
            return View(kit);
        }

        // POST: Kits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kit kit = db.Kit.Find(id);
 
        db.KitToProduct.RemoveRange(kit.KitToProduct);
            
                 

            db.Kit.Remove(kit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
