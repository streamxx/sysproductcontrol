//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemOfProductionControl.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class InformationGroupToHistory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InformationGroupToHistory()
        {
            this.HistoryToInformation = new HashSet<HistoryToInformation>();
            this.ParentToInformation = new HashSet<ParentToInformation>();
        }
    
        public string Name { get; set; }
        public int TypeSave { get; set; }
        public int ID_Group { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HistoryToInformation> HistoryToInformation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ParentToInformation> ParentToInformation { get; set; }
    }
}
