﻿namespace SystemOfProductionControl.Models.Warning
{
    public class WarningVmModel
    {

        public int Type { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public int Idjournal { get; set; }
        public WarningVmModel(string name, int type, int id, int Idjournal)
        {
            Name = name;
            Type = type;
            ID = id;
            this.Idjournal = Idjournal;
        }
    }
}