﻿using System.ComponentModel;

namespace SystemOfProductionControl.Models
{
    public class Repair_Report_Model
    {
        [DisplayName("Наименование:")]
        public string Name { get; set; }

        [DisplayName("Количество:")]
        public int MalfunctionCount { get; set; }

        [DisplayName("Типы Неисправностей:")]
        public string MalfunctionTypes { get; set; }

        [DisplayName("Причины Ремонта:")]
        public string RepairCause { get; set; }

    }
}