﻿using System;

namespace SystemOfProductionControl.Models
{
    public class ActAjaxModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? City { get; set; }
        public int? Company { get; set; }
        public bool? Warranty { get; set; }
        public bool? replace { get; set; }
        public bool? diagnostic { get; set; }
        public bool? repair { get; set; }
        public string TextUserProblem { get => textUserProblem; set => textUserProblem = value; }

        public string DateOfRepair;

        public string TextComments = "";
        private string textUserProblem = "";
        public ActAjaxModel(Act act)
        {
            ID = act.ID;
            Name = act.Name;
            City = act.City;
            Company = act.Company;
            Warranty = act.Warranty;
            //replace = act.replace;
            //diagnostic = act.diagnostic;
            //repair = act.repair;
            
            DateOfRepair = ((DateTime)act.DateCreate).ToString("yyyy-MM-dd");

        }
    }
}