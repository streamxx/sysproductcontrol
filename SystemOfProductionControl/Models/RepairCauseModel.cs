﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemOfProductionControl.Models
{
    public class RepairCauseModel
    {
        public List<string> Heads;
        public List<NameToCountsModel> NameToCounts;
    }
}