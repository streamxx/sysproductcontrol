﻿using System.Collections.Generic;

namespace SystemOfProductionControl.Models
{
    public class ReferenceViewModels
    {
        public IEnumerable<SystemOfProductionControl.Models.ProductType> ProductTypes { get; set; }
        public IEnumerable<SystemOfProductionControl.Models.WarningType> WarningTypes { get; set; }
        public IEnumerable<SystemOfProductionControl.Models.Cities> Cities { get; set; }
        public IEnumerable<SystemOfProductionControl.Models.Company> Companies { get; set; }
        public IEnumerable<SystemOfProductionControl.Models.Kit> Kits { get; set; }
        public IEnumerable<SystemOfProductionControl.Models.AspNetUsers> AspNetUsers { get; set; }
    
    
    }
}