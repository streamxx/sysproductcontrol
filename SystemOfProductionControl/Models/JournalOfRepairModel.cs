﻿using System;
using System.Collections.Generic;
using SystemOfProductionControl.Models.Warning;
namespace SystemOfProductionControl.Models
{
    public class JournalOfRepairModel
    {
        private JournalOfRepair journalOfRepair;
        public int ID { get; set; }
        public Act Act { get; set; }

        public int ActId { get; set; }
        public int City { get; set; }
        public int Company { get; set; }
        public int TypeProductID { get; set; }
        public string TypeProduct { get; set; }
        public string NumberBlock { get; set; }
        public int? WarningComments { get; set; }
        public string WarningCommentsText { get; set; }
        public DateTime? DateOfDelivery { get; set; }
        public string LoginId { get; set; }
        public DateTime? DateOfSale { get; set; }
        public int? WarningTypeFromUser { get; set; }
        public string WarningTypeFromUserText { get; set; }
        public bool Replace { get; set; }
        public bool WaitingComponent { get; set; }
        public string Price = "";
        public string PriceCoeff = "";

        public List<WarningVmModel> WarningList = new List<WarningVmModel>();

        private readonly Entities db = new Entities();
        public JournalOfRepairModel(JournalOfRepair journalOfRepair)
        {
            WarningList = new List<WarningVmModel>();
            this.journalOfRepair = journalOfRepair;
            ID = journalOfRepair.ID;
            Act = journalOfRepair.Act1;
            ActId = journalOfRepair.Act.Value;
            TypeProductID = journalOfRepair.TypeProduct;
            TypeProduct = journalOfRepair.ProductType.Name;
            NumberBlock = journalOfRepair.NumberBlock;
            DateOfDelivery = journalOfRepair.DateOfDelivery;
            LoginId = journalOfRepair.LoginId;
            DateOfSale = journalOfRepair.DateOfSale;
            WarningTypeFromUser = journalOfRepair.WarningTypeFromUser;
            Replace = journalOfRepair.Replace;
            WaitingComponent = journalOfRepair.WaitingComponent;
            if (journalOfRepair.WarningComments != null && journalOfRepair.RepairComments != null)
                WarningCommentsText = journalOfRepair.RepairComments.Text;
            if (WarningTypeFromUser != null)
                WarningTypeFromUserText = journalOfRepair.WarningTypeFromUsers.Name;
            if (journalOfRepair.Price != null) Price = journalOfRepair.Price;
            if (journalOfRepair.ProductType.CoefficientPrice != null) PriceCoeff = journalOfRepair.ProductType.CoefficientPrice.Value.ToString("0.00");

            foreach (var item in this.journalOfRepair.JournalToWarningOrComponent)
            {
                string a = db.WarningType.Find(item.IDWarningOrComponent)?.Name;

                if (item.Type == 0)
                {
                }
                else if (item.Type == 1)
                {
                    a = "Компонент вышел из строя: " + a;
                }
                else if (item.Type == 2)
                {
                    a = "Оборудование вышло из строя: " + a;
                }

                WarningList.Add(new WarningVmModel(a, item.Type, item.IDWarningOrComponent, item.IDJournal));
            }
          
        }
    }
}