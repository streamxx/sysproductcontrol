﻿namespace SystemOfProductionControl.Models
{
    public class CompProductModel
    {
        readonly Entities Entities = new Entities();
        public int ID { get; set; }
        public int Idjournal { get; set; }
        public string Name { get; set; }
        public string Storage = "";
        public string CodeOrABRM = "";
        public int Size { get; set; }
        public int Type;
        public string Price;
        public int Issued = 0;

        public CompProductModel(JournalOfRepairToComponent jrepcomp)
        {
            Type = jrepcomp.Type;
            ID = jrepcomp.ComponentOrProductID;
            Idjournal = jrepcomp.JournalOfRepairID;
            Size = jrepcomp.Size;
            Price = jrepcomp.Price;


            if (jrepcomp.Issued != null) Issued = jrepcomp.Issued.Value;

            switch (Type)
            {
                case 0:
                    {
                        Component component = Entities.Component.Find(ID);
                        if (component == null) break;
                        Name = component.Name.TrimEnd(' ');
                        Storage = component.Storage1.Name; // компонент может быть сложным блоком, и они все хранятся на складе КИ
                        CodeOrABRM = component.code.ToString();
                        break;
                    }
                case 1:
                    {
                        Products products = Entities.Products.Find(ID);
                        if (products == null) break;
                        Name = products.Name;
                        Storage = "ГП/КДК";
                        CodeOrABRM = products.ABRM;
                        break;
                    }

            }

        }
        public CompProductModel(ProductTypeToComponentHistory jrepcompHist)
        {
            Type = jrepcompHist.Type;
            ID = jrepcompHist.ComponentOrProductID;
            switch (Type)
            {
                case 0:
                    {
                        if (Entities.Component.Find(ID) == null) break;
                        //  Name = Entities.Component.Find(ID).Name.TrimEnd(' '); вообще ни на что не влияет
                        //  Storage = Entities.Component.Find(ID).Storage1.Name;

                        // CodeOrABRM = Entities.Component.Find(ID).code.ToString();
                        break;
                    }
                case 1:
                    {
                        if (Entities.Products.Find(ID) == null) break;
                        // Name = Entities.Products.Find(ID).Name;
                        //  Storage = "ГП/КjkljkljkljkДК";
                        //Storage = Entities.Component.Find(ID).Storage1.Name;
                        //  CodeOrABRM = Entities.Products.Find(ID).ABRM;
                        break;
                    }
            }

        }

    }
}