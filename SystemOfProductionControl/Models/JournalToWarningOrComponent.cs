//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemOfProductionControl.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class JournalToWarningOrComponent
    {
        public int IDJournal { get; set; }
        public int IDWarningOrComponent { get; set; }
        public int Type { get; set; }
    
        public virtual JournalOfRepair JournalOfRepair { get; set; }
    }
}
