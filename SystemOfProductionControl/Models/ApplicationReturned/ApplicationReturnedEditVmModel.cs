﻿using System;
using System.Collections.Generic;

namespace SystemOfProductionControl.Models.ApplicationReturned_namespace
{
    public class ApplicationReturnedEditVmModel
    {
        public int ID { get; set; }
        public int CompanyID = -1;
        public string Company = "";
        public string CompanyCity = "";
        public string CompanyKPP = "";
        public string CompanyINN = "";
        public string CompanyPhone = "";
        public string Comment = "";

        public DateTime DateCreate { get; set; }

        public DateTime CompletionDate { get; set; }

        public List<ActOfTransfer_namespace.ActOfTransferIndexVmModel> ActOfTransferIndexVmModels = new List<ActOfTransfer_namespace.ActOfTransferIndexVmModel>();
        private Entities db = new Entities();
        public ApplicationReturnedEditVmModel(SystemOfProductionControl.Models.ApplicationReturned applicationReturned)
        {
            ID = applicationReturned.ID;
            CompanyID = applicationReturned.Company;
            DateCreate = applicationReturned.DateCreate;
            CompletionDate = applicationReturned.CompletionDate;

            if (applicationReturned.Comment != null)
            {
                RepairComments repairComment = applicationReturned.RepairComments;
                Comment = repairComment.Text;
            }
             
            Company company = applicationReturned.Company1; 
            if (company != null)
            {
                CompanyID = company.Id;
                if (company.Name != null)
                    Company = company.Name;
                if (company.KPP != null)
                    CompanyKPP = company.KPP;
                if (company.INN != null)
                    CompanyINN = company.INN;
                if (company.Phone != null)
                    CompanyPhone = company.Phone;
                if (company.Cities != null)
                    CompanyCity = company.Cities.Name;
            }


            foreach (var actOfTransfer in applicationReturned.ActOfTransfer)
            {
                ActOfTransfer_namespace.ActOfTransferIndexVmModel actOfTransferIndexVmModel = new ActOfTransfer_namespace.ActOfTransferIndexVmModel(actOfTransfer);
                ActOfTransferIndexVmModels.Add(actOfTransferIndexVmModel);
            }
        }

    }
}