﻿using System.Collections.Generic;

namespace SystemOfProductionControl.Models
{
    public class HomeViewModel
    {
        public List<Journal> journals { get; set; }
        public List<ProductType> ProductTypes { get; set; }
        public List<WarningType> WarningTypes { get; set; }
    }
}