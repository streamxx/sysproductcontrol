﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SystemOfProductionControl.Models.ActOfTransfer_namespace
{
    public class ActOfTransferIndexVmModel
    {
        public int ID { get; set; }
        public System.DateTime DateCreate { get; set; }
        public string Administrative = "";
        public string TransferMail = "";
        public int ApplicationReturnedId = -1;
        public List<string> JournalOfReturnedGroupProduct = new List<string>();
        public int NomerInAct = 1;
        public ActOfTransferIndexVmModel(SystemOfProductionControl.Models.ActOfTransfer actOfTransfer)
        {

            ID = actOfTransfer.ID;
            DateCreate = actOfTransfer.DateCreate;
            Administrative = actOfTransfer.Administrative;
            TransferMail = actOfTransfer.TransferMail;
            ApplicationReturnedId = actOfTransfer.ApplicationReturned.FirstOrDefault().ID;
            NomerInAct = actOfTransfer.NomerInAct.GetValueOrDefault(1);

            var s = actOfTransfer.JournalOfReturned.GroupBy(t => t.ProductType)
                .Select(t => new {product = t.Key, count = t.Count()}).ToList();

            foreach (var item in s)
            {
                String prodAndCount = ""; 
                prodAndCount += item.product.Name.TrimEnd(' ');
                if (item.count > 1)
                {
                    prodAndCount += " - " + item.count.ToString();
                } 
                JournalOfReturnedGroupProduct.Add(prodAndCount);
            }

        }
    }
}