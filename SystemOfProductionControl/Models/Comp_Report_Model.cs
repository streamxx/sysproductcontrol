﻿using System.ComponentModel;
using System.Linq;

namespace SystemOfProductionControl.Models
{
    public class Comp_Report_Model
    {

        [DisplayName("ID компонента:")]
        public int CompID { get; set; }

        private readonly Entities db = new Entities();


        [DisplayName("Имя компонента:")]
        public string Compname { get; set; }


        public Comp_Report_Model(int CompID)

        {

            string Compnamex = db.Component.Where(p => p.ID == CompID).FirstOrDefault().Name.ToString();
            Compname = Compnamex;

        }



        [DisplayName("Количество компонентов:")]
        public int CompCount { get; set; }


        [DisplayName("Имя журнала:")]
        public string CompJournal { get; set; }


        [DisplayName("Количество:")]
        public int MalfunctionCount { get; set; }

        [DisplayName("Типы Неисправностей:")]
        public string MalfunctionTypes { get; set; }

        [DisplayName("Причины Ремонта:")]
        public string RepairCause { get; set; }

    }
}