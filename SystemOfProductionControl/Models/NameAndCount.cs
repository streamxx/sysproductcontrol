﻿using System.ComponentModel;

namespace SystemOfProductionControl.Models
{
    public class NameAndCount

    {
        [DisplayName("Название")]
        public string Name { get; set; }
        [DisplayName("Количество")]
        public int Count { get; set; }
    }
}