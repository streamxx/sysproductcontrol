﻿using System;
using System.Collections.Generic;
using SystemOfProductionControl.Models.Warning;
namespace SystemOfProductionControl.Models.JournalOfrepair
{
    public class JournalOfRepairAndComponentModel
    {
        private readonly JournalOfRepair journalOfRepair;
        public int ID { get; set; }
        public int ActId { get; set; }
        public bool Type { get; set; }
        public int City { get; set; }
        public int Company { get; set; }
        public int TypeProductID { get; set; }
        public string TypeProduct { get; set; }
        public string NumberBlock { get; set; }
        public int? WarningComments { get; set; }
        public string WarningCommentsText { get; set; }
        public DateTime? DateOfDelivery { get; set; }
        public string LoginId { get; set; }
        public DateTime? DateOfSale { get; set; }
        public int? WarningTypeFromUser { get; set; }
        public string WarningTypeFromUserText { get; set; }
        public bool Replace { get; set; }
        public bool WaitingComponent { get; set; }
        public List<WarningVmModel> WarningList = new List<WarningVmModel>();
        public List<CompProductModel> Components = new List<CompProductModel>();
        private Entities db = new Entities();
        public JournalOfRepairAndComponentModel(JournalOfRepair journalOfRepair)
        {
            this.journalOfRepair = journalOfRepair ?? throw new ArgumentNullException(nameof(journalOfRepair));
            ID = journalOfRepair.ID;
            ActId = journalOfRepair.Act.Value;
            Type = journalOfRepair.Act1.Warranty.Value;
            TypeProductID = journalOfRepair.TypeProduct;
            TypeProduct = journalOfRepair.ProductType.Name;
            NumberBlock = journalOfRepair.NumberBlock;
            WarningComments = journalOfRepair.WarningComments;
            if ((WarningComments != null) & journalOfRepair.RepairComments != null) WarningCommentsText = journalOfRepair.RepairComments.Text;

            DateOfDelivery = journalOfRepair.DateOfDelivery;
            LoginId = journalOfRepair.LoginId;
            DateOfSale = journalOfRepair.DateOfSale;
            WarningTypeFromUser = journalOfRepair.WarningTypeFromUser;
            if (WarningTypeFromUser != null) WarningTypeFromUserText = journalOfRepair.WarningTypeFromUsers.Name;
            Replace = journalOfRepair.Replace;
            WaitingComponent = journalOfRepair.WaitingComponent;

            foreach (JournalToWarningOrComponent journalToWarningOrComponent in this.journalOfRepair.JournalToWarningOrComponent)
            {
                string a = "";
                if (journalToWarningOrComponent.Type == 0)
                    a = db.WarningType.Find(journalToWarningOrComponent.IDWarningOrComponent)?.Name;
                else if (journalToWarningOrComponent.Type == 1)
                {
                    a = db.Component.Find(journalToWarningOrComponent.IDWarningOrComponent)?.Name;
                    a = "Компонент вышел из строя: " + a;
                }
                else if (journalToWarningOrComponent.Type == 2)
                {
                    a = db.Products.Find(journalToWarningOrComponent.IDWarningOrComponent)?.Name;
                    a = "Оборудование вышло из строя: " + a;
                }
                WarningList.Add(new WarningVmModel(a, journalToWarningOrComponent.Type, journalToWarningOrComponent.IDWarningOrComponent, journalToWarningOrComponent.IDJournal));
            }
            foreach (var item in this.journalOfRepair.JournalOfRepairToComponent)
            {
                CompProductModel model = new CompProductModel(item);
                Components.Add(model);
            }
        }
    }
}
