﻿using System.Collections.Generic;
using System.Linq;
using SystemOfProductionControl.Models.JournalOfrepair;

namespace SystemOfProductionControl.Models.App_namespace
{
    public class ApplicationEditOfRepairModel
    {
        public Application application;

        public int CompanyID = -1;
        public int CityId = -1;
        public string City = "";
        public string Company = "";
        public string CompanyCity = "";
        public string CompanyPhone = "";
        public string CompanyKPP = "";
        public string CompanyINN = "";
        public string RepairMail = "";
        public string Consignment = "";
        public List<JournalOfRepairAndComponentModel> JournalOfRepairs;


        public ApplicationEditOfRepairModel(Application application, List<JournalOfRepairAndComponentModel> JournalOfRepairs)
        {
            this.application = application;
            this.JournalOfRepairs = JournalOfRepairs;
            Act act = application.Act.FirstOrDefault(); // у приложения же 2 акта, почему тут используется 1?!

            if (act != null)
            {
                Company company = act.Company1;
                Cities city = act.Cities;
                if (company != null)
                {
                    CompanyID = company.Id;
                    if (company.Name != null)
                        Company = company.Name;
                    if (company.Phone != null)
                        CompanyPhone = company.Phone;
                    if (company.Cities != null)
                        CompanyCity = company.Cities.Name;
                    if (company.KPP != null)
                        CompanyKPP = company.KPP;
                    if (company.INN != null)
                        CompanyINN = company.INN;
                }
                if (city != null)
                {
                    CityId = city.ID;
                    City = city.Name;
                }

            }



        }

    }
}