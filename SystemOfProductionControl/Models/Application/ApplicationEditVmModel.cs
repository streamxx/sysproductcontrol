﻿using System.Collections.Generic;
using System.Linq;
namespace SystemOfProductionControl.Models.App_namespace
{
    public class ApplicationEditVmModel
    {
        private readonly Entities db = new Entities();
        public Application application;
        public int CityId = -1;
        public int CompanyID = -1;
        public string City = "";
        public string Company = "";
        public string CompanyCity = "";
        public string CompanyKPP = "";
        public string CompanyINN = "";
        public string CompanyPhone = "";
        public string RepairMail = "";
        public string Consignment = "";
        public List<JournalOfRepairModel> JournalOfRepairs;
        public List<ActOfTransfer_namespace.ActOfTransferIndexVmModel> ActOfTransferIndexVmModels = new List<ActOfTransfer_namespace.ActOfTransferIndexVmModel>();
        public ApplicationEditVmModel(Application application, List<JournalOfRepairModel> JournalOfRepairs)
        {
            this.application = application;
            Act act = application.Act.FirstOrDefault();
            if (act != null)
            {
                Company company = act.Company1;
                var city = act.Cities;
                if (company != null)
                {
                    CompanyID = company.Id;
                    Company = company.Name;
                    CompanyCity = company.Cities.Name;
                    if (company.KPP != null) CompanyKPP = company.KPP;
                    if (company.INN != null) CompanyINN = company.INN;
                    if (company.Phone != null) CompanyPhone = company.Phone;
                }
                CityId = city.ID;
                City = city.Name;
            }
            ApplicationToLinkedDocument applinkeddoc = application.ApplicationToLinkedDocument.FirstOrDefault(t => t.Type == 0);
            if (applinkeddoc != null)
            {
                MailOfRepair mailOfRepair = db.MailOfRepair.FirstOrDefault(t => t.ID == applinkeddoc.IDDoc);
                if (mailOfRepair != null) RepairMail = mailOfRepair.Text;
            }

            ApplicationToLinkedDocument applinkeddoc1 = application.ApplicationToLinkedDocument.FirstOrDefault(t => t.Type == 1);
            if (applinkeddoc1 != null && db.Consignment.FirstOrDefault(t => t.ID == applinkeddoc1.IDDoc) != null)
                Consignment = db.Consignment.FirstOrDefault(t => t.ID == applinkeddoc1.IDDoc).Text;

            this.JournalOfRepairs = JournalOfRepairs;
        }
    }
}