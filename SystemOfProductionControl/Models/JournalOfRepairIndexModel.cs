﻿using System;

namespace SystemOfProductionControl.Models
{
    public class journalOfRepairIndexModel
    {
        public int ID { get; set; }
        public DateTime DateCreate { get; set; } 
        public DateTime? DateOfShipment { get; set; } 
        public string Name { get; set; }
        public String City { get; set; }
        public String Company { get; set; }
        public String Phone { get; set; }
        public Nullable<bool> Warranty { get; set; }
  
        
        public int state { get; set; }

        public String TextComments = "";
        public String TextUserProblem = "";

        public String Count ="";
        public String BlocksType = "";
        public int Shipment;

       public journalOfRepairIndexModel(Act act)
        {
            ID = act.ID;
            Name = act.Name;
            City = act.Cities.Name;
            Company = act.Company1.Name;
            Phone = act.Company1.Phone;

            DateCreate = act.DateCreate;
            DateOfShipment = act.DateOfShipment;
            Warranty = act.Warranty;

        }

    }
}