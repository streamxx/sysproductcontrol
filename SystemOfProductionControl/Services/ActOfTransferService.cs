﻿using System;
using System.Linq;
using SystemOfProductionControl.Models;
namespace SystemOfProductionControl.Services
{
    public class ActOfTransferService
    {
        public Entities db;
        public ActOfTransferService(Entities db)
        {
            this.db = db;
        }
        public ActOfTransfer CreateActOfTransfer(int num)
        {
            ActOfTransfer actOfTransfer = new ActOfTransfer
            {
                DateCreate = DateTime.Now,
                NomerInAct = num
            };
            db.ActOfTransfer.Add(actOfTransfer);
            db.SaveChanges();
            return actOfTransfer;
        }
        public void AddProducts(ActOfTransfer actOfTransfer, int count, int typeProduct, String[] number,
        DateTime?[] DateOfSale, float price, String comment = null)
        {
            for (int i = 0; i < count; i++)
            {
                AddProduct(actOfTransfer, typeProduct, number[i], DateOfSale[i], price, comment);
            }
        }
        public void AddProduct(ActOfTransfer actOfTransfer, int typeProduct, String number,
        DateTime? DateOfSale, float price, String comment = null)
        {
            JournalOfReturned journalOfReturned = new JournalOfReturned();
            journalOfReturned.DateCreate = DateTime.Now.AddHours(4);
            journalOfReturned.TypeProduct = typeProduct;
            journalOfReturned.Number = number;
            journalOfReturned.DateOfSale = DateOfSale;
            journalOfReturned.Price = price;
            if (comment != null)
            {
                comment = comment.TrimEnd(' ');
                if (comment != "")
                {
                    RepairComments comments = new RepairComments();
                    comments.Text = comment;
                    db.RepairComments.Add(comments);
                    db.SaveChanges();
                }
            }
            AddProduct(actOfTransfer, journalOfReturned);
        }
        public void AddProduct(ActOfTransfer actOfTransfer, JournalOfReturned journalOfReturned)
        {
            actOfTransfer.JournalOfReturned.Add(journalOfReturned);
            db.SaveChanges();
        }
        public void Delete(ActOfTransfer actOfTransfer)
        {
            var journals = actOfTransfer.JournalOfReturned.ToList();
            actOfTransfer.ApplicationReturned.Clear();
            db.SaveChanges();
            db.JournalOfReturned.RemoveRange(journals);
            db.SaveChanges();
            db.ActOfTransfer.Remove(actOfTransfer);
            db.SaveChanges();
        }
    }
}