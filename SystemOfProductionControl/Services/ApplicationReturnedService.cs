﻿using System;
using System.Linq;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Services
{
    public class ApplicationReturnedService
    {
        private Entities db = new Entities();

        public ApplicationReturnedService(Entities db)
        {
            this.db = db;
        }


        public ApplicationReturned CreateApplicationReturned(int docNum, int CompanyId, DateTime dateCreate, DateTime dateComplition)
        {
            ApplicationReturned applicationReturned = new ApplicationReturned
            {
                ID = docNum,
                Company = CompanyId,
                DateCreate = DateTime.Now.AddHours(4),
                CompletionDate = dateComplition
            };
            db.ApplicationReturned.Add(applicationReturned);
            db.SaveChanges(); 
            return applicationReturned;
        }


        public void AddAdditionalAgreementDocName(int additionalAgreementId, String text)
        {
            AdditionalAgreement additionalAgreement = db.AdditionalAgreement.Find(additionalAgreementId);

            if (additionalAgreement != null)

            {

                additionalAgreement.DocumentName = text.TrimEnd(' ');
                db.SaveChanges();

            }

        }

        public void AddAdditionalAgreement(ApplicationReturned applicationReturned, DateTime dateComplition)
        {
            AdditionalAgreement additionalAgreement = new AdditionalAgreement();
            additionalAgreement.ApplicationReturnedId = additionalAgreement.ID;
            additionalAgreement.CompletionDate = dateComplition;
            db.AdditionalAgreement.Add(additionalAgreement);
            db.SaveChanges(); 
        }

        public void Delete(ApplicationReturned applicationReturned)
        {
            var acts = applicationReturned.ActOfTransfer.ToList();
            ActOfTransferService actOfTransferService = new ActOfTransferService(db);

            foreach (var actOfTransfer in acts)
            {
                actOfTransferService.Delete(actOfTransfer);
            }

            var s = db.AdditionalAgreement.Where(t => t.ApplicationReturnedId == applicationReturned.ID);

            db.AdditionalAgreement.RemoveRange(s);
            db.SaveChanges();

            db.ApplicationReturned.Remove(applicationReturned);
            db.SaveChanges();
        }

    }

}