﻿using System;
using System.Collections.Generic;
using System.Linq;
using SystemOfProductionControl.Models;
namespace SystemOfProductionControl.Services
{
    public class JournalOfRepairServices
    {
        public JournalOfRepairServices(Entities db)
        {
            this.db = db ?? throw new ArgumentNullException(nameof(db));
        }

        public Entities db;


        public void CloseRepair(JournalOfRepair journalOfRepair, ApplicationUser user)
        {
            if (journalOfRepair != null)
            {
                journalOfRepair.DateOfDelivery = DateTime.Now.AddHours(4);
                journalOfRepair.LoginId = user.Id;
                db.SaveChanges();

                List<JournalOfRepairToComponent> jrepcomcomp = journalOfRepair.JournalOfRepairToComponent.ToList();

                foreach (var item in jrepcomcomp)
                {
                    List<ProductTypeToComponentHistory> jrepprodtype = db.ProductTypeToComponentHistory
                    .Where(t => t.ComponentOrProductID == item.ComponentOrProductID && t.Type == item.Type && t.ProductTypeID == journalOfRepair.TypeProduct)
                    .ToList();

                    if (!jrepprodtype.Any())
                    {
                        ProductTypeToComponentHistory prodtypehist = new ProductTypeToComponentHistory
                        {
                            Type = item.Type,
                            ComponentOrProductID = item.ComponentOrProductID,
                            ProductTypeID = journalOfRepair.ProductType.ID
                        };
                        db.ProductTypeToComponentHistory.Add(prodtypehist); // what а зачем вести историю, если уже есть jrep?

                    }
                }

                Application application = journalOfRepair.Act1.Application.FirstOrDefault();
                application.Act.FirstOrDefault().ActCondition.IsRepairCompleted = true;
                    var acts = application.Act.ToList();

                    foreach (var act in acts)
                    {
                    List<int> s = db.JournalOfRepair
                            .Where(t => t.Act == act.ID)
                            .Select(t => t.ID)
                            .ToList();

                    List<int> jrcomp = db.JournalOfRepairToComponent
                            .Where(t =>
                        s.Contains(t.JournalOfRepairID) &&
                        t.Type == 0).Select(t => t.ComponentOrProductID)
                        .ToList();

                        if (jrcomp != null && jrcomp.Count != 0)
                        {
                            var compStorage = db.Component.Where(t => jrcomp.Contains(t.ID) && t.Storage1.Name.Contains("КИ")).ToList();
                            if (compStorage != null && compStorage.Count != 0)

                                continue;
                        }
                        act.ReadyStorage = true;
                        act.ActCondition.ProcessedStorage = true;
                        db.SaveChanges();
                    }
                
            }
        }


        public void AddJournals
            (
            int TypeProductID,
            Act act,
            int Count,
            int? WarningTypeFromUsers,
            string[] NumberBlock,
            DateTime?[] DateOfSale,
            string WarningComments,
            DateTime? DateOfDelivery = null,
            int? productReverse = null)

        {
            List<JournalOfRepair> jrList = new List<JournalOfRepair>();

            for (int i = 0; i < Count; i++)
                jrList.Add(AddJournal(TypeProductID, act.ID, WarningTypeFromUsers, NumberBlock[i], DateOfSale[i], WarningComments, DateOfDelivery));
            _ = new ActServices(db);
            //   ActServices.SetActReadyStorageCancel(ActId); // what
            // ActServices.SetActReadyCancel(ActId); // what они же и так по умолчанию такие
            _ = act.Application.FirstOrDefault();

            //  application.Act.FirstOrDefault().ActCondition.OnRepair = true; wtf? с чего бы это при добавлении в журнал автоматом ставить в ремонт? Точнее, верно, но зачем тогда еще кнопку добавить в ремонт добавлять
            // Надо было тогда изначально все делать одним действием 

            if (productReverse != null)
            {
                foreach (var journalOfRepair in jrList)
                {
                    JournalOfRepairToComponent jrcomponent = new JournalOfRepairToComponent
                    {
                        Type = 1,
                        ComponentOrProductID = productReverse.Value,
                        JournalOfRepairID = journalOfRepair.ID,
                        Size = 1
                    };
                    db.JournalOfRepairToComponent.Add(jrcomponent);
                }
                db.SaveChanges();
            }
        }


        public JournalOfRepair AddJournal(int TypeProductID, int ActId,
        int? WarningTypeFromUsers, string NumberBlock, DateTime? DateOfSale, string WarningComments, DateTime? DateOfDelivery = null, int? productReverse = null)
        {
            JournalOfRepair journalOfRepair = AddJournal(TypeProductID, ActId,
            WarningTypeFromUsers, NumberBlock, DateOfSale, WarningComments, DateOfDelivery);
            db.SaveChanges();
            if (productReverse != null)
            {
                JournalOfRepairToComponent component = new JournalOfRepairToComponent
                {
                    Type = 1,
                    ComponentOrProductID = productReverse.Value,
                    JournalOfRepairID = journalOfRepair.ID
                };
                db.JournalOfRepairToComponent.Add(component);
                db.SaveChanges();
            }
            return journalOfRepair;
        }

        private JournalOfRepair AddJournal(int TypeProductID, int ActId,
        int? WarningTypeFromUsers, string NumberBlock, DateTime? DateOfSale, string WarningComments, DateTime? DateOfDelivery = null)
        {
            JournalOfRepair journalOfRepair = new JournalOfRepair
            {
                WarningTypeFromUser = WarningTypeFromUsers,
                Act = ActId,
                TypeProduct = TypeProductID,
                NumberBlock = NumberBlock,
                DateOfDelivery = DateOfDelivery
            };

            if (DateOfSale != null)
                journalOfRepair.DateOfSale = DateOfSale;
            db.JournalOfRepair.Add(journalOfRepair);
            db.SaveChanges();
            int WarningCommentsId = -1;
            if (WarningComments != null)
            {
                RepairComments Warning;
                if (WarningComments != "")
                {
                    Warning = new RepairComments
                    {
                        Text = WarningComments
                    };
                    var d = db.RepairComments.Add(Warning);
                    db.SaveChanges();
                    WarningCommentsId = d.ID;
                }
            }
            if (WarningCommentsId != -1)
            {
                journalOfRepair.WarningComments = WarningCommentsId;
            }

            return journalOfRepair;
        }
    }
}