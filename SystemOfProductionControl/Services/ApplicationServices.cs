﻿using System.Collections.Generic;
using System.Linq;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Services
{
    public class ApplicationServices
    {
        private readonly Entities db;

        public ApplicationServices(Entities Entities)
        {
            db = Entities;

        }


        public bool Delete(Application application)
        {
            ICollection<Act> acts = application.Act;
            var actRemove = acts.ToList();
            var ActsID = acts.Select(t => (int?)t.ID).ToList();

            var journals = db.JournalOfRepair.Where(t => ActsID.Contains(t.Act));

            foreach (var journalOfRepair in journals)
            {
                db.JournalOfRepairToComponent.RemoveRange(journalOfRepair.JournalOfRepairToComponent);
                db.JournalToWarningOrComponent.RemoveRange(journalOfRepair.JournalToWarningOrComponent);
            }
            db.SaveChanges();

            db.JournalOfRepair.RemoveRange(journals);
            db.SaveChanges();



            db.ApplicationToLinkedDocument.RemoveRange(application.ApplicationToLinkedDocument);
            db.SaveChanges();

            foreach (var act in acts)
            {
                // application.Act.Remove(act);
                act.Application.Remove(application);
            }

            db.SaveChanges();

            foreach (var item in actRemove)
            {
                db.ActCondition.Remove(item.ActCondition);
            }

            db.Act.RemoveRange(actRemove);
            db.SaveChanges();

            db.Application.Remove(application);
            db.SaveChanges();

            return true;
        }
    }
}