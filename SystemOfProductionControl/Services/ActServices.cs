﻿using System;
using System.Linq;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.Services
{
    public class ActServices
    {
        public Entities db;
        public ActServices(Entities db)
        {
            this.db = db;
        }
        public Act CreateAct(int? CityID, int? CompanyID, bool Warranty = false)
        {
            Act act = new Act
            {
                Name = "",
                City = CityID,
                DateCreate = DateTime.Now.AddHours(4),
                Priority = 0,
                Company = CompanyID,
                Warranty = Warranty,
            };

            ActCondition actCondition = new ActCondition
            {
                Completed = false,
                Paid = false,
                CreateApplication = true, // Создана заявка - акт приема оборудования
                OnRepair = false, // В ремонте, создана заявка ремонта, точнее, был ли хоть раз в ремонте. Действует бессрочно
                IsRepairCompleted = false, // Завершен ремонт
                ProcessedStorage = false, // Акт ремонта обработан складом
                ProcessedBookkeeping = false, // Акт Обработан бухгалтерий 
                ProductSent = false, // Продукт отправлен
                PendingSubmission = false, // Ожидает отправки
                WriteOff = false // Списание
            };



            act.ActCondition = actCondition;
            db.Act.Add(act);
            // db.Database.ExecuteSqlCommand(@"SET IDENTITY_INSERT [dbo].[ActCondition] ON");
            // db.ActCondition.Add(actCondition);
            db.SaveChanges();
            //   db.Database.BeginTransaction().Commit();
            //   db.Database.ExecuteSqlCommand(@"SET IDENTITY_INSERT [dbo].[ActCondition] OFF"); // все равно не работает,  - тестировал на всякий случай
            return act;
        }

        public ActCondition CreateActCondition(int ActID)
        {
            ActCondition actCondition = new ActCondition
            {
                ActID = ActID,
                OnRepair = false,
                CreateApplication = true,
                IsRepairCompleted = false,
                ProcessedStorage = false,
                ProcessedBookkeeping = false,
                ProductSent = false,
                Completed = false,
                Paid = false,
                WriteOff = false // Списание
            };
            db.Act.FirstOrDefault(act => act.ID == ActID).ActCondition = actCondition;
            db.SaveChanges();
            return actCondition;
        }
        public void SetActReady(Act act)
        {
            if (act != null)
            {

                if (act.ActCondition.ProcessedStorage == true)
                {
                    act.ActCondition.Completed = true;
                }

                var application = act.Application.FirstOrDefault();
                var notwarrantact1 = act.Application.FirstOrDefault().Act.FirstOrDefault
                    (t => (!t.Ready || !t.ReadyStorage || !t.Paid.Value) && (!t.Warranty == true));

                var notwarrantact = act.Application.FirstOrDefault().Act.FirstOrDefault(t => ((bool)!t.ActCondition.Completed || (bool)!t.ActCondition.ProcessedStorage || (bool)!t.ActCondition.Paid) && (!t.Warranty == true));


                //   var warrantact = act.Application.FirstOrDefault().Act.FirstOrDefault(t => (!t.Ready || !t.ReadyStorage) && (t.Warranty == true));


                var warrantact = act.Application.FirstOrDefault().Act.FirstOrDefault(t => ((bool)!t.ActCondition.Completed || (bool)!t.ActCondition.ProcessedStorage) && (t.Warranty == true));




                //  var warrantact = act.ActCondition(tx=>tx.)



                if (notwarrantact == null && warrantact == null) application.Act.FirstOrDefault().ActCondition.PendingSubmission = true;


                db.SaveChanges();
            }
        }

        public void SetActReadyCancel(Act act)
        {
            if (act != null)
            {
                var application = act.Application.FirstOrDefault();
                if (application.Act.FirstOrDefault().ActCondition.ProductSent == true) return;
                act.ActCondition.Completed = false;

                act.DateOfShipment = null;

                db.SaveChanges();
            }
        }

        public void SetActReadyStorage(Act act)
        {
            act.ReadyStorage = true;
            Application application = act.Application.FirstOrDefault();
            //if (application.Act.FirstOrDefault(t => !t.Ready || !t.ReadyStorage || !t.Paid.Value) == null)

            application.Act.FirstOrDefault().ActCondition.ProcessedBookkeeping = true;
            act.ActCondition.ProcessedStorage = true;
            db.SaveChanges();

        }

        public void SetActReadyStorageCancel(Act act)
        {
            Application application = act.Application.FirstOrDefault();
            if (application.Act.FirstOrDefault().ActCondition.ProductSent == true) return;
            // act.ActCondition.ProcessedStorage = false;
            act.ReadyStorage = false;
            act.DateOfShipment = null;
            act.ActCondition.ProcessedStorage = false;
            db.SaveChanges();

        }

        public void SetActPaidandPend(Act act)
        {
            if (act != null)
            {
                act.Paid = true;
                act.ActCondition.Paid = true;
                Application application = act.Application.FirstOrDefault();
                Act s1 = application.Act.FirstOrDefault(t => (!(bool)t.ActCondition.Completed || !(bool)t.ActCondition.ProcessedStorage) && t.Warranty.Value);
                Act s2 = application.Act.FirstOrDefault(t => (!(bool)t.ActCondition.Completed || !(bool)t.ActCondition.ProcessedStorage || !(bool)t.ActCondition.Paid) && (!t.Warranty.Value));


                if (s1 == null && s2 == null)
                    act.ActCondition.PendingSubmission = true;
                db.SaveChanges();
            }
        }

        public void PaidCancel(Act act)
        {
            if (act != null)
            {
                if (act.ActCondition.ProductSent == true) return;
                act.Paid = false;
                act.ActCondition.Paid = false;

                act.DateOfShipment = null;

                if (act.ActCondition.IsRepairCompleted == false)
                    act.ActCondition.IsRepairCompleted = true;

                db.SaveChanges();
            }
        }
    }
}