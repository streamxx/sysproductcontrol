﻿using System;
using System.Linq;
using SystemOfProductionControl.Models;

namespace SystemOfProductionControl.other
{
    public static class CustomConstants
    {

       private static Entities db = new Entities();

       public static float GetNDS()
       {
           TableConstants constants = db.TableConstants.FirstOrDefault(t => t.Name == "NDS");

           float nds = 0;

           try
           {
               nds = float.Parse(constants.Value);


           }
           catch (Exception)
           {
                
           }

           
            return  nds;
        }


        public static float GetWorkPrice()

        {
            TableConstants constants = db.TableConstants.FirstOrDefault(t => t.Name == "WorkPrice");

            float WorkPrice = 0;

            try
            {
                WorkPrice = float.Parse(constants.Value);


            }
            catch (Exception)
            {

            }


            return WorkPrice;
        }

        public static float GetWorkPriceMnog()
        {
            TableConstants constants = db.TableConstants.FirstOrDefault(t => t.Name == "WorkPriceMnog");

            float WorkPriceMnog = 0;

            try
            {
                WorkPriceMnog = float.Parse(constants.Value);


            }
            catch (Exception)
            {

            }


            return WorkPriceMnog;
        }



    }
}