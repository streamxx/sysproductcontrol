﻿using System.Collections.Generic;
namespace SystemOfProductionControl.other
{
    public static class BaseEnums
    {
        public static Dictionary<string, List<string>> ComponentsGroupfilter = new Dictionary<string, List<string>>()
            {
            {"адаптер",new List<string>(){"адаптер"}},
            {"аккумулятор",new List<string>(){"аккумулятор"}},
            {"антенна",new List<string>(){"антенна"}},
            {"аудио оборудование",new List<string>(){"излучатель звука","динамик"}},
            {"варистор",new List<string>(){"варистор"}},
            {"диод",new List<string>(){"диод","диодный мост"}},
            {"индуктивность",new List<string>(){"индуктивность","индук.","индукт."}},
            {"источник питания",new List<string>(){"источник","питания","питан","блок питания","ист.питания"}},
            {"индикатор",new List<string>(){"индикатор"}},
            {"инвертор",new List<string>(){"инвертор"}},
            {"конденсатор",new List<string>(){"конденсатор","конд."}},
            {"клеммник",new List<string>(){"клеммник","колодка","клеммный блок"}},
            {"кварцевый резонатор",new List<string>(){"резонатор","кварцевый","кварц"}},
            {"клавиатура",new List<string>(){"клавиатура"}},
            {"кнопка",new List<string>(){"кнопка","тумблер"}},
            {"контактор",new List<string>(){"контактор"}},
            {"корпус",new List<string>(){"корпус","рейка","каркас"}},
            {"кабель",new List<string>(){"кабель"}},
            {"лампа",new List<string>(){"лампа"}},
            {"микросхема",new List<string>(){"микросхема"}},
            {"модуль",new List<string>(){"модуль"}},
            {"оптопара",new List<string>(){"оптопара"}},
            {"преобразователь",new List<string>(){"преобразователь"}},
            {"переключатель",new List<string>(){"переключатель","грозозащита"}},
            {"предохранитель",new List<string>(){"предохранитель","автомат"}},
            {"плата",new List<string>(){"плата"}},
            {"панель",new List<string>(){"панель"}},
            {"резистор",new List<string>(){"резистор","резисторная сборка"}},
            {"разъём",new List<string>(){"разъём","штекер","кабельный ввод","вилка","розетка"}},
            {"разрядник",new List<string>(){"разрядник"}},
            {"светодиод",new List<string>(){"светодиод"}},
            {"симистор",new List<string>(){"симистор"}},
            {"стабилитрон",new List<string>(){"стабилитрон"}},
            {"тиристор",new List<string>(){"тиристор"}},
            {"термистор",new List<string>(){"термистор"}},
            {"трансформатор",new List<string>(){"трансформатор"}},
            {"фототранзистор",new List<string>(){"фототранзистор"}}

            };

        public enum ApplicationLinkedType
        {
            RepairMail,
            Consignment
        }
        public enum JournalToComponentTypeEnum
        {
            Component,
            Product
        }
        public enum ProductTypeEnum
        {
            Product,
            //полуфабрикат
            Prepack,
            Component
        }
        public enum JournalToWarningOrComponentOrProduct
        {
            Warning,
            Component,
            Product
        }
        public enum PriorityEnum
        {
            Normal,
            Hide,
            Low
        }
        public enum ApplicationTypeEnum
        {
            Normal,
            Returned,   // возврат
            Shortage // недокомплект
        }
        public enum ApplicationStatus
        {
            None,   // 0 Неизвестно
            CreateApplication,    // 1	Создан акт приема оборудования 
            CreateAct,   // 2	Создан акт ремонта
            DivideApplication,     // 3	Разделение акта приема оборудования 
            Repair,  // 4	Ремонт
            RepairCompleted,  // 5 Ремонт завершен
            ActSetNumber,  // 6	Акту ремонта присвоен номер
            ProcessedStorage,   // 7	Акт ремонта обработан складом
            ActProcessedBookkeeping,  // 8 Акт ремонта обработан бухгалтерией
            ProductSent,  // 9	Оборудование отправлено
            ApplicationCompleted,  // 10 Завершено
            PendingSubmission,  // 11  Ожидает отправки
            ApplicationOffs    // 12 Списание
        }
    }
}