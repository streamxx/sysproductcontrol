﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SystemOfProductionControl.Startup))]
namespace SystemOfProductionControl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);



        }

    }
}
